from django.apps import AppConfig


class PlantsystemConfig(AppConfig):
    name = 'plantsystem'

    def ready(self):
        import plantsystem.signals

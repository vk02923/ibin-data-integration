from django.db import models

# Create your models here.
from django.db import models

# Create your models here.
from django.db import models
from django.contrib.auth.models import User
import string
from datetime import date,datetime



class IBINDataModel(models.Model):
    user_id=models.ForeignKey(User,on_delete=models.CASCADE)
    validated=models.BooleanField(default=False)

    def __str__(self):
        return (self.user_id.username + "_" + str(self.id))



class BaseElement(models.Model):

    RecordID=models.ForeignKey(IBINDataModel,on_delete=models.CASCADE)
    SpeciesID=models.CharField(max_length=60,blank=True,unique=True,null=True)
    GlobalUniqueIdentfier=models.TextField(max_length=100,blank=True,null=True)
    Abstract=models.TextField(max_length=100,blank=True,null=True)
    RecordBasis=models.TextField(max_length=100,blank=True,null=True)

    def __str__(self):
        return  str(self.RecordID)


class NomenclatureAndClassification(models.Model):

    RecordID=models.ForeignKey(IBINDataModel,on_delete=models.CASCADE)
    Genus=models.TextField(blank=False)
    Specie=models.TextField(blank=False)
    Subspecie=models.TextField(blank=True,null=True)
    Varietie=models.TextField(blank=True,null=True)
    Race=models.TextField(blank=True)
    Strains=models.TextField(blank=True)
    CommonName=models.TextField(blank=True)
    Synonyms=models.TextField(blank=True)
    Language=models.TextField(blank=True)
    Kingdom=models.TextField(blank=True)
    Phylum=models.TextField(blank=True)
    Class=models.TextField(blank=True)
    SubClass=models.TextField(blank=True,null=True)
    Order=models.TextField(blank=True)
    Family=models.TextField(blank=True)
    


    def __str__(self):
        return  str(self.RecordID)

class Description(models.Model):

    RecordID=models.ForeignKey(IBINDataModel,on_delete=models.CASCADE)
    DescripionOfTaxonAndPhenology=models.TextField(blank=True,null=True)
    Diagnostic=models.TextField(blank=True,null=True)
    

    def __str__(self):
        return  str(self.RecordID)


class NaturalHistory(models.Model):
    RecordID=models.ForeignKey(IBINDataModel,on_delete=models.CASCADE)
    Cyclicity=models.TextField(blank=True,null=True)
    Reproduction=models.TextField(blank=True,null=True)
    Migration=models.TextField(blank=True,null=True)
    Dispersal=models.TextField(blank=True,null=True)
    Association=models.TextField(blank=True,null=True)
    Disesase=models.TextField(blank=True,null=True)

    def __str__(self):
        return  str(self.RecordID)


class GeneticMolecularAndBiochemicalCharacterisation(models.Model):
    RecordID=models.ForeignKey(IBINDataModel,on_delete=models.CASCADE)
    Somatic=models.TextField(blank=True,null=True)
    Zygotic=models.TextField(blank=True,null=True)
    Gametic=models.TextField(blank=True,null=True)
    PloidyLevel=models.TextField(blank=True,null=True)
    GenomeSize=models.TextField(blank=True,null=True)
    MolecularChemicalMarkers=models.TextField(blank=True,null=True)
    InputsFromCSIRCIMAP=models.TextField(blank=True,null=True)
    
    def __str__(self):
        return  str(self.RecordID)

class HabitatLocationAndDistribution(models.Model):
    RecordID=models.ForeignKey(IBINDataModel,on_delete=models.CASCADE)
    Habitat=models.TextField(blank=True,null=True)
    MinAltitude=models.TextField(blank=True,null=True)
    MaxAltitude=models.TextField(blank=True,null=True)
    Latitude=models.TextField(blank=True,null=True)
    Longitude=models.TextField(blank=True,null=True)
    BiogeographicZoneAndProvince=models.TextField(blank=True,null=True)
    BioticZone=models.TextField(blank=True,null=True)
    state=models.TextField(blank=True,null=True)
    District=models.TextField(blank=True,null=True)
    Taluk=models.TextField(blank=True,null=True)
    Place=models.TextField(blank=True,null=True)
    VegetationType=models.TextField(blank=True,null=True)
    Habit=models.TextField(blank=True,null=True)
    Source=models.TextField(blank=True,null=True)
    Nativity=models.TextField(blank=True,null=True)
    Endemism=models.TextField(blank=True,null=True)
    GlobalDistribution=models.TextField(blank=True,null=True)

    def __str__(self):
        return  str(self.RecordID)


class DemographyAndConversion(models.Model):
    RecordID=models.ForeignKey(IBINDataModel,on_delete=models.CASCADE)
    PopulationBiology=models.TextField(blank=True,null=True)
    IndianRedDataBook=models.TextField(blank=True,null=True)
    Cities=models.TextField(blank=True,null=True)
    IUCN=models.TextField(blank=True,null=True)
    ThreatStatus=models.TextField(blank=True,null=True)
    WPA=models.TextField(blank=True,null=True)
    FRIHTThreatAssessment=models.TextField(blank=True,null=True)
    Legislation=models.TextField(blank=True,null=True)

    def __str__(self):
        return  str(self.RecordID)


class UsesAndManagement(models.Model):
    RecordID=models.ForeignKey(IBINDataModel,on_delete=models.CASCADE)
    BroadSubject=models.TextField(blank=True,null=True)
    Keywords=models.TextField(blank=True,null=True)
    Parts=models.TextField(blank=True,null=True)
    Uses=models.TextField(blank=True,null=True)
    SystemOfMedicine=models.TextField(blank=True,null=True)
    Comments=models.TextField(blank=True,null=True)
    Others=models.TextField(blank=True,null=True)
    FolkKnowledge=models.TextField(blank=True,null=True)
    Shlokam=models.TextField(blank=True,null=True)
    Source=models.TextField(blank=True,null=True)
    ShlokaNumber=models.TextField(blank=True,null=True)
    DetailsofShloka=models.TextField(blank=True,null=True)
    PlantslistedAndOthers=models.TextField(blank=True,null=True)
    OtherInformation=models.TextField(blank=True,null=True)
    Management=models.TextField(blank=True,null=True)

    def __str__(self):
        return  str(self.RecordID)

class DatasetDetail(models.Model):
    RecordID=models.ForeignKey(IBINDataModel,on_delete=models.CASCADE)
    DatasetName=models.TextField(blank=True,null=True)
    DatasetPurpose=models.TextField(blank=True,null=True)
    DatasaetDescription=models.TextField(blank=True,null=True)
    DatasetLink=models.TextField(blank=True,null=True)


    def __str__(self):
        return str(self.RecordID)


class PatentDetail(models.Model):
    RecordID=models.ForeignKey(IBINDataModel,on_delete=models.CASCADE)
    PatentName=models.TextField(blank=True,null=True)
    PatentNumber=models.TextField(blank=True,null=True)
    PatentOwner=models.TextField(blank=True,null=True)
    ProductProcess=models.TextField(blank=True,null=True)
    PatentDate=models.TextField(blank=True,null=True)
    PatentLink=models.TextField(blank=True,null=True)

    def __str__(self):
        return  str(self.RecordID)


class SpecimenDetail(models.Model):
    RecordID=models.ForeignKey(IBINDataModel,on_delete=models.CASCADE)
    TypeSpecimen=models.TextField(blank=True,null=True)
    TypeCollector=models.TextField(blank=True,null=True)
    TypeDate=models.DateField(blank=True,null=True)
    TypeLocation=models.TextField(blank=True,null=True)
    TypeDepository=models.TextField(blank=True,null=True)
    TypeRecord=models.TextField(blank=True,null=True)
    TypeSource=models.TextField(blank=True,null=True)

    def __str__(self):
        return  str(self.RecordID)


class InformationListing(models.Model):
    RecordID=models.ForeignKey(IBINDataModel,on_delete=models.CASCADE)
    SpeciesAndChromosomeAuthor=models.TextField(blank=True,null=True)
    SpeciesAndChromosomeReference=models.TextField(blank=True,null=True)
    SpeciesAndChromosomeVolume=models.TextField(blank=True,null=True)
    SpeciesAndChromosomeEdition=models.TextField(blank=True,null=True)
    SpeciesAndChromosomeYearOfPublication=models.TextField(blank=True,null=True)
    SpeciesAndChromosomeContributor=models.TextField(blank=True,null=True)
    SpeciesAndChromosomeType=models.TextField(blank=True,null=True)
    VarietyAndSpeciesAuthor=models.TextField(blank=True,null=True)
    VarietyAndSpeciesReference=models.TextField(blank=True,null=True)
    VarietyAndSpeciesVolume=models.TextField(blank=True,null=True)
    VarietyAndSpeciesPageno=models.TextField(blank=True,null=True)
    VarietyAndSpeciesYearOfPublication=models.TextField(blank=True,null=True)
    VernicularNamesAuthor=models.TextField(blank=True,null=True)
    VernicularNamesReference=models.TextField(blank=True,null=True)
    VernicularNamesVolume=models.TextField(blank=True,null=True)
    VernicularNamesPublication=models.TextField(blank=True,null=True)
    VernicularNamesYearOfPublication=models.TextField(blank=True,null=True)
    SynonymsAuthor=models.TextField(blank=True,null=True)
    SynonymsReference=models.TextField(blank=True,null=True)
    SynonymsYearOfPublication=models.TextField(blank=True,null=True)
    ShlokaAuthor=models.TextField(blank=True,null=True)
    ShlokaReference=models.TextField(blank=True,null=True)
    ShlokaYearOfPublication=models.TextField(blank=True,null=True)
    ShlokaPageno=models.TextField(blank=True,null=True)
    ShlokaPublisher=models.TextField(blank=True,null=True)
    ShlokaVerse=models.TextField(blank=True,null=True)
    Documents=models.TextField(blank=True,null=True)
    HtmlPageLinks=models.TextField(blank=True,null=True)
    SpeciesWebServicesApi=models.TextField(blank=True,null=True)
    ChromosomesDataLinkFromdGRIP=models.TextField(blank=True,null=True)

    
    def __str__(self):
        return  str(self.RecordID)



class ObservationalInfromation(models.Model):
    RecordID=models.ForeignKey(IBINDataModel,on_delete=models.CASCADE)
    TypeMethod=models.TextField(blank=True,null=True)
    AssocatedTaxa=models.TextField(blank=True,null=True)
    SpecimenSex=models.TextField(blank=True,null=True)
    SpecimenWeight=models.TextField(blank=True,null=True)
    SampleEffort=models.TextField(blank=True,null=True)
    Temperature=models.TextField(blank=True,null=True)


    def __str__(self):
        return  str(self.RecordID)
    
class WMSURL(models.Model):
    RecordID=models.ForeignKey(IBINDataModel,on_delete=models.CASCADE)
    WMSURLforSpeciesDistribution=models.TextField(blank=True,null=True)

    def __str__(self):
        return  str(self.RecordID)


class Multimedia(models.Model):
    RecordID=models.ForeignKey(IBINDataModel,on_delete=models.CASCADE)
    Source=models.TextField(blank=True,null=True)
    Collector=models.TextField(blank=True,null=True)

    def __str__(self):
        return  str(self.RecordID)



class DataQualityStatu(models.Model):
    RecordID=models.ForeignKey(IBINDataModel,on_delete=models.CASCADE)
    CurationLevel=models.TextField(blank=True,null=True)
    CuratedBy=models.TextField(blank=True,null=True)
    CurationDate=models.DateField(blank=True,null=True)

    def __str__(self):
        return  str(self.RecordID)






class Images(models.Model):
    RecordID=models.ForeignKey(IBINDataModel,on_delete=models.CASCADE)
    Type=models.CharField(max_length=100,null=True,blank=True)   
    image = models.ImageField(upload_to='images/',null=True,blank=True)

    def __str__(self):
        return  str(self.RecordID)





class uploaded_csv(models.Model):
    User_Id=models.ForeignKey(User,on_delete=models.CASCADE)
    filename=models.TextField(blank=False,null=False)
    filepath=models.TextField(blank=False,null=False)
    status=models.CharField(max_length=256, choices=[('1', 'denied'), ('2', 'processing'),('3', 'processed'),('4', 'success'),('5', 'upload successful')])
    date=models.DateField(default=date.today)
    def __str__(self):
        return  str(self.filename)


class uploaded_records(models.Model):
    User_Id=models.ForeignKey(User,on_delete=models.CASCADE)
    RecordID=models.ForeignKey(IBINDataModel,on_delete=models.CASCADE)
    status=models.CharField(max_length=256, choices=[('1', 'denied'), ('2', 'processing'),('3', 'processed'),('4', 'success'),('5', 'upload successful')])
    date=models.DateField(default=date.today)
    def __str__(self):
        return  str(self.RecordID)  


# class process_report_csv(models.Model):
#     RecordID=models.ForeignKey(uploaded_csv,on_delete=models.CASCADE)
#     report_path=models.TextField(blank=True,null=True)
#     message=models.TextField(blank=True,null=True)
#     def __str__(self):
#         return  str(self.RecordID)


# class process_report_csv(models.Model):
#     RecordID=models.ForeignKey(uploaded_csv,on_delete=models.CASCADE)
#     report_path=models.TextField(blank=True,null=True)
#     message=models.TextField(blank=True,null=True)
#     def __str__(self):
#         return  str(self.RecordID)








class IbinSpecieslist(models.Model):
    ibin_speciesid = models.CharField(max_length=800, blank=True, null=True)
    ibin_speciesname = models.CharField(max_length=800, blank=True, null=True)
    ibin_scnm = models.CharField(max_length=800, blank=True, null=True)
    author = models.CharField(max_length=800, blank=True, null=True)
    path = models.CharField(max_length=800, blank=True, null=True)
    img_wt = models.CharField(max_length=800, blank=True, null=True)
    contributor = models.CharField(max_length=800, blank=True, null=True)
    category = models.CharField(max_length=800, blank=True, null=True)
    genus = models.CharField(max_length=800, blank=True, null=True)
    type = models.CharField(max_length=800, blank=True, null=True)
    chromosme_path = models.CharField(max_length=800, blank=True, null=True)
    chromosomes = models.CharField(max_length=800, blank=True, null=True)
    species_id = models.CharField(max_length=800, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ibin_specieslist'





class PlantGenus(models.Model):
    genus_id = models.CharField(primary_key=True, max_length=250)
    genus_name = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'plant_genus'

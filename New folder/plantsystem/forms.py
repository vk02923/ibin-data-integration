from django.forms import ModelForm
from .models import *
from django import forms

class BaseElementForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BaseElementForm, self).__init__(*args, **kwargs)
        self.fields['SpeciesID'].widget=forms.TextInput(attrs={})
        self.fields['GlobalUniqueIdentfier'].widget=forms.TextInput(attrs={  })
        self.fields['Abstract'].widget=forms.TextInput(attrs={  })
        self.fields['RecordBasis'].widget=forms.TextInput(attrs={  })
        

    class Meta:
        model = BaseElement
        fields = "__all__"
        exclude= ('RecordID',)
    
class NomenclatureAndClassificationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(NomenclatureAndClassificationForm, self).__init__(*args, **kwargs)
        self.fields['Genus'].widget=forms.TextInput(attrs={ 'id':'tags' })
        self.fields['Specie'].widget=forms.TextInput(attrs={  })
        self.fields['Subspecie'].widget=forms.TextInput(attrs={  })
        self.fields['Varietie'].widget=forms.TextInput(attrs={  })
        self.fields['Race'].widget=forms.TextInput(attrs={  })
        self.fields['Strains'].widget=forms.TextInput(attrs={  })
        self.fields['CommonName'].widget=forms.TextInput(attrs={  })
        self.fields['Language'].widget=forms.TextInput(attrs={  })
        self.fields['Kingdom'].widget=forms.TextInput(attrs={  })
        self.fields['Phylum'].widget=forms.TextInput(attrs={  })
        self.fields['Class'].widget=forms.TextInput(attrs={  })
        self.fields['SubClass'].widget=forms.TextInput(attrs={  })
        self.fields['Order'].widget=forms.TextInput(attrs={  })
        self.fields['Family'].widget=forms.TextInput(attrs={  })
      

    class Meta:
        model = NomenclatureAndClassification
        fields = "__all__"
        exclude= ('RecordID',)

class DescriptionForm(ModelForm):
    
    def __init__(self, *args, **kwargs):
        super(DescriptionForm, self).__init__(*args, **kwargs)
        self.fields['DescripionOfTaxonAndPhenology'].widget=forms.TextInput(attrs={  })
        self.fields['Diagnostic'].widget=forms.TextInput(attrs={  })
       
    class Meta:
        model = Description
        fields = "__all__"
        exclude= ('RecordID',)

class NaturalHistroyForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(NaturalHistroyForm, self).__init__(*args, **kwargs)
        self.fields['Cyclicity'].widget=forms.TextInput(attrs={  })
        self.fields['Reproduction'].widget=forms.TextInput(attrs={  })
        self.fields['Migration'].widget=forms.TextInput(attrs={  })
        self.fields['Dispersal'].widget=forms.TextInput(attrs={  })
        self.fields['Association'].widget=forms.TextInput(attrs={  })
        self.fields['Disesase'].widget=forms.TextInput(attrs={  })
        
    class Meta:
        model = NaturalHistory
        fields = "__all__"
        exclude= ('RecordID',)

class GenericMolecularCharactersticForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(GenericMolecularCharactersticForm, self).__init__(*args, **kwargs)
        self.fields['Somatic'].widget=forms.TextInput(attrs={  })
        self.fields['Zygotic'].widget=forms.TextInput(attrs={  })
        self.fields['Gametic'].widget=forms.TextInput(attrs={  })
        self.fields['PloidyLevel'].widget=forms.TextInput(attrs={  })
        self.fields['GenomeSize'].widget=forms.TextInput(attrs={  })
        self.fields['MolecularChemicalMarkers'].widget=forms.TextInput(attrs={  })
        self.fields['InputsFromCSIRCIMAP'].widget=forms.TextInput(attrs={  })
        
       
    class Meta:
        model = GeneticMolecularAndBiochemicalCharacterisation
        fields = "__all__"
        exclude= ('RecordID',)

class HabitatLocationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(HabitatLocationForm, self).__init__(*args, **kwargs)
        self.fields['Habitat'].widget=forms.TextInput(attrs={  })
        self.fields['MinAltitude'].widget=forms.TextInput(attrs={  })
        self.fields['MaxAltitude'].widget=forms.TextInput(attrs={  })
        self.fields['Latitude'].widget=forms.TextInput(attrs={  })
        self.fields['Longitude'].widget=forms.TextInput(attrs={  })
        self.fields['BiogeographicZoneAndProvince'].widget=forms.TextInput(attrs={  })
        self.fields['BioticZone'].widget=forms.TextInput(attrs={  })
        self.fields['state'].widget=forms.TextInput(attrs={  })
        self.fields['District'].widget=forms.TextInput(attrs={  })
        self.fields['Taluk'].widget=forms.TextInput(attrs={  })

        self.fields['Place'].widget=forms.TextInput(attrs={  })
        self.fields['VegetationType'].widget=forms.TextInput(attrs={  })

        self.fields['Habit'].widget=forms.TextInput(attrs={  })

        self.fields['Source'].widget=forms.TextInput(attrs={  })
        self.fields['Nativity'].widget=forms.TextInput(attrs={  })

        self.fields['Endemism'].widget=forms.TextInput(attrs={  })
        self.fields['GlobalDistribution'].widget=forms.TextInput(attrs={  })
    class Meta:
        model = HabitatLocationAndDistribution
        fields = "__all__"
        exclude= ('RecordID',)

class DemographicForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(DemographicForm, self).__init__(*args, **kwargs)
        self.fields['PopulationBiology'].widget=forms.TextInput(attrs={  })
        self.fields['IndianRedDataBook'].widget=forms.TextInput(attrs={  })
        self.fields['Cities'].widget=forms.TextInput(attrs={  })
        self.fields['IUCN'].widget=forms.TextInput(attrs={  })
        self.fields['ThreatStatus'].widget=forms.TextInput(attrs={  })
        self.fields['WPA'].widget=forms.TextInput(attrs={  })
        self.fields['FRIHTThreatAssessment'].widget=forms.TextInput(attrs={  })
        self.fields['Legislation'].widget=forms.TextInput(attrs={  })
    class Meta:
        model = DemographyAndConversion
        fields = "__all__"
        exclude= ('RecordID',)

class UsesAndManagmentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(UsesAndManagmentForm, self).__init__(*args, **kwargs)
        self.fields['BroadSubject'].widget=forms.TextInput(attrs={  })
        self.fields['Keywords'].widget=forms.TextInput(attrs={  })
        self.fields['Parts'].widget=forms.TextInput(attrs={  })
        self.fields['Uses'].widget=forms.TextInput(attrs={  })
        self.fields['SystemOfMedicine'].widget=forms.TextInput(attrs={  })
        self.fields['Comments'].widget=forms.TextInput(attrs={  })
        self.fields['Others'].widget=forms.TextInput(attrs={  })
        self.fields['FolkKnowledge'].widget=forms.TextInput(attrs={  })
        self.fields['Shlokam'].widget=forms.TextInput(attrs={  })
        self.fields['Source'].widget=forms.TextInput(attrs={  })
        self.fields['ShlokaNumber'].widget=forms.TextInput(attrs={  })
        self.fields['DetailsofShloka'].widget=forms.TextInput(attrs={  })
        self.fields['PlantslistedAndOthers'].widget=forms.TextInput(attrs={  })
        self.fields['OtherInformation'].widget=forms.TextInput(attrs={  })
        self.fields['Management'].widget=forms.TextInput(attrs={  })
        
    class Meta:
        model = UsesAndManagement
        fields = "__all__"
        exclude= ('RecordID',)

class DatasetForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(DatasetForm, self).__init__(*args, **kwargs)
        self.fields['DatasetName'].widget=forms.TextInput(attrs={  })
        self.fields['DatasetPurpose'].widget=forms.TextInput(attrs={  })
        self.fields['DatasaetDescription'].widget=forms.TextInput(attrs={  })
        self.fields['DatasetLink'].widget=forms.TextInput(attrs={  })
       
    class Meta:
        model = DatasetDetail
        fields = "__all__"
        exclude= ('RecordID',)

class PatentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(PatentForm, self).__init__(*args, **kwargs)
        self.fields['PatentName'].widget=forms.TextInput(attrs={  })
        self.fields['PatentNumber'].widget=forms.TextInput(attrs={  })
        self.fields['PatentOwner'].widget=forms.TextInput(attrs={  })
        self.fields['ProductProcess'].widget=forms.TextInput(attrs={  })
        self.fields['PatentDate'].widget=forms.TextInput(attrs={  })
        self.fields['PatentLink'].widget=forms.TextInput(attrs={  })
        
    class Meta:
        model = PatentDetail
        fields = "__all__"
        exclude= ('RecordID',)

class SpecimentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SpecimentForm, self).__init__(*args, **kwargs)
        self.fields['TypeSpecimen'].widget=forms.TextInput(attrs={  })
        self.fields['TypeCollector'].widget=forms.TextInput(attrs={  })
        self.fields['TypeDate'].widget=forms.TextInput(attrs={  })
        self.fields['TypeLocation'].widget=forms.TextInput(attrs={  })
        self.fields['TypeDepository'].widget=forms.TextInput(attrs={  })
        self.fields['TypeRecord'].widget=forms.TextInput(attrs={  })
        self.fields['TypeSource'].widget=forms.TextInput(attrs={  })
        
    class Meta:
        model = SpecimenDetail
        fields = "__all__"
        exclude= ('RecordID',)

class InformationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(InformationForm, self).__init__(*args, **kwargs)
        self.fields['SpeciesAndChromosomeAuthor'].widget=forms.TextInput(attrs={  })
        self.fields['SpeciesAndChromosomeAuthor'].label = "Species And Chromosome Author"
        self.fields['SpeciesAndChromosomeReference'].widget=forms.TextInput(attrs={  })
        self.fields['SpeciesAndChromosomeReference'].label="Species And Chromosome Reference"
        self.fields['SpeciesAndChromosomeVolume'].widget=forms.TextInput(attrs={  })
        self.fields['SpeciesAndChromosomeVolume'].label="Species And Chromosome Volume"
        self.fields['SpeciesAndChromosomeEdition'].widget=forms.TextInput(attrs={  })
        self.fields['SpeciesAndChromosomeEdition'].label="Species And Chromosome Edition"
        self.fields['SpeciesAndChromosomeYearOfPublication'].widget=forms.TextInput(attrs={  })
        self.fields['SpeciesAndChromosomeYearOfPublication'].label="Species And Chromosome Year Of Publication"
        self.fields['SpeciesAndChromosomeContributor'].widget=forms.TextInput(attrs={  })
        self.fields['SpeciesAndChromosomeContributor'].label = "Species And Chromosome Contributor"
        self.fields['SpeciesAndChromosomeType'].widget=forms.TextInput(attrs={  })
        self.fields['SpeciesAndChromosomeType'].label = "Species And Chromosome Type"
        self.fields['VarietyAndSpeciesAuthor'].widget=forms.TextInput(attrs={  })
        self.fields['VarietyAndSpeciesAuthor'].label = "Variety And Species Author"
        self.fields['VarietyAndSpeciesReference'].widget=forms.TextInput(attrs={  })
        self.fields['VarietyAndSpeciesReference'].label = "Variety And Species Reference"
        self.fields['VarietyAndSpeciesVolume'].widget=forms.TextInput(attrs={  })
        self.fields['VarietyAndSpeciesVolume'].label = "Variety And Species Volume"
        self.fields['VarietyAndSpeciesPageno'].widget=forms.TextInput(attrs={  })
        self.fields['VarietyAndSpeciesYearOfPublication'].widget=forms.TextInput(attrs={  })
        self.fields['VernicularNamesAuthor'].widget=forms.TextInput(attrs={  })
        self.fields['VernicularNamesReference'].widget=forms.TextInput(attrs={  })
        self.fields['VernicularNamesVolume'].widget=forms.TextInput(attrs={  })
        self.fields['VernicularNamesPublication'].widget=forms.TextInput(attrs={  })
        self.fields['VernicularNamesYearOfPublication'].widget=forms.TextInput(attrs={  })
        self.fields['SynonymsAuthor'].widget=forms.TextInput(attrs={  })
        self.fields['SynonymsReference'].widget=forms.TextInput(attrs={  })
        self.fields['SynonymsYearOfPublication'].widget=forms.TextInput(attrs={  })
        self.fields['ShlokaAuthor'].widget=forms.TextInput(attrs={  })

        self.fields['ShlokaReference'].widget=forms.TextInput(attrs={  })
        self.fields['ShlokaYearOfPublication'].widget=forms.TextInput(attrs={  })
        self.fields['ShlokaPageno'].widget=forms.TextInput(attrs={  })
        self.fields['ShlokaPublisher'].widget=forms.TextInput(attrs={  })
        self.fields['ShlokaVerse'].widget=forms.TextInput(attrs={  })
        self.fields['Documents'].widget=forms.TextInput(attrs={  })
        self.fields['HtmlPageLinks'].widget=forms.TextInput(attrs={  })
        self.fields['SpeciesWebServicesApi'].widget=forms.TextInput(attrs={  })
        self.fields['ChromosomesDataLinkFromdGRIP'].widget=forms.TextInput(attrs={  })
    class Meta:
        model = InformationListing
        fields = "__all__"
        exclude= ('RecordID',)

class ObservationalForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ObservationalForm, self).__init__(*args, **kwargs)
        self.fields['TypeMethod'].widget=forms.TextInput(attrs={  })
        self.fields['AssocatedTaxa'].widget=forms.TextInput(attrs={  })
        self.fields['SpecimenSex'].widget=forms.TextInput(attrs={  })
        self.fields['SpecimenWeight'].widget=forms.TextInput(attrs={  })
        self.fields['SampleEffort'].widget=forms.TextInput(attrs={  })
        self.fields['Temperature'].widget=forms.TextInput(attrs={  })
        
    class Meta:
        model = ObservationalInfromation
        fields = "__all__"
        exclude= ('RecordID',)

class WmsurlForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(WmsurlForm, self).__init__(*args, **kwargs)
        self.fields['WMSURLforSpeciesDistribution'].widget=forms.TextInput(attrs={  })
       
    class Meta:
        model = WMSURL
        fields = "__all__"
        exclude= ('RecordID',)

class MultimediaForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(MultimediaForm, self).__init__(*args, **kwargs)
        self.fields['Source'].widget=forms.TextInput(attrs={  })
        self.fields['Collector'].widget=forms.TextInput(attrs={  })
    
    class Meta:
        model = Multimedia
        fields = ['Source','Collector']
        exclude= ('RecordID',)

class DataQualityForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(DataQualityForm, self).__init__(*args, **kwargs)
        self.fields['CurationLevel'].widget=forms.TextInput(attrs={  })
        self.fields['CuratedBy'].widget=forms.TextInput(attrs={  })
        
    class Meta:
        model = DataQualityStatu
        fields = "__all__"
        exclude= ('RecordID',)


class MultimediaFullForm(MultimediaForm): #extending form
    Herberium = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True}),required=False)
    LineDiagram=forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True}),required=False)
    photographs=forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True}),required=False,label="Photographs/Field Images")
    others=forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True}),required=False,label="Others/Miscellaneous")

    class Meta(MultimediaForm.Meta):
        fields = MultimediaForm.Meta.fields + ['Herberium','LineDiagram','photographs','others']


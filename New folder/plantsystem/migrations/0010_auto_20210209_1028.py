# Generated by Django 3.1.5 on 2021-02-09 04:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('plantsystem', '0009_auto_20210208_1656'),
    ]

    operations = [
        migrations.AlterField(
            model_name='usesandmanagement',
            name='FolkKnowledge',
            field=models.TextField(blank=True, null=True),
        ),
    ]

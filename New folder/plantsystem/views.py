from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from .forms import *
import pandas as pd
from django.contrib import messages
import csv
import chardet 
from django.shortcuts import redirect
import json
from django.views.decorators.csrf import csrf_exempt
import simplejson as jsonn
from django.http import JsonResponse
from django.db import connection
from django.http import HttpResponse
from django.core.files.storage import FileSystemStorage
from datetime import date,datetime
from .models import uploaded_csv,PlantGenus




@login_required
def input_field(request):


    
    if 'term' in request.GET:
        print("inside")
        print(request.GET.get('term'))
        qs=IbinSpecieslist.objects.filter(genus__contains=request.GET.get('term'))
        print(qs)
        exp=list()
        for prod in qs:
            exp.append(prod.genus)
        print(exp)
        return JsonResponse(exp,safe=False)



    

    if request.method == 'POST':
            print("form submitted")
            # Baseform = BaseElementForm(request.POST)
            NandCform=NomenclatureAndClassificationForm(request.POST)
            Descform=DescriptionForm(request.POST)
            Nhistoryform=NaturalHistroyForm(request.POST)
            GeneticMolecularForm=GenericMolecularCharactersticForm(request.POST)
            Hldform=HabitatLocationForm(request.POST)
            Demograhicform=DemographicForm(request.POST)
            usesform=UsesAndManagmentForm(request.POST)
            datasetForm=DatasetForm(request.POST)
            patentform=PatentForm(request.POST)
            specimentForm=SpecimentForm(request.POST)
            infoform=InformationForm(request.POST)
            obserform=ObservationalForm(request.POST)
            wsmlform=WmsurlForm(request.POST)
            fullmultimediaform=MultimediaFullForm(request.POST)
            dataqualityform=DataQualityForm(request.POST)

            herberiumfiles=request.FILES.getlist('Herberium')
            LineDiagramfiles=request.FILES.getlist('LineDiagram')
            others=request.FILES.getlist('others')
            photographs=request.FILES.getlist('photographs')

            print(herberiumfiles)



            if NandCform.is_valid() and Descform.is_valid() and Nhistoryform.is_valid() and GeneticMolecularForm.is_valid() and Hldform.is_valid() and Demograhicform.is_valid() and usesform.is_valid and datasetForm.is_valid() and patentform.is_valid() and specimentForm.is_valid() and infoform.is_valid() and obserform.is_valid() and wsmlform.is_valid() and fullmultimediaform.is_valid() and dataqualityform.is_valid   :

                print("validated")
                datamodel = IBINDataModel(user_id=request.user,validated=False)
                datamodel.save()
                # belement=BaseElement(RecordID=datamodel,SpeciesID=Baseform.cleaned_data.get('SpeciesID'),GlobalUniqueIdentfier=Baseform.cleaned_data.get('GlobalUniqueIdentfier'),Abstract=Baseform.cleaned_data.get('Abstract'),RecordBasis=Baseform.cleaned_data.get('RecordBasis'))

                belement=BaseElement(RecordID=datamodel,SpeciesID=None,GlobalUniqueIdentfier=None,Abstract=None,RecordBasis=None)
                belement.save()

                obj=NandCform.save(commit=False)
                obj.RecordID=datamodel
                obj.save()

                obj=Descform.save(commit=False)
                obj.RecordID=datamodel
                obj.save()

                obj=Nhistoryform.save(commit=False)
                obj.RecordID=datamodel
                obj.save()

                obj=GeneticMolecularForm.save(commit=False)
                obj.RecordID=datamodel
                obj.save()

                obj=Hldform.save(commit=False)
                obj.RecordID=datamodel
                obj.save()

                obj=Demograhicform.save(commit=False)
                obj.RecordID=datamodel
                obj.save()


                obj=usesform.save(commit=False)
                obj.RecordID=datamodel
                obj.save()

                obj=datasetForm.save(commit=False)
                obj.RecordID=datamodel
                obj.save()


                obj=patentform.save(commit=False)
                obj.RecordID=datamodel
                obj.save()

                obj=specimentForm.save(commit=False)
                obj.RecordID=datamodel
                obj.save()


                obj=infoform.save(commit=False)
                obj.RecordID=datamodel
                obj.save()


                obj=obserform.save(commit=False)
                obj.RecordID=datamodel
                obj.save()

                obj=wsmlform.save(commit=False)
                obj.RecordID=datamodel
                obj.save()


                obj=Multimedia(RecordID=datamodel,Source=fullmultimediaform.cleaned_data.get("Source"),Collector=fullmultimediaform.cleaned_data.get("Collector"))
                obj.save()


                for f in herberiumfiles:
                    Images.objects.create(RecordID=datamodel,Type="Herberium",image=f)
                for f in LineDiagramfiles:
                    Images.objects.create(RecordID=datamodel,Type="LineDiagram",image=f)
                for f in others:
                    Images.objects.create(RecordID=datamodel,Type="others",image=f)
                for f in photographs:
                    Images.objects.create(RecordID=datamodel,Type="photographs",image=f)
                

                obj=datasetForm.save(commit=False)
                obj.RecordID=datamodel
                obj.save()


                obj=uploaded_records(User_Id=request.user,RecordID=datamodel,status='5')
                obj.save()



                
                return redirect("successsubmit")
            
            else:
                error="error submiting form please try again"
                
                context1={
                        # "baseform":Baseform,
                        "nandcform":NandCform,
                        "descform":Descform,
                        "Nhistoryfrom":Nhistoryform,
                        "geneticform":GeneticMolecularForm,
                        "hldform":Hldform,
                        "demographicform":Demograhicform,
                        "usesform":usesform,
                        "datasetForm":datasetForm,
                        "patentForm":patentform,
                        "specimenfrom":SpecimentForm,
                        "infoform":infoform,
                        "obserform":obserform,
                        "wsmlform":wsmlform,
                        "multimediaform":fullmultimediaform,
                        "dataquality":dataqualityform,
                        'error':error,

                        }

                return render(request,template_name = "plantsystem.html",context=context1)





    # Baseform = BaseElementForm()
    NandCform=NomenclatureAndClassificationForm()
    Descform=DescriptionForm()
    Nhistoryform=NaturalHistroyForm()
    GeneticMolecularForm=GenericMolecularCharactersticForm()
    Hldform=HabitatLocationForm()
    Demograhicform=DemographicForm()
    usesform=UsesAndManagmentForm()
    datasetForm=DatasetForm()
    patentform=PatentForm()
    specimentForm=SpecimentForm()
    infoform=InformationForm()
    obserform=ObservationalForm()
    wsmlform=WmsurlForm()
    multimediaform=MultimediaFullForm()
    dataqualityform=DataQualityForm()

    context1={
        # "baseform":Baseform,
        "nandcform":NandCform,
        "descform":Descform,
        "Nhistoryfrom":Nhistoryform,
        "geneticform":GeneticMolecularForm,
        "hldform":Hldform,
        "demographicform":Demograhicform,
        "usesform":usesform,
        "datasetForm":datasetForm,
        "patentForm":patentform,
        "specimenfrom":SpecimentForm,
        "infoform":infoform,
        "obserform":obserform,
        "wsmlform":wsmlform,
        "multimediaform":multimediaform,
        "dataquality":dataqualityform,

    }

        # return render(request = request,
        #                 template_name = "index.html",
        #                 context={"form":form})
    
    return render(request = request,
                        template_name = "plantsystem.html",context=context1)




@login_required
def Home(request):
    return render(request,template_name="plantform.html")


@login_required
def successsubmit(request):
    return render(request,template_name="Successfulsubmit.html")







@login_required
def uploadcsv(request):
    if request.method == "POST":
        print("yes file found")
        my_uploaded_file = request.FILES['my_uploaded_file']
        if not my_uploaded_file.name.endswith('.csv'):
            return render(request,template_name="uploadcsv.html",context={"error":"THIS IS NOT A CSV FILE"})
        else:
            # data = pd.read_csv (my_uploaded_file,encoding="ISO-8859-1",warn_bad_lines=False, error_bad_lines=False)
            today = date.today()
            folder='file_uploads/'+str(today)
            fs = FileSystemStorage(location=folder) #defaults to   MEDIA_ROOT  
            filen = fs.save(my_uploaded_file.name,my_uploaded_file)
            file_url = fs.url(filen)
            print(file_url)
            obj=uploaded_csv(User_Id=request.user,filename=filen,filepath=folder+file_url,status='5')
            obj.save()
            






        



        
           
        


        

    return render(request,template_name="uploadcsv.html")






# @login_required
# def uploadcsv(request):
#     if request.method == "POST":
#         print("yes file found")
#         my_uploaded_file = request.FILES['my_uploaded_file']
#         if not my_uploaded_file.name.endswith('.csv'):
#             return render(request,template_name="uploadcsv.html",context={"error":"THIS IS NOT A CSV FILE"})

        
        
#         data = pd.read_csv (my_uploaded_file)
#         cols=data.columns
#         data.columns=data.columns.str.lower()
#         data.columns = data.columns.str.replace(' ', '')





#             # request.session['CURRENT_CSV']={'doc_file': data.to_json()}
#             # df = pd.DataFrame(data, columns= ['speciesid','globaluniqueidentfier','abstract','recordbasis','genus','species','subspecies','varieties','races','strains','common/vernacularnames','language','synonyms','kingdom','phylum','class','sub-class','order','family','descriptionoftaxonandphenology','diagnostic','cyclic','reporduction','migration','dispersal','associations','diseases','zygotic','somatic','gametic','karyotype','plioditylevel,genomesize','molecularchemicalmark','habitat','minaltitude','maxaltitude','latitude','longitude','biogeographiczone','province','bioticzone','state','district','taluk','place','vegetationtype','habitat','source','nativity','endemism','globaldistribution','populationbiology','indianreaddatabook','cites','iucn','threatstatus','wpa','frlhtthreatassessment','broadsubject','keywords','parts','uses','systemofmedicine','comments','others','shlokam','source','shlokamnumber','detailsofshloka','plantslistedandotherinformation','management','datasetname','datasetpurpose','datasetpurpose','datasetdescription','datasetlink','patentname','patentnumber','patentowner','patentprocess','patentdate','patentlink','typespecimen','typecollector','typedate','typelocation','typedepository','typerecord','typesource','speciesauthor','speciesreference','speciesvolume','speciesedition','speciesyearofpublication','speciescontributor','speciestype','chromosomesauthor','chromosomesreference','cromosomevolume','chormosomeedition','chromosomeyearofpubication','contributor','type','varietyauthor','varietyreference','varietyvolume','varietyyearofpublication','varietypageno','subspeciesauthor','subspeciesreference','subspeciesvolume','subspeciesyearofpublication','subspeciespageno','common/vernacularnamesauthor','common/vernacularnamesreference','common/vernacularnamesvolume','common/vernacularnamesyearofpublication','common/vernacularnamespublication','synonymsauthor','synonymsreference','synonymsyearofpublication','shlokaauthor','shlokavolume',
#             # 'shlokayearofpublication','shlokapageno','shlokaverse','shlokapublisher','shlokareference','documents','htmlpagelinks','speceieswebservicesapi','chromosomesdatalinkfromdgrip','typemethod','assocaitedtaxa','specimensex','specimenweight','samplingeffort','temperature','wsmlurlforspeciesdistribution','herabarium','linediagram','photographs','imagepath','source','collector'])
#         df=pd.DataFrame(data,columns=data.columns)
#         # print(df)
        
#         ar= df.to_numpy()
        
#         json_list = jsonn.dumps(data.columns.tolist())

#         data_html = data.to_html()
#         co=len(df.columns)


#         res=cols
        
#         try:

#             cursor = connection.cursor()

#             s="Create table test_table  (id serial PRIMARY KEY,"
            

#             for i in res:
#                 s=s+i+" VARCHAR  ,"
            

#             s=s+"ibin_speciesid VARCHAR );"

        
#             print(s)


#             cursor.execute(s)

        

#             for k in ar:
#                 a=[]
#                 s='INSERT INTO test_table ('
#                 for i in res:
#                     s=s+'' +i+','
#                 s=s[:len(s)-1]+') VALUES ('
#                 for j in k:
#                     a.append(j)
#                     s=s+' %s ,'
#                 s=s[:len(s)-1]+" )"
#                 cursor.execute(s,a)

            
#             cursor.execute(''' update test_table set ibin_speciesid=(select ibin_speciesid from ibin_specieslist where test_table.species_name=ibin_specieslist.ibin_speciesname)  ''')

#             todo_obj = query_to_dicts(''' select * from test_table where ibin_speciesid is not null ''')

#             res=res.tolist()
#             res.append("ibin_speciesid")
#             with open('E:/csv/species_with_id.csv', 'w', newline='') as file:
#                 writer = csv.writer(file)
#                 writer.writerow(res)
#                 for obj in todo_obj:
#                     tes=[]
#                     for i in res:
#                         tes.append(obj[i])
                    
#                     writer.writerow(tes)

#             cursor.execute(''' delete from test_table where ibin_speciesid is not null ''')


#             todo_obj = query_to_dicts(''' select * from test_table inner join plant_synonyms_col on test_table.species_name = plant_synonyms_col.synonym_species ''')

            
#             res.append("ibin_speciesid")
#             with open('E:/csv/species_with_synoniams.csv', 'w', newline='') as file:
#                 writer = csv.writer(file)
#                 writer.writerow(res)
#                 for obj in todo_obj:
#                     tes=[]
#                     for i in res:
#                         tes.append(obj[i])
                    
#                     writer.writerow(tes)


#             cursor.execute('''  DELETE  FROM test_table where test_table.id in( select test_table.id from test_table inner join plant_synonyms_col on test_table.species_name = plant_synonyms_col.synonym_species)  ''')         

            

#             todo_obj = query_to_dicts(''' select * from test_table ''')

            
#             res.append("ibin_speciesid")
#             with open('E:/csv/species_with_no_data.csv', 'w', newline='') as file:
#                 writer = csv.writer(file)
#                 writer.writerow(res)
#                 for obj in todo_obj:
#                     tes=[]
#                     for i in res:
#                         tes.append(obj[i])
                    
#                     writer.writerow(tes)



#             cursor.execute(''' drop table test_table ''')



            





#             context = {'loaded_data': data_html,'data':df.itertuples(),'colums':cols,"array":ar,"length":co,"json_list":json_list}

            
            


#             return render(request,template_name="uploadcsv.html",context=context)
        
#         except:
#             cursor.execute(''' drop table IF EXISTS test_table ''')



#         # datamodel = IBINDataModel(user_id=request.user,validated=False)
#         # datamodel.save()
#         # for row in df.itertuples():
            
#         #     belement=BaseElement(RecordID=datamodel,SpeciesID=row.SpeciesID,GlobalUniqueIdentfier=row.GlobalUniqueIdentfier,Abstract=row.Abstract,RecordBasis=row.RecordBasis)
#         #     belement.save()

#         #     obj=NomenclatureAndClassification(Genus=row.Genus,Specie=row.Specie,Race=row.Race,Strains=row.Strains,CommonName=row.CommonName,Language=row.Language,Kingdom=row.Kingdom,Phylum=row.Phylum,Class=row.Class,Order=row.Order,Family=row.Family)
#         #     obj.RecordID=datamodel
#         #     obj.save()
           
        


        

#     return render(request,template_name="uploadcsv.html")

# # def cehck_file_type(myfile):
# #     print(chardet.detect(myfile.read()))

@csrf_exempt
def save_form(request):
    
    if request.method == 'POST':
       
        
        request_data=json.loads(request.body)
        
        for k in request_data:
            res=k.keys()
            break
        
        

        cursor = connection.cursor()

        s="Create table test_table  (id serial PRIMARY KEY,"
        

        for i in res:
            s=s+i+" VARCHAR  ,"
          

        s=s[:len(s)-1]+" );"

       
        print(s)


        cursor.execute(s)

    

        for k in request_data:
            a=[]
            s='INSERT INTO test_table ('
            for i in res:
                s=s+'' +i+','
            s=s[:len(s)-1]+') VALUES ('
            for i in res:
                a.append(k[i])
                s=s+' %s ,'
            s=s[:len(s)-1]+" )"

            print(s)

            cursor.execute(s,a)

        

        todo_obj = query_to_dicts('''SELECT * FROM test_table''')

        with open('E:/protagonist.csv', 'w', newline='') as file:
            writer = csv.writer(file)
            writer.writerow(res)
            for obj in todo_obj:
                tes=[]
                for i in res:
                    tes.append(obj[i])
                
                writer.writerow(tes)
        

        cursor.execute(''' drop table test_table ''')
                




















        # datamodel = IBINDataModel(user_id=request.user,validated=False)
        # datamodel.save()
        
        # for k in request_data:
        #     # belement=BaseElement(RecordID=datamodel,SpeciesID=k.get('SpeciesID'),GlobalUniqueIdentfier=k.get('GlobalUniqueIdentfier'),Abstract=k.get('Abstract'),RecordBasis=k.get('RecordBasis'))
        #     # belement.save()
        #     try:
        #         nandcelement=NomenclatureAndClassification(RecordID=datamodel,Genus=k.get('genus'),Specie=k.get('specie'),Subspecie=k.get('subspecie'),Varietie=k.get("varietie"),Race=k.get("race"),Strains=k.get("strains"),CommonName=k.get("commonname"),Language=k.get("language"),Kingdom=k.get("kingdom"),Phylum=k.get("phylum"),Class=k.get("class"),SubClass=k.get("subclass"),Order=k.get("order"),Family=k.get("family"))
        #         nandcelement.save()

        #         Desc = Description(RecordID=datamodel, DescripionOfTaxonAndPhenology=k.get(
        #             'descripionoftaxonandphenology'), Diagnostic=k.get('diagnostic'))
        #         Desc.save()

        #         NatHistory = NaturalHistory(RecordID=datamodel, Cyclicity=k.get('cyclicity'), Reproduction=k.get('reproduction'), Migration=k.get(
        #             'migration'), Dispersal=k.get('dispersal'), Association=k.get('association'), Disesase=k.get('disesase'))
        #         NatHistory.save()

        #         GeneticMolecularAnd = GeneticMolecularAndBiochemicalCharacterisation(RecordID=datamodel, Somatic=k.get('somatic'), Zygotic=k.get('zygotic'), Gametic=k.get('gametic'), PloidyLevel=k.get(
        #             'ploidylevel'), GenomeSize=k.get('genomesize'), MolecularChemicalMarkers=k.get('molecularchemicalmarkers'), InputsFromCSIRCIMAP=k.get('inputsfromcsircimap'))
        #         GeneticMolecularAnd.save()

        #         HabitatLocationAnd = HabitatLocationAndDistribution(RecordID=datamodel, Habitat=k.get('habitat'), MinAltitude=k.get('minaltitude'), MaxAltitude=k.get('maxaltitude'), Latitude=k.get('latitude'), Longitude=k.get('longitude'), BiogeographicZoneAndProvince=k.get('biogeographiczoneandprovince'), BioticZone=k.get(
        #             'bioticzone'), state=k.get('state'), District=k.get('district'), Taluk=k.get('taluk'), Place=k.get('place'), VegetationType=k.get('vegetationtype'), Habit=k.get('habit'), Source=k.get('source'), Nativity=k.get('nativity'), Endemism=k.get('endemism'), GlobalDistribution=k.get('globaldistribution'))
        #         HabitatLocationAnd.save()

        #         DemographyAndConv = DemographyAndConversion(RecordID=datamodel, PopulationBiology=k.get('populationbiology'), IndianRedDataBook=k.get('indianreddatabook'), Cities=k.get(
        #             'cities'), IUCN=k.get('iucn'), ThreatStatus=k.get('threatstatus'), WPA=k.get('wpa'), FRIHTThreatAssessment=k.get('frihtthreatassessment'), Legislation=k.get('legislation'))
        #         DemographyAndConv.save()

        #         UsesAndMan = UsesAndManagement(RecordID=datamodel, BroadSubject=k.get('broadsubject'), Keywords=k.get('keywords'), Parts=k.get('parts'), Uses=k.get('uses'), SystemOfMedicine=k.get('systemofmedicine'), Comments=k.get('comments'), Others=k.get('others'), FolkKnowledge=k.get(
        #             'folkknowledge'), Shlokam=k.get('shlokam'), Source=k.get('source'), ShlokaNumber=k.get('shlokanumber'), DetailsofShloka=k.get('detailsofshloka'), PlantslistedAndOthers=k.get('plantslistedandothers'), OtherInformation=k.get('otherinformation'), Management=k.get('management'))
        #         UsesAndMan.save()

        #         DatasetDet = DatasetDetail(RecordID=datamodel, DatasetName=k.get('datasetname'), DatasetPurpose=k.get(
        #             'datasetpurpose'), DatasaetDescription=k.get('datasaetdescription'), DatasetLink=k.get('datasetlink'))
        #         DatasetDet.save()

        #         PatentDet = PatentDetail(RecordID=datamodel, PatentName=k.get('patentname'), PatentNumber=k.get('patentnumber'), PatentOwner=k.get(
        #             'patentowner'), ProductProcess=k.get('productprocess'), PatentDate=k.get('patentdate'), PatentLink=k.get('patentlink'))
        #         PatentDet.save()

        #         SpecimenDet = SpecimenDetail(RecordID=datamodel, TypeSpecimen=k.get('typespecimen'), TypeCollector=k.get('typecollector'), TypeDate=k.get(
        #             'typedate'), TypeLocation=k.get('typelocation'), TypeDepository=k.get('typedepository'), TypeRecord=k.get('typerecord'), TypeSource=k.get('typesource'))
        #         SpecimenDet.save()

        #         InformationList = InformationListing(RecordID=datamodel, SpeciesAndChromosomeAuthor=k.get('speciesandchromosomeauthor'), SpeciesAndChromosomeReference=k.get('speciesandchromosomereference'), SpeciesAndChromosomeVolume=k.get('speciesandchromosomevolume'), SpeciesAndChromosomeEdition=k.get('speciesandchromosomeedition'), SpeciesAndChromosomeYearOfPublication=k.get('speciesandchromosomeyearofpublication'), SpeciesAndChromosomeContributor=k.get('speciesandchromosomecontributor'), SpeciesAndChromosomeType=k.get('speciesandchromosometype'), VarietyAndSpeciesAuthor=k.get('varietyandspeciesauthor'), VarietyAndSpeciesReference=k.get('varietyandspeciesreference'), VarietyAndSpeciesVolume=k.get('varietyandspeciesvolume'), VarietyAndSpeciesPageno=k.get('varietyandspeciespageno'), VarietyAndSpeciesYearOfPublication=k.get('varietyandspeciesyearofpublication'), VernicularNamesAuthor=k.get(
        #             'vernicularnamesauthor'), VernicularNamesReference=k.get('vernicularnamesreference'), VernicularNamesVolume=k.get('vernicularnamesvolume'), VernicularNamesPublication=k.get('vernicularnamespublication'), VernicularNamesYearOfPublication=k.get('vernicularnamesyearofpublication'), SynonymsAuthor=k.get('synonymsauthor'), SynonymsReference=k.get('synonymsreference'), SynonymsYearOfPublication=k.get('synonymsyearOfpublication'), ShlokaAuthor=k.get('shlokaauthor'), ShlokaReference=k.get('shlokareference'), ShlokaYearOfPublication=k.get('shlokayearofpublication'), ShlokaPageno=k.get('shlokapageno'), ShlokaPublisher=k.get('shlokapublisher'), ShlokaVerse=k.get('shlokaverse'), Documents=k.get('documents'), HtmlPageLinks=k.get('htmlpagelinks'), SpeciesWebServicesApi=k.get('specieswebservicesapi'), ChromosomesDataLinkFromdGRIP=k.get('chromosomesdatalinkfromdgrip'))
        #         InformationList.save()

        #         ObservationalInfp = ObservationalInfromation(RecordID=datamodel, TypeMethod=k.get('typemethod'), AssocatedTaxa=k.get('assocatedtaxa'), SpecimenSex=k.get(
        #             'specimensex'), SpecimenWeight=k.get('specimenweight'), SampleEffort=k.get('samplefffort'), Temperature=k.get('temperature'))
        #         ObservationalInfp.save()

        #         WMSurl = WMSURL(RecordID=datamodel, WMSURLforSpeciesDistribution=k.get(
        #             ' wmsurlforspeciesdistribution'))
        #         WMSurl.save()

        #         Multimed = Multimedia(RecordID=datamodel, Source=k.get(
        #             'source'), Collector=k.get('collector'))
        #         Multimed.save()

        #         DataQuality = DataQualityStatu(RecordID=datamodel, CurationLevel=k.get(
        #             'curationlevel'), CuratedBy=k.get('curatedby'), CurationDate=k.get('curationdate'))
        #         DataQuality.save()

        #         Image = Images(RecordID=datamodel, Type=k.get(
        #             'type'), image=k.get('image'))
        #         Image.save()
        #     except:
        #         data = {
        #             'error': 'error saving csv data make sure all the required field is in csv',
        #             }
        #         dump = json.dumps(data)
               
        #         return HttpResponse(status=500,content=dump,content_type='application/json')
            
        #     data = {
        #             'message':'data saved successfully'
        #             }
        #     dump = json.dumps(data)
            
        #     return  HttpResponse(status=200,content=dump, content_type='application/json')

            
             

        
            



    return render(request,template_name="save_form.html")





def query_to_dicts(query_string, *query_args):

    #log.debug(str(dir(connection)))
    cursor = connection.cursor()
    #log.debug(str(dir(cursor)))
    cursor.execute(query_string, query_args)
    #log.debug(cursor.rowcount)log
    col_names = [desc[0] for desc in cursor.description]
    #log.debug(str(col_names))
    while True:
        row = cursor.fetchone()
        if row is None:
            break
        
        row_dict = dict(zip(col_names, row))
        yield row_dict
    
    return




def fileuploades(request):
    cu=uploaded_csv.objects.filter(User_Id=request.user)

    context={
        "data":cu,
        
    }

    return render(request,"fileuploades.html",context)






def autocomp(request):
    if 'term' in request.GET:
        print("inside")
       
        qs=IbinSpecieslist.objects.filter(genus=request.GET.get('term'))
        exp=list()
        for prod in qs:
            exp.append(prod.genus)
        return JsonResponse(exp,safe=False)
from django.urls import path

from . import views

urlpatterns = [
    path("home/",views.Home,name="plantsystemhome"),
    path('Filldetails/',views.input_field,name='plantsystem'),
    path('uploadCSV/',views.uploadcsv,name='plantsystemuploadCsv'),
    path('submit/',views.successsubmit,name='successsubmit'),
    path('saved_data/',views.save_form,name='save_form'),
    path('fileuploaded/',views.fileuploades,name='fileuploades'),
    path('auto',views.autocomp,name='autocomp'),
]

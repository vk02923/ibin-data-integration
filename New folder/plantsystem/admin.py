from django.contrib import admin
from .models import *
from rangefilter.filters import DateRangeFilter, DateTimeRangeFilter


# Register your models here.
# admin.site.register(IBINDataModel)
# admin.site.register(BaseElement)
# admin.site.register(NomenclatureAndClassification)
# admin.site.register(Description)
# admin.site.register(NaturalHistory)
# admin.site.register(GeneticMolecularAndBiochemicalCharacterisation)
# admin.site.register(HabitatLocationAndDistribution)
# admin.site.register(DemographyAndConversion)
# admin.site.register(UsesAndManagement)
# admin.site.register(DatasetDetail)
# admin.site.register(PatentDetail)
# admin.site.register(SpecimenDetail)
# admin.site.register(InformationListing)
# admin.site.register(ObservationalInfromation)
# admin.site.register(WMSURL)
# admin.site.register(Multimedia)
# admin.site.register(DataQualityStatu)
# admin.site.register(Images)
# admin.site.register(uploaded_csv)
@admin.register(uploaded_csv)
class PostAdmin(admin.ModelAdmin):
    list_filter = (
        ('date', DateRangeFilter),
    )

    # If you would like to add a default range filter
    # method pattern "get_rangefilter_{field_name}_default"
    def get_rangefilter_created_at_default(self, request):
        return (datetime.date.today, datetime.date.today)

    # If you would like to change a title range filter
    # method pattern "get_rangefilter_{field_name}_title"
    def get_rangefilter_created_at_title(self, request, field_path):
        return 'custom title'
# admin.site.register(process_report)



@admin.register(uploaded_records)
class PostAdmin(admin.ModelAdmin):
    list_filter = (
        ('date', DateRangeFilter),
    )

    # If you would like to add a default range filter
    # method pattern "get_rangefilter_{field_name}_default"
    def get_rangefilter_created_at_default(self, request):
        return (datetime.date.today, datetime.date.today)

    # If you would like to change a title range filter
    # method pattern "get_rangefilter_{field_name}_title"
    def get_rangefilter_created_at_title(self, request, field_path):
        return 'custom title'
# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class LoginCustomuser(models.Model):
    organisation_name = models.CharField(max_length=100)
    piname = models.CharField(max_length=100)
    telephone = models.CharField(max_length=10)
    qualification = models.TextField()
    expertise_in_which_family = models.TextField()
    project_name = models.TextField()
    user = models.OneToOneField('AuthUser', models.DO_NOTHING)
    image = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Login_customuser'


class Amphibian(models.Model):
    ibin_speciesname = models.TextField(blank=True, null=True)
    habit_and_habitat = models.TextField(db_column='HABIT and HABITAT', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    distribution = models.TextField(db_column='Distribution', blank=True, null=True)  # Field name made lowercase.
    diagnostic_features = models.TextField(db_column='DIAGNOSTIC FEATURES', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    colour = models.TextField(db_column='Colour', blank=True, null=True)  # Field name made lowercase.
    breeding_period = models.TextField(db_column='Breeding period', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    extent_of_occurrence = models.TextField(db_column='Extent of occurrence', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    area_of_occupancy = models.TextField(db_column='Area of occupancy', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    area_of_occurrence = models.TextField(db_column='Area of occurrence', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    field_10 = models.TextField(blank=True, null=True)
    altitude = models.TextField(db_column='Altitude', blank=True, null=True)  # Field name made lowercase.
    population = models.TextField(db_column='Population', blank=True, null=True)  # Field name made lowercase.
    conservation_threat_status = models.TextField(db_column='Conservation/ Threat Status', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    common_name = models.TextField(db_column='Common Name', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    miscellaneous_information = models.TextField(db_column='Miscellaneous Information', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    literature = models.TextField(db_column='Literature', blank=True, null=True)  # Field name made lowercase.
    water_body = models.TextField(db_column='Water body', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    forest_type = models.TextField(db_column='Forest type', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    breeding_system = models.TextField(db_column='Breeding system', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ibin_speciesid = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'amphibian'


class AmphibianFinal(models.Model):
    ibin_speciesname = models.TextField(blank=True, null=True)
    habit_and_habitat = models.TextField(db_column='HABIT and HABITAT', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    distribution = models.TextField(db_column='Distribution', blank=True, null=True)  # Field name made lowercase.
    diagnostic_features = models.TextField(db_column='DIAGNOSTIC FEATURES', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    colour = models.TextField(db_column='Colour', blank=True, null=True)  # Field name made lowercase.
    breeding_period = models.TextField(db_column='Breeding period', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    extent_of_occurrence = models.TextField(db_column='Extent of occurrence', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    area_of_occupancy = models.TextField(db_column='Area of occupancy', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    area_of_occurrence = models.TextField(db_column='Area of occurrence', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    altitude = models.TextField(db_column='Altitude', blank=True, null=True)  # Field name made lowercase.
    population = models.TextField(db_column='Population', blank=True, null=True)  # Field name made lowercase.
    conservation_threat_status = models.TextField(db_column='Conservation/ Threat Status', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    common_name = models.TextField(db_column='Common Name', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    miscellaneous_information = models.TextField(db_column='Miscellaneous Information', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    literature = models.TextField(db_column='Literature', blank=True, null=True)  # Field name made lowercase.
    water_body = models.TextField(db_column='Water body', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    forest_type = models.TextField(db_column='Forest type', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    breeding_system = models.TextField(db_column='Breeding system', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ibin_speciesid = models.TextField(blank=True, null=True)
    language = models.TextField(blank=True, null=True)
    type = models.TextField(blank=True, null=True)
    state = models.TextField(blank=True, null=True)
    lat = models.TextField(blank=True, null=True)
    long = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'amphibian_final'


class AmphibiansColor(models.Model):
    ibin_speciesid = models.CharField(max_length=200, blank=True, null=True)
    color = models.CharField(max_length=850, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'amphibians_color'


class AmphibiansCommonName(models.Model):
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    language = models.CharField(max_length=250, blank=True, null=True)
    common_name = models.CharField(max_length=850, blank=True, null=True)
    type = models.CharField(max_length=250, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'amphibians_common_name'


class AmphibiansDiagnostic(models.Model):
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    diagnostic_character = models.CharField(max_length=1050, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'amphibians_diagnostic'


class AmphibiansDistribution(models.Model):
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    state = models.CharField(max_length=250, blank=True, null=True)
    district = models.CharField(max_length=250, blank=True, null=True)
    lat = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    long = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'amphibians_distribution'


class AmphibiansImages(models.Model):
    ibin_speciesid = models.CharField(max_length=400, blank=True, null=True)
    img_path = models.CharField(max_length=600, blank=True, null=True)
    ibin_speciesname = models.CharField(max_length=350, blank=True, null=True)
    contributor = models.CharField(max_length=350, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'amphibians_images'


class AmphibiansList(models.Model):
    ibin_speciesname_old = models.CharField(max_length=350, blank=True, null=True)
    ibin_speciesname_new = models.CharField(max_length=350, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'amphibians_list'


class AncientKnowledge(models.Model):
    verse_no = models.CharField(max_length=100, blank=True, null=True)
    source = models.CharField(max_length=200, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    description = models.CharField(max_length=650, blank=True, null=True)
    audio = models.CharField(max_length=650, blank=True, null=True)
    contributor = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ancient_knowledge'


class AncientKnowledgeOld(models.Model):
    audio = models.CharField(max_length=150, blank=True, null=True)
    description = models.CharField(max_length=650, blank=True, null=True)
    details = models.CharField(max_length=650, blank=True, null=True)
    source = models.CharField(max_length=200, blank=True, null=True)
    verse = models.CharField(max_length=100, blank=True, null=True)
    contributor = models.CharField(max_length=200, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ancient_knowledge_old'


class AnimalCons(models.Model):
    ibin_speciesid = models.CharField(primary_key=True, max_length=250)
    iucn_status = models.CharField(max_length=250, blank=True, null=True)
    iucn_assessment_year = models.CharField(max_length=250, blank=True, null=True)
    iirs_verifydate = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'animal_cons'


class AnimalGenus(models.Model):
    genus_id = models.CharField(primary_key=True, max_length=250)
    genus_name = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'animal_genus'


class AnimalSynonyms(models.Model):
    synonyms = models.CharField(max_length=500, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=650, blank=True, null=True)
    source = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'animal_synonyms'


class AnimalTaxonomy(models.Model):
    ibin_speciesid = models.CharField(primary_key=True, max_length=250)
    kingdom_id = models.CharField(max_length=250, blank=True, null=True)
    phylum_id = models.CharField(max_length=250, blank=True, null=True)
    class_id = models.CharField(max_length=250, blank=True, null=True)
    order_id = models.CharField(max_length=250, blank=True, null=True)
    superfamily_id = models.CharField(max_length=250, blank=True, null=True)
    family_id = models.CharField(max_length=250, blank=True, null=True)
    genus_id = models.CharField(max_length=250, blank=True, null=True)
    species_id = models.CharField(max_length=250, blank=True, null=True)
    subfamily_id = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'animal_taxonomy'


class AnimalTaxonomyTemp(models.Model):
    ibin_speciesid = models.CharField(primary_key=True, max_length=250)
    kingdom_id = models.CharField(max_length=250, blank=True, null=True)
    phylum_id = models.CharField(max_length=250, blank=True, null=True)
    class_id = models.CharField(max_length=250, blank=True, null=True)
    order_id = models.CharField(max_length=250, blank=True, null=True)
    superfamily_id = models.CharField(max_length=250, blank=True, null=True)
    family_id = models.CharField(max_length=250, blank=True, null=True)
    genus_id = models.CharField(max_length=250, blank=True, null=True)
    species_id = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'animal_taxonomy_temp'


class ArchaeaGenus(models.Model):
    genus_id = models.CharField(primary_key=True, max_length=250)
    genus_name = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'archaea_genus'


class ArchaeaImages(models.Model):
    ibin_speciesid = models.CharField(max_length=400, blank=True, null=True)
    img_path = models.CharField(max_length=600, blank=True, null=True)
    ibin_speciesname = models.CharField(max_length=350, blank=True, null=True)
    contributor = models.CharField(max_length=350, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'archaea_images'


class ArchaeaTaxonomy(models.Model):
    ibin_speciesid = models.CharField(primary_key=True, max_length=250)
    kingdom_id = models.CharField(max_length=250, blank=True, null=True)
    phylum_id = models.CharField(max_length=250, blank=True, null=True)
    class_id = models.CharField(max_length=250, blank=True, null=True)
    order_id = models.CharField(max_length=250, blank=True, null=True)
    family_id = models.CharField(max_length=250, blank=True, null=True)
    genus_id = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'archaea_taxonomy'


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class AuthorityMaster(models.Model):
    id = models.IntegerField()
    authority_name = models.CharField(max_length=200)
    authority_display_name = models.CharField(max_length=200)
    designation = models.CharField(max_length=200)
    organization = models.CharField(max_length=200)
    option1 = models.CharField(max_length=200, blank=True, null=True)
    option2 = models.CharField(max_length=200, blank=True, null=True)
    option3 = models.CharField(max_length=200, blank=True, null=True)
    status = models.CharField(max_length=10)
    ins_by = models.CharField(max_length=200)
    ins_date = models.CharField(max_length=200)
    ins_time = models.CharField(max_length=200)
    ins_ip = models.CharField(max_length=200)

    class Meta:
        managed = False
        db_table = 'authority_master'


class BacteriaGenus(models.Model):
    genus_id = models.CharField(primary_key=True, max_length=250)
    genus_name = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'bacteria_genus'


class BacteriaImages(models.Model):
    ibin_speciesid = models.CharField(max_length=400, blank=True, null=True)
    img_path = models.CharField(max_length=600, blank=True, null=True)
    ibin_speciesname = models.CharField(max_length=350, blank=True, null=True)
    contributor = models.CharField(max_length=350, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'bacteria_images'


class BacteriaTaxonomy(models.Model):
    ibin_speciesid = models.CharField(primary_key=True, max_length=250)
    kingdom_id = models.CharField(max_length=250, blank=True, null=True)
    phylum_id = models.CharField(max_length=250, blank=True, null=True)
    class_id = models.CharField(max_length=250, blank=True, null=True)
    order_id = models.CharField(max_length=250, blank=True, null=True)
    family_id = models.CharField(max_length=250, blank=True, null=True)
    genus_id = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'bacteria_taxonomy'


class BatCmnname(models.Model):
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    common_name = models.CharField(max_length=850, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'bat_cmnname'


class BatImages(models.Model):
    ibin_speciesid = models.CharField(max_length=400, blank=True, null=True)
    img_path = models.CharField(max_length=600, blank=True, null=True)
    ibin_speciesname = models.CharField(max_length=350, blank=True, null=True)
    contributor = models.CharField(max_length=35, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'bat_images'


class BatsTaxonomy(models.Model):
    scnm = models.CharField(primary_key=True, max_length=250)
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    kingdom_id = models.CharField(max_length=250, blank=True, null=True)
    phylum_id = models.CharField(max_length=250, blank=True, null=True)
    class_id = models.CharField(max_length=250, blank=True, null=True)
    order_id = models.CharField(max_length=250, blank=True, null=True)
    superfamily_id = models.CharField(max_length=250, blank=True, null=True)
    family_id = models.CharField(max_length=250, blank=True, null=True)
    genus_id = models.CharField(max_length=250, blank=True, null=True)
    species_id = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'bats_taxonomy'


class BirdsBioticZone(models.Model):
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    biotic_zone = models.CharField(max_length=250, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'birds_biotic_zone'


class BirdsBreedingSeason(models.Model):
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    breeding_season = models.CharField(max_length=450, blank=True, null=True)
    remarks = models.CharField(max_length=450, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'birds_breeding_season'


class BirdsCommonname(models.Model):
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    common_name = models.CharField(max_length=1050, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'birds_commonname'


class BirdsDiagnostic(models.Model):
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    diagnostic = models.CharField(max_length=1050, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'birds_diagnostic'


class BirdsDistribution(models.Model):
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    location = models.CharField(max_length=250, blank=True, null=True)
    lat = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    long = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'birds_distribution'


class BirdsEndemic(models.Model):
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    endemic = models.CharField(max_length=450, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'birds_endemic'


class BirdsFoodpref(models.Model):
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    foodpref = models.CharField(max_length=1050, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'birds_foodpref'


class BirdsHabit(models.Model):
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    habit = models.CharField(max_length=450, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'birds_habit'


class BirdsHabitat(models.Model):
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    habitat = models.CharField(max_length=1050, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'birds_habitat'


class BirdsImages(models.Model):
    path = models.CharField(max_length=250, blank=True, null=True)
    ibin_speciesname = models.CharField(max_length=1050, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'birds_images'


class BirdsOccurrences(models.Model):
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    occurrences = models.CharField(max_length=450, blank=True, null=True)
    comment = models.CharField(max_length=450, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'birds_occurrences'


class BirdsOtherInfo(models.Model):
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    common_name = models.CharField(max_length=450, blank=True, null=True)
    grop = models.CharField(max_length=250, blank=True, null=True)
    status = models.CharField(max_length=250, blank=True, null=True)
    sex = models.CharField(max_length=250, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'birds_other_info'


class BirdsRemarks(models.Model):
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    remarks = models.CharField(max_length=850, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'birds_remarks'


class BirdsSeasonald(models.Model):
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    seasonald = models.CharField(max_length=1050, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'birds_seasonald'


class BirdsSource(models.Model):
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    source = models.CharField(max_length=1050, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'birds_source'


class BirdsStatusd(models.Model):
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    statusd = models.CharField(max_length=1050, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'birds_statusd'


class BirdsTaxonomy(models.Model):
    ibin_scientificname = models.CharField(primary_key=True, max_length=250)
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    kingdom_id = models.CharField(max_length=250, blank=True, null=True)
    phylum_id = models.CharField(max_length=250, blank=True, null=True)
    class_id = models.CharField(max_length=250, blank=True, null=True)
    order_id = models.CharField(max_length=250, blank=True, null=True)
    family_id = models.CharField(max_length=250, blank=True, null=True)
    genus_id = models.CharField(max_length=250, blank=True, null=True)
    species_id = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'birds_taxonomy'


class BirdsVoice(models.Model):
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    voice = models.CharField(max_length=1050, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'birds_voice'


class BsiPlants(models.Model):
    ibin_speciesname = models.CharField(max_length=35000, blank=True, null=True)
    status = models.CharField(max_length=3500, blank=True, null=True)
    distribution = models.CharField(max_length=35000, blank=True, null=True)
    habitat_ecology = models.CharField(max_length=3500, blank=True, null=True)
    conservation_measures = models.CharField(max_length=3500, blank=True, null=True)
    conservation_measures_proposed = models.CharField(max_length=3500, blank=True, null=True)
    biology_potential = models.CharField(max_length=3500, blank=True, null=True)
    cultivation = models.CharField(max_length=3500, blank=True, null=True)
    description = models.CharField(max_length=13500, blank=True, null=True)
    reference = models.CharField(max_length=135000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'bsi_plants'


class ButterfliesCmnname(models.Model):
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    common_name = models.CharField(max_length=850, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'butterflies_cmnname'


class ButterfliesImages(models.Model):
    ibin_speciesid = models.CharField(max_length=400, blank=True, null=True)
    img_path = models.CharField(max_length=600, blank=True, null=True)
    ibin_speciesname = models.CharField(max_length=350, blank=True, null=True)
    contributor = models.CharField(max_length=35, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'butterflies_images'


class ButterflyStructure(models.Model):
    antenna = models.CharField(max_length=500, blank=True, null=True)
    family = models.CharField(max_length=500, blank=True, null=True)
    field1 = models.CharField(max_length=500, blank=True, null=True)
    species = models.CharField(max_length=500, blank=True, null=True)
    scientific_name = models.CharField(max_length=500, blank=True, null=True)
    dorsalcolor = models.CharField(max_length=500, blank=True, null=True)
    black = models.CharField(max_length=500, blank=True, null=True)
    brown = models.CharField(max_length=500, blank=True, null=True)
    blue = models.CharField(max_length=500, blank=True, null=True)
    green = models.CharField(max_length=500, blank=True, null=True)
    grey = models.CharField(max_length=500, blank=True, null=True)
    orange = models.CharField(max_length=500, blank=True, null=True)
    red = models.CharField(max_length=500, blank=True, null=True)
    white = models.CharField(max_length=500, blank=True, null=True)
    yellow = models.CharField(max_length=500, blank=True, null=True)
    ventralcolor = models.CharField(max_length=500, blank=True, null=True)
    black1 = models.CharField(max_length=500, blank=True, null=True)
    brown1 = models.CharField(max_length=500, blank=True, null=True)
    blue1 = models.CharField(max_length=500, blank=True, null=True)
    green1 = models.CharField(max_length=500, blank=True, null=True)
    grey1 = models.CharField(max_length=500, blank=True, null=True)
    orange1 = models.CharField(max_length=500, blank=True, null=True)
    red1 = models.CharField(max_length=500, blank=True, null=True)
    white1 = models.CharField(max_length=500, blank=True, null=True)
    yellow1 = models.CharField(max_length=500, blank=True, null=True)
    size = models.CharField(max_length=500, blank=True, null=True)
    tail = models.CharField(max_length=500, blank=True, null=True)
    eyespot = models.CharField(max_length=500, blank=True, null=True)
    bands = models.CharField(max_length=500, blank=True, null=True)
    patches = models.CharField(max_length=500, blank=True, null=True)
    stripes = models.CharField(max_length=500, blank=True, null=True)
    wavy_markings = models.CharField(max_length=500, blank=True, null=True)
    margin = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'butterfly_structure'


class CaptchaCaptchastore(models.Model):
    challenge = models.CharField(max_length=32)
    response = models.CharField(max_length=32)
    hashkey = models.CharField(unique=True, max_length=40)
    expiration = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'captcha_captchastore'


class ChemicalsPlantImage(models.Model):
    ibin_speciesid = models.TextField(blank=True, null=True)
    image_path = models.TextField(blank=True, null=True)
    contributor = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'chemicals_plant_image'


class ChemicalsPlants(models.Model):
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    species_name = models.CharField(max_length=250, blank=True, null=True)
    chemical_constituents = models.CharField(max_length=25000, blank=True, null=True)
    pharmacology = models.CharField(max_length=25000, blank=True, null=True)
    mol_2dfile = models.CharField(max_length=250, blank=True, null=True)
    mol_3dfile = models.CharField(max_length=250, blank=True, null=True)
    structure_name = models.CharField(max_length=250, blank=True, null=True)
    structure_type = models.CharField(max_length=250, blank=True, null=True)
    pubchem_url = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'chemicals_plants'


class ChromAnaphase(models.Model):
    ibin_speciesid = models.CharField(primary_key=True, max_length=250)
    fn = models.CharField(max_length=250, blank=True, null=True)
    anaphase = models.CharField(max_length=25000, blank=True, null=True)
    source = models.CharField(max_length=550, blank=True, null=True)
    contributor = models.CharField(max_length=100, blank=True, null=True)
    web_source = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'chrom_anaphase'


class ChromBanding(models.Model):
    ibin_speciesid = models.CharField(primary_key=True, max_length=250)
    fn = models.CharField(max_length=250, blank=True, null=True)
    banding = models.CharField(max_length=25000, blank=True, null=True)
    source = models.CharField(max_length=550, blank=True, null=True)
    contributor = models.CharField(max_length=100, blank=True, null=True)
    web_source = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'chrom_banding'


class ChromBasic(models.Model):
    ibin_speciesid = models.CharField(primary_key=True, max_length=250)
    fn = models.CharField(max_length=250, blank=True, null=True)
    basic = models.CharField(max_length=25000, blank=True, null=True)
    source = models.CharField(max_length=550, blank=True, null=True)
    contributor = models.CharField(max_length=100, blank=True, null=True)
    web_source = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'chrom_basic'


class ChromChromosomal(models.Model):
    ibin_speciesid = models.CharField(primary_key=True, max_length=250)
    fn = models.CharField(max_length=250, blank=True, null=True)
    chromosomal = models.CharField(max_length=25000, blank=True, null=True)
    source = models.CharField(max_length=550, blank=True, null=True)
    contributor = models.CharField(max_length=100, blank=True, null=True)
    web_source = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'chrom_chromosomal'


class ChromDegree(models.Model):
    ibin_speciesid = models.CharField(primary_key=True, max_length=250)
    fn = models.CharField(max_length=250, blank=True, null=True)
    degree = models.CharField(max_length=25000, blank=True, null=True)
    source = models.CharField(max_length=550, blank=True, null=True)
    contributor = models.CharField(max_length=100, blank=True, null=True)
    web_source = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'chrom_degree'


class ChromDnalevel(models.Model):
    ibin_speciesid = models.CharField(primary_key=True, max_length=250)
    fn = models.CharField(max_length=250, blank=True, null=True)
    dnalevel = models.CharField(max_length=25000, blank=True, null=True)
    source = models.CharField(max_length=550, blank=True, null=True)
    contributor = models.CharField(max_length=100, blank=True, null=True)
    web_source = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'chrom_dnalevel'


class ChromEconomic(models.Model):
    ibin_speciesid = models.CharField(primary_key=True, max_length=250)
    fn = models.CharField(max_length=250, blank=True, null=True)
    economic = models.CharField(max_length=25000, blank=True, null=True)
    source = models.CharField(max_length=550, blank=True, null=True)
    contributor = models.CharField(max_length=100, blank=True, null=True)
    web_source = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'chrom_economic'


class ChromFemalemeiosis(models.Model):
    ibin_speciesid = models.CharField(primary_key=True, max_length=250)
    fn = models.CharField(max_length=250, blank=True, null=True)
    femalemeiosis = models.CharField(max_length=25000, blank=True, null=True)
    source = models.CharField(max_length=550, blank=True, null=True)
    contributor = models.CharField(max_length=100, blank=True, null=True)
    web_source = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'chrom_femalemeiosis'


class ChromFluorescent(models.Model):
    ibin_speciesid = models.CharField(primary_key=True, max_length=250)
    fn = models.CharField(max_length=250, blank=True, null=True)
    fluorescent = models.CharField(max_length=25000, blank=True, null=True)
    source = models.CharField(max_length=550, blank=True, null=True)
    contributor = models.CharField(max_length=100, blank=True, null=True)
    web_source = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'chrom_fluorescent'


class ChromGametic(models.Model):
    ibin_speciesid = models.CharField(primary_key=True, max_length=250)
    fn = models.CharField(max_length=250, blank=True, null=True)
    gametic = models.CharField(max_length=25000, blank=True, null=True)
    source = models.CharField(max_length=550, blank=True, null=True)
    contributor = models.CharField(max_length=100, blank=True, null=True)
    web_source = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'chrom_gametic'


class ChromGenomic(models.Model):
    ibin_speciesid = models.CharField(primary_key=True, max_length=250)
    fn = models.CharField(max_length=250, blank=True, null=True)
    genomic = models.CharField(max_length=25000, blank=True, null=True)
    source = models.CharField(max_length=550, blank=True, null=True)
    contributor = models.CharField(max_length=100, blank=True, null=True)
    web_source = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'chrom_genomic'


class ChromGenomicInsitu(models.Model):
    ibin_speciesid = models.CharField(primary_key=True, max_length=250)
    fn = models.CharField(max_length=250, blank=True, null=True)
    genomic_insitu = models.CharField(max_length=25000, blank=True, null=True)
    source = models.CharField(max_length=550, blank=True, null=True)
    contributor = models.CharField(max_length=100, blank=True, null=True)
    web_source = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'chrom_genomic_insitu'


class ChromHabit(models.Model):
    ibin_speciesid = models.CharField(primary_key=True, max_length=250)
    fn = models.CharField(max_length=250, blank=True, null=True)
    habit_habitat = models.CharField(max_length=25000, blank=True, null=True)
    source = models.CharField(max_length=550, blank=True, null=True)
    contributor = models.CharField(max_length=100, blank=True, null=True)
    web_source = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'chrom_habit'


class ChromIbinGenuslist(models.Model):
    filename = models.CharField(max_length=250, blank=True, null=True)
    speciesname = models.CharField(max_length=250, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=350, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'chrom_ibin_genuslist'


class ChromIbinSplist(models.Model):
    filename = models.CharField(primary_key=True, max_length=250)
    speciesname = models.CharField(max_length=250, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=350, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'chrom_ibin_splist'


class ChromInsitu(models.Model):
    ibin_speciesid = models.CharField(primary_key=True, max_length=250)
    fn = models.CharField(max_length=250, blank=True, null=True)
    insitu = models.CharField(max_length=25000, blank=True, null=True)
    source = models.CharField(max_length=550, blank=True, null=True)
    contributor = models.CharField(max_length=100, blank=True, null=True)
    web_source = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'chrom_insitu'


class ChromKaryotype(models.Model):
    ibin_speciesid = models.CharField(primary_key=True, max_length=250)
    fn = models.CharField(max_length=250, blank=True, null=True)
    karyotype = models.CharField(max_length=25000, blank=True, null=True)
    source = models.CharField(max_length=550, blank=True, null=True)
    contributor = models.CharField(max_length=100, blank=True, null=True)
    web_source = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'chrom_karyotype'


class ChromLifeForm(models.Model):
    ibin_speciesid = models.CharField(primary_key=True, max_length=250)
    fn = models.CharField(max_length=250, blank=True, null=True)
    life_form = models.CharField(max_length=25000, blank=True, null=True)
    source = models.CharField(max_length=550, blank=True, null=True)
    contributor = models.CharField(max_length=100, blank=True, null=True)
    web_source = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'chrom_life_form'


class ChromLinkage(models.Model):
    ibin_speciesid = models.CharField(primary_key=True, max_length=250)
    fn = models.CharField(max_length=250, blank=True, null=True)
    linkage = models.CharField(max_length=25000, blank=True, null=True)
    source = models.CharField(max_length=550, blank=True, null=True)
    contributor = models.CharField(max_length=100, blank=True, null=True)
    web_source = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'chrom_linkage'


class ChromList(models.Model):
    id = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'chrom_list'


class ChromMalemeiosis(models.Model):
    ibin_speciesid = models.CharField(primary_key=True, max_length=250)
    fn = models.CharField(max_length=250, blank=True, null=True)
    malemeiosis = models.CharField(max_length=25000, blank=True, null=True)
    source = models.CharField(max_length=550, blank=True, null=True)
    contributor = models.CharField(max_length=100, blank=True, null=True)
    web_source = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'chrom_malemeiosis'


class ChromNor(models.Model):
    ibin_speciesid = models.CharField(primary_key=True, max_length=250)
    fn = models.CharField(max_length=250, blank=True, null=True)
    nor = models.CharField(max_length=25000, blank=True, null=True)
    source = models.CharField(max_length=550, blank=True, null=True)
    contributor = models.CharField(max_length=100, blank=True, null=True)
    web_source = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'chrom_nor'


class ChromPloidy(models.Model):
    ibin_speciesid = models.CharField(primary_key=True, max_length=250)
    fn = models.CharField(max_length=250, blank=True, null=True)
    ploidy = models.CharField(max_length=25000, blank=True, null=True)
    source = models.CharField(max_length=550, blank=True, null=True)
    contributor = models.CharField(max_length=100, blank=True, null=True)
    web_source = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'chrom_ploidy'


class ChromPolyploidy(models.Model):
    ibin_speciesid = models.CharField(primary_key=True, max_length=250)
    fn = models.CharField(max_length=250, blank=True, null=True)
    polyploidy = models.CharField(max_length=25000, blank=True, null=True)
    source = models.CharField(max_length=550, blank=True, null=True)
    contributor = models.CharField(max_length=100, blank=True, null=True)
    web_source = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'chrom_polyploidy'


class ChromProgenitor(models.Model):
    ibin_speciesid = models.CharField(primary_key=True, max_length=250)
    fn = models.CharField(max_length=250, blank=True, null=True)
    progenitor = models.CharField(max_length=25000, blank=True, null=True)
    source = models.CharField(max_length=550, blank=True, null=True)
    contributor = models.CharField(max_length=100, blank=True, null=True)
    web_source = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'chrom_progenitor'


class ChromRefrnc(models.Model):
    fn = models.CharField(max_length=250, blank=True, null=True)
    ref = models.CharField(max_length=25000, blank=True, null=True)
    genus = models.CharField(max_length=250, blank=True, null=True)
    genus_id = models.CharField(max_length=350, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'chrom_refrnc'


class ChromSize(models.Model):
    ibin_speciesid = models.CharField(primary_key=True, max_length=250)
    fn = models.CharField(max_length=250, blank=True, null=True)
    size = models.CharField(max_length=25000, blank=True, null=True)
    source = models.CharField(max_length=550, blank=True, null=True)
    contributor = models.CharField(max_length=100, blank=True, null=True)
    web_source = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'chrom_size'


class ChromSpecialized(models.Model):
    ibin_speciesid = models.CharField(primary_key=True, max_length=250)
    fn = models.CharField(max_length=250, blank=True, null=True)
    specialized = models.CharField(max_length=25000, blank=True, null=True)
    source = models.CharField(max_length=550, blank=True, null=True)
    contributor = models.CharField(max_length=100, blank=True, null=True)
    web_source = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'chrom_specialized'


class ChromZygotic(models.Model):
    ibin_speciesid = models.CharField(primary_key=True, max_length=250)
    fn = models.CharField(max_length=250, blank=True, null=True)
    zygotic = models.CharField(max_length=25000, blank=True, null=True)
    source = models.CharField(max_length=550, blank=True, null=True)
    contributor = models.CharField(max_length=100, blank=True, null=True)
    web_source = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'chrom_zygotic'


class ChromistaGenus(models.Model):
    genus_id = models.CharField(primary_key=True, max_length=250)
    genus_name = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'chromista_genus'


class ChromistaImages(models.Model):
    ibin_speciesid = models.CharField(max_length=400, blank=True, null=True)
    img_path = models.CharField(max_length=600, blank=True, null=True)
    ibin_speciesname = models.CharField(max_length=350, blank=True, null=True)
    contributor = models.CharField(max_length=350, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'chromista_images'


class ChromistaTaxonomy(models.Model):
    ibin_speciesid = models.CharField(max_length=250)
    kingdom_id = models.CharField(max_length=250, blank=True, null=True)
    phylum_id = models.CharField(max_length=250, blank=True, null=True)
    class_id = models.CharField(max_length=250, blank=True, null=True)
    order_id = models.CharField(max_length=250, blank=True, null=True)
    family_id = models.CharField(max_length=250, blank=True, null=True)
    genus_id = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'chromista_taxonomy'


class CimapData(models.Model):
    species_name = models.CharField(max_length=250, blank=True, null=True)
    locality = models.CharField(max_length=250, blank=True, null=True)
    distribution = models.CharField(max_length=250, blank=True, null=True)
    latitute = models.CharField(max_length=250, blank=True, null=True)
    longitute = models.CharField(max_length=250, blank=True, null=True)
    minelevation = models.CharField(max_length=250, blank=True, null=True)
    maxelevation = models.CharField(max_length=250, blank=True, null=True)
    collection_date = models.CharField(max_length=250, blank=True, null=True)
    plant_desc = models.CharField(max_length=25000, blank=True, null=True)
    plant_image = models.CharField(max_length=250, blank=True, null=True)
    plant_part_image = models.CharField(max_length=250, blank=True, null=True)
    common_name = models.CharField(max_length=25000, blank=True, null=True)
    plant_remark = models.CharField(max_length=250, blank=True, null=True)
    chemical_constituents = models.CharField(max_length=25000, blank=True, null=True)
    medicinal_uses_traditional = models.CharField(max_length=25000, blank=True, null=True)
    medicinal_uses_other = models.CharField(max_length=25000, blank=True, null=True)
    part_uses = models.CharField(max_length=2500, blank=True, null=True)
    pharmacology = models.CharField(max_length=25000, blank=True, null=True)
    marphology = models.CharField(max_length=25000, blank=True, null=True)
    mol_2dfile = models.CharField(max_length=250, blank=True, null=True)
    mol_3dfile = models.CharField(max_length=250, blank=True, null=True)
    structure_name = models.CharField(max_length=250, blank=True, null=True)
    structure_type = models.CharField(max_length=250, blank=True, null=True)
    pubchem_url = models.CharField(max_length=250, blank=True, null=True)
    ins_date = models.CharField(max_length=250, blank=True, null=True)
    image_path = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cimap_data'


class CimapIbinLinks(models.Model):
    grid1x = models.CharField(max_length=950, blank=True, null=True)
    links = models.CharField(max_length=12500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cimap_ibin_links'


class Class(models.Model):
    class_id = models.CharField(primary_key=True, max_length=250)
    class_name = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'class'


class Crabs(models.Model):
    genus = models.TextField(db_column='Genus', blank=True, null=True)  # Field name made lowercase.
    specie = models.TextField(db_column='Specie', blank=True, null=True)  # Field name made lowercase.
    ibin_speciesname = models.TextField(blank=True, null=True)
    family = models.TextField(db_column='Family', blank=True, null=True)  # Field name made lowercase.
    phylum = models.TextField(db_column='Phylum', blank=True, null=True)  # Field name made lowercase.
    order = models.TextField(db_column='Order', blank=True, null=True)  # Field name made lowercase.
    class_field = models.TextField(db_column='Class', blank=True, null=True)  # Field name made lowercase. Field renamed because it was a Python reserved word.
    suborder = models.TextField(db_column='Suborder', blank=True, null=True)  # Field name made lowercase.
    subfamily = models.TextField(db_column='SubFamily', blank=True, null=True)  # Field name made lowercase.
    synonyms = models.TextField(db_column='Synonyms', blank=True, null=True)  # Field name made lowercase.
    habit_habitat = models.TextField(db_column='HABIT & HABITAT', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    distribution = models.TextField(db_column='Distribution', blank=True, null=True)  # Field name made lowercase.
    morphological_features = models.TextField(db_column='MORPHOLOGICAL FEATURES', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    uses = models.TextField(db_column='Uses', blank=True, null=True)  # Field name made lowercase.
    parts_used = models.TextField(db_column='Parts Used', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    miscellaneous_information = models.TextField(db_column='Miscellaneous Information', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    biology = models.TextField(db_column='BIOLOGY', blank=True, null=True)  # Field name made lowercase.
    ibin_speciesid = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'crabs'


class CrabsDistribution(models.Model):
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    country = models.CharField(max_length=250, blank=True, null=True)
    state = models.CharField(max_length=250, blank=True, null=True)
    district = models.CharField(max_length=250, blank=True, null=True)
    lat = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    long = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'crabs_distribution'


class CrabsFinal(models.Model):
    long = models.TextField(blank=True, null=True)
    lat = models.TextField(blank=True, null=True)
    contributor = models.TextField(blank=True, null=True)
    refrence = models.TextField(blank=True, null=True)
    genus = models.TextField(db_column='Genus', blank=True, null=True)  # Field name made lowercase.
    specie = models.TextField(db_column='Specie', blank=True, null=True)  # Field name made lowercase.
    ibin_speciesname = models.TextField(blank=True, null=True)
    family = models.TextField(db_column='Family', blank=True, null=True)  # Field name made lowercase.
    phylum = models.TextField(db_column='Phylum', blank=True, null=True)  # Field name made lowercase.
    order = models.TextField(db_column='Order', blank=True, null=True)  # Field name made lowercase.
    class_field = models.TextField(db_column='Class', blank=True, null=True)  # Field name made lowercase. Field renamed because it was a Python reserved word.
    suborder = models.TextField(db_column='Suborder', blank=True, null=True)  # Field name made lowercase.
    subfamily = models.TextField(db_column='SubFamily', blank=True, null=True)  # Field name made lowercase.
    synonyms = models.TextField(db_column='Synonyms', blank=True, null=True)  # Field name made lowercase.
    habit_habitat = models.TextField(db_column='HABIT & HABITAT', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    distribution = models.TextField(db_column='Distribution', blank=True, null=True)  # Field name made lowercase.
    morphological_features = models.TextField(db_column='MORPHOLOGICAL FEATURES', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    uses = models.TextField(db_column='Uses', blank=True, null=True)  # Field name made lowercase.
    parts_used = models.TextField(db_column='Parts Used', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    miscellaneous_information = models.TextField(db_column='Miscellaneous Information', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    biology = models.TextField(db_column='BIOLOGY', blank=True, null=True)  # Field name made lowercase.
    ibin_speciesid = models.TextField(blank=True, null=True)
    environment = models.TextField(db_column='Environment', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'crabs_final'


class CrabsImages(models.Model):
    ibin_speciesid = models.CharField(max_length=400, blank=True, null=True)
    img_path = models.CharField(max_length=600, blank=True, null=True)
    ibin_speciesname = models.CharField(max_length=350, blank=True, null=True)
    contributor = models.CharField(max_length=350, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'crabs_images'


class CrabsInfo(models.Model):
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    environment = models.CharField(max_length=850, blank=True, null=True)
    common_name = models.CharField(max_length=250, blank=True, null=True)
    colouration = models.CharField(max_length=350, blank=True, null=True)
    remarks = models.CharField(max_length=850, blank=True, null=True)
    refrence = models.CharField(max_length=850, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'crabs_info'


class CrabsTaxonomy(models.Model):
    scnm = models.CharField(primary_key=True, max_length=250)
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    kingdom_id = models.CharField(max_length=250, blank=True, null=True)
    phylum_id = models.CharField(max_length=250, blank=True, null=True)
    class_id = models.CharField(max_length=250, blank=True, null=True)
    order_id = models.CharField(max_length=250, blank=True, null=True)
    superfamily_id = models.CharField(max_length=250, blank=True, null=True)
    family_id = models.CharField(max_length=250, blank=True, null=True)
    genus_id = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'crabs_taxonomy'


class CroppestCmnname(models.Model):
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    common_name = models.CharField(max_length=850, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'croppest_cmnname'


class CroppestDistribution(models.Model):
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    state = models.CharField(max_length=250, blank=True, null=True)
    district = models.CharField(max_length=250, blank=True, null=True)
    lat = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    long = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'croppest_distribution'


class CroppestImages(models.Model):
    ibin_speciesid = models.CharField(max_length=400, blank=True, null=True)
    img_path = models.CharField(max_length=600, blank=True, null=True)
    ibin_speciesname = models.CharField(max_length=350, blank=True, null=True)
    contributor = models.CharField(max_length=350, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'croppest_images'


class CroppestTaxonomy(models.Model):
    old_scnm = models.CharField(max_length=250, blank=True, null=True)
    scnm = models.CharField(primary_key=True, max_length=250)
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    kingdom_id = models.CharField(max_length=250, blank=True, null=True)
    phylum_id = models.CharField(max_length=250, blank=True, null=True)
    class_id = models.CharField(max_length=250, blank=True, null=True)
    order_id = models.CharField(max_length=250, blank=True, null=True)
    superfamily_id = models.CharField(max_length=250, blank=True, null=True)
    family_id = models.CharField(max_length=250, blank=True, null=True)
    genus_id = models.CharField(max_length=250, blank=True, null=True)
    species_id = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'croppest_taxonomy'


class Cuckoo(models.Model):
    pid = models.CharField(max_length=200, blank=True, null=True)
    msg_date = models.CharField(max_length=200, blank=True, null=True)
    sat = models.CharField(max_length=200, blank=True, null=True)
    loc_date = models.CharField(max_length=200, blank=True, null=True)
    match_date = models.CharField(max_length=200, blank=True, null=True)
    lng = models.CharField(max_length=200, blank=True, null=True)
    lat = models.CharField(max_length=200, blank=True, null=True)
    alt = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cuckoo'


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class DomesticAnimalImages(models.Model):
    ibin_speciesid = models.CharField(max_length=400, blank=True, null=True)
    path = models.CharField(max_length=600, blank=True, null=True)
    ibin_speciesname = models.CharField(max_length=350, blank=True, null=True)
    contributor = models.CharField(max_length=35, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'domestic_animal_images'


class DomesticanimalOtherinfo(models.Model):
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    commonname = models.CharField(max_length=300, blank=True, null=True)
    breed = models.CharField(max_length=3000, blank=True, null=True)
    breed_type = models.CharField(max_length=3000, blank=True, null=True)
    home_tract = models.CharField(max_length=3000, blank=True, null=True)
    district = models.CharField(max_length=3000, blank=True, null=True)
    synonym = models.CharField(max_length=3000, blank=True, null=True)
    comment_on_breeding_tract = models.CharField(max_length=3000, blank=True, null=True)
    main_use = models.CharField(max_length=3000, blank=True, null=True)
    origin = models.CharField(max_length=3000, blank=True, null=True)
    herd_book_register_established = models.CharField(max_length=3000, blank=True, null=True)
    breed_societies = models.CharField(max_length=3000, blank=True, null=True)
    adaptability_to_environment = models.CharField(max_length=3000, blank=True, null=True)
    management_system = models.CharField(max_length=3000, blank=True, null=True)
    mobility = models.CharField(max_length=3000, blank=True, null=True)
    feeding_of_adults = models.CharField(max_length=3000, blank=True, null=True)
    comments_on_management_conditions = models.CharField(max_length=3000, blank=True, null=True)
    colour = models.CharField(max_length=300, blank=True, null=True)
    no_of_horns = models.CharField(max_length=300, blank=True, null=True)
    horns_shape_size = models.CharField(max_length=300, blank=True, null=True)
    visible_characteristics = models.CharField(max_length=3000, blank=True, null=True)
    hair_wool = models.CharField(max_length=300, blank=True, null=True)
    fiber_type = models.CharField(max_length=300, blank=True, null=True)
    height_avg_cm = models.CharField(max_length=300, blank=True, null=True)
    body_length_avg_cm = models.CharField(max_length=300, blank=True, null=True)
    heart_girth_avg_cm = models.CharField(max_length=300, blank=True, null=True)
    weight_avg_kg = models.CharField(max_length=300, blank=True, null=True)
    birth_weight_avg_kg = models.CharField(max_length=300, blank=True, null=True)
    plumage_type = models.CharField(max_length=300, blank=True, null=True)
    plumage_pattern = models.CharField(max_length=300, blank=True, null=True)
    plumage_colour = models.CharField(max_length=300, blank=True, null=True)
    comb_type = models.CharField(max_length=300, blank=True, null=True)
    skin_colour = models.CharField(max_length=300, blank=True, null=True)
    shank_colour = models.CharField(max_length=300, blank=True, null=True)
    egg_shell_colour = models.CharField(max_length=300, blank=True, null=True)
    reference1 = models.CharField(max_length=3000, blank=True, null=True)
    reference2 = models.CharField(max_length=3000, blank=True, null=True)
    locations = models.CharField(max_length=300, blank=True, null=True)
    contact_agencies = models.TextField(blank=True, null=True)
    milk_yield = models.CharField(max_length=300, blank=True, null=True)
    milk_fat = models.CharField(max_length=300, blank=True, null=True)
    first_lactation = models.CharField(max_length=300, blank=True, null=True)
    length_lacation = models.CharField(max_length=300, blank=True, null=True)
    dry_period = models.CharField(max_length=300, blank=True, null=True)
    first_calving = models.CharField(max_length=300, blank=True, null=True)
    interval_calving = models.CharField(max_length=300, blank=True, null=True)
    lambing_percentage = models.CharField(max_length=300, blank=True, null=True)
    age_at_first_lambling = models.CharField(max_length=300, blank=True, null=True)
    wool_yield = models.CharField(max_length=300, blank=True, null=True)
    staple_length = models.CharField(max_length=300, blank=True, null=True)
    fibre_density = models.CharField(max_length=300, blank=True, null=True)
    medullation = models.CharField(max_length=300, blank=True, null=True)
    fibre_diameter = models.CharField(max_length=300, blank=True, null=True)
    contributor = models.CharField(max_length=300, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'domesticanimal_otherinfo'


class Family(models.Model):
    family_id = models.CharField(primary_key=True, max_length=250)
    family_name = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'family'


class Fish(models.Model):
    species_name = models.TextField(blank=True, null=True)
    common_names = models.TextField(db_column='Common Names', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    habit_and_habitat = models.TextField(db_column='HABIT and HABITAT', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    distribution = models.TextField(db_column='Distribution', blank=True, null=True)  # Field name made lowercase.
    morphological_features = models.TextField(db_column='MORPHOLOGICAL FEATURES', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    reproductive_biology = models.TextField(db_column='REPRODUCTIVE BIOLOGY', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    breeding_season = models.TextField(db_column='Breeding season', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    breeding_techinique = models.TextField(db_column='Breeding techinique', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fishing_methods = models.TextField(db_column='Fishing methods', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    biology = models.TextField(db_column='BIOLOGY', blank=True, null=True)  # Field name made lowercase.
    stages_of_development = models.TextField(db_column='Stages of development', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    economic_importance = models.TextField(db_column='Economic Importance', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    uses = models.TextField(db_column='Uses', blank=True, null=True)  # Field name made lowercase.
    miscellaneous_information = models.TextField(db_column='Miscellaneous Information', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    literature = models.TextField(db_column='Literature', blank=True, null=True)  # Field name made lowercase.
    ibin_speciesid = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'fish'


class FishCommonName(models.Model):
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    common_name = models.CharField(max_length=850, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'fish_common_name'


class FishDistribution(models.Model):
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    state = models.CharField(max_length=250, blank=True, null=True)
    district = models.CharField(max_length=250, blank=True, null=True)
    river = models.CharField(max_length=250, blank=True, null=True)
    lat = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    long = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'fish_distribution'


class FishEconomic(models.Model):
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    economic = models.CharField(max_length=850, blank=True, null=True)
    author = models.CharField(max_length=450, blank=True, null=True)
    book_journal = models.CharField(max_length=450, blank=True, null=True)
    pageno = models.CharField(max_length=450, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'fish_economic'


class FishFinal(models.Model):
    common_names = models.TextField(db_column='Common Names', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    habit_and_habitat = models.TextField(db_column='HABIT and HABITAT', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    distribution = models.TextField(db_column='Distribution', blank=True, null=True)  # Field name made lowercase.
    morphological_features = models.TextField(db_column='MORPHOLOGICAL FEATURES', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    reproductive_biology = models.TextField(db_column='REPRODUCTIVE BIOLOGY', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    breeding_season = models.TextField(db_column='Breeding season', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    breeding_techinique = models.TextField(db_column='Breeding techinique', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fishing_methods = models.TextField(db_column='Fishing methods', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    biology = models.TextField(db_column='BIOLOGY', blank=True, null=True)  # Field name made lowercase.
    stages_of_development = models.TextField(db_column='Stages of development', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    economic_importance = models.TextField(db_column='Economic Importance', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    uses = models.TextField(db_column='Uses', blank=True, null=True)  # Field name made lowercase.
    miscellaneous_information = models.TextField(db_column='Miscellaneous Information', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    literature = models.TextField(db_column='Literature', blank=True, null=True)  # Field name made lowercase.
    ibin_speciesid = models.TextField(blank=True, null=True)
    ibin_speciesname = models.TextField(blank=True, null=True)
    status = models.TextField(blank=True, null=True)
    habitat = models.TextField(blank=True, null=True)
    peninsular = models.TextField(blank=True, null=True)
    ornamental = models.TextField(blank=True, null=True)
    sport_fish = models.TextField(blank=True, null=True)
    contributor = models.TextField(blank=True, null=True)
    morpho = models.TextField(blank=True, null=True)
    color = models.TextField(blank=True, null=True)
    finformulae = models.TextField(blank=True, null=True)
    fishery = models.TextField(blank=True, null=True)
    state = models.TextField(blank=True, null=True)
    river = models.TextField(blank=True, null=True)
    lat = models.TextField(blank=True, null=True)
    long = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'fish_final'


class FishImages(models.Model):
    ibin_speciesid = models.CharField(max_length=400, blank=True, null=True)
    img_path = models.CharField(max_length=600, blank=True, null=True)
    ibin_speciesname = models.CharField(max_length=350, blank=True, null=True)
    contributor = models.CharField(max_length=35, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'fish_images'


class FishInfo(models.Model):
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    status = models.CharField(max_length=250, blank=True, null=True)
    habitat = models.CharField(max_length=250, blank=True, null=True)
    peninsular = models.CharField(max_length=150, blank=True, null=True)
    ornamental = models.CharField(max_length=250, blank=True, null=True)
    sport_fish = models.CharField(max_length=250, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'fish_info'


class FishMorphology(models.Model):
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    morpho = models.CharField(max_length=850, blank=True, null=True)
    color = models.CharField(max_length=850, blank=True, null=True)
    finformulae = models.CharField(max_length=850, blank=True, null=True)
    fishery = models.CharField(max_length=850, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'fish_morphology'


class FishTaxonomy(models.Model):
    ibin_scientificname = models.CharField(primary_key=True, max_length=250)
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    kingdom_id = models.CharField(max_length=250, blank=True, null=True)
    phylum_id = models.CharField(max_length=250, blank=True, null=True)
    class_id = models.CharField(max_length=250, blank=True, null=True)
    order_id = models.CharField(max_length=250, blank=True, null=True)
    family_id = models.CharField(max_length=250, blank=True, null=True)
    genus_id = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'fish_taxonomy'


class FsiIbinlinks(models.Model):
    grdid_1x = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    links = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'fsi_ibinlinks'


class FsiSpecieslist(models.Model):
    joinid = models.CharField(max_length=254, blank=True, null=True)
    species = models.CharField(max_length=354, blank=True, null=True)
    grd1x = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    ibin_speciesid = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'fsi_specieslist'


class FsiSpecieslistMatched(models.Model):
    joinid = models.CharField(max_length=254, blank=True, null=True)
    species = models.CharField(max_length=354, blank=True, null=True)
    grd1x = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    ibin_speciesid = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'fsi_specieslist_matched'


class FsiSplist(models.Model):
    spnm = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'fsi_splist'


class FungiGenus(models.Model):
    genus_id = models.CharField(primary_key=True, max_length=250)
    genus_name = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'fungi_genus'


class FungiImages(models.Model):
    ibin_speciesid = models.CharField(max_length=400, blank=True, null=True)
    img_path = models.CharField(max_length=600, blank=True, null=True)
    ibin_speciesname = models.CharField(max_length=350, blank=True, null=True)
    contributor = models.CharField(max_length=350, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'fungi_images'


class FungiTaxonomy(models.Model):
    ibin_speciesid = models.CharField(primary_key=True, max_length=250)
    kingdom_id = models.CharField(max_length=250, blank=True, null=True)
    phylum_id = models.CharField(max_length=250, blank=True, null=True)
    class_id = models.CharField(max_length=250, blank=True, null=True)
    order_id = models.CharField(max_length=250, blank=True, null=True)
    family_id = models.CharField(max_length=250, blank=True, null=True)
    genus_id = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'fungi_taxonomy'


class IbinAllTaxonomy(models.Model):
    ibin_speciesid = models.CharField(primary_key=True, max_length=250)
    kingdom_id = models.CharField(max_length=250, blank=True, null=True)
    phylum_id = models.CharField(max_length=250, blank=True, null=True)
    class_id = models.CharField(max_length=250, blank=True, null=True)
    order_id = models.CharField(max_length=250, blank=True, null=True)
    superfamily_id = models.CharField(max_length=250, blank=True, null=True)
    family_id = models.CharField(max_length=250, blank=True, null=True)
    genus_id = models.CharField(max_length=250, blank=True, null=True)
    species_id = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ibin_all_taxonomy'


class IbinOrder(models.Model):
    order_id = models.CharField(primary_key=True, max_length=250)
    order_name = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'ibin_order'


class IbinRpimages(models.Model):
    path = models.CharField(max_length=450, blank=True, null=True)
    img_wt = models.IntegerField(blank=True, null=True)
    contributor = models.CharField(max_length=450, blank=True, null=True)
    ibin_speciesname = models.CharField(max_length=450, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ibin_rpimages'


class IbinSpecieslist(models.Model):
    ibin_speciesid = models.CharField(primary_key=True, max_length=250)
    ibin_speciesname = models.CharField(max_length=250)
    ibin_scnm = models.CharField(max_length=250)
    author = models.CharField(max_length=250, blank=True, null=True)
    path = models.CharField(max_length=250, blank=True, null=True)
    img_wt = models.IntegerField(blank=True, null=True)
    contributor = models.CharField(max_length=200, blank=True, null=True)
    category = models.CharField(max_length=50, blank=True, null=True)
    genus = models.CharField(max_length=250, blank=True, null=True)
    type = models.CharField(max_length=100, blank=True, null=True)
    chromosme_path = models.CharField(max_length=450, blank=True, null=True)
    chromosomes = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ibin_specieslist'


class Ibinusers(models.Model):
    sno = models.AutoField(primary_key=True)
    userid = models.CharField(max_length=250, blank=True, null=True)
    passwd = models.CharField(max_length=250, blank=True, null=True)
    email = models.CharField(max_length=250, blank=True, null=True)
    name = models.CharField(max_length=350, blank=True, null=True)
    organisation = models.CharField(max_length=550, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ibinusers'


class IhbtEdiblePlants(models.Model):
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    local_name = models.CharField(max_length=900, blank=True, null=True)
    distribution = models.CharField(max_length=900, blank=True, null=True)
    edible = models.CharField(max_length=900, blank=True, null=True)
    other_uses = models.CharField(max_length=900, blank=True, null=True)
    plant_part_used = models.CharField(max_length=900, blank=True, null=True)
    general_description = models.CharField(max_length=900, blank=True, null=True)
    leaf = models.CharField(max_length=900, blank=True, null=True)
    flower = models.CharField(max_length=900, blank=True, null=True)
    fruit_seed = models.CharField(max_length=900, blank=True, null=True)
    contributor = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ihbt_edible_plants'


class IhbtIbinLinks(models.Model):
    grid1x = models.CharField(max_length=950, blank=True, null=True)
    links = models.CharField(max_length=10020500, blank=True, null=True)
    join_count = models.CharField(max_length=90, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ihbt_ibin_links'


class IhbtImages(models.Model):
    img_path = models.CharField(max_length=450, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ihbt_images'


class IirsGrd1XCorrectedUnisp(models.Model):
    grdid = models.CharField(max_length=20, blank=True, null=True)
    uni_sp = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'iirs_grd1x_corrected_unisp'


class IirsSampleplotSpecies(models.Model):
    gid = models.CharField(max_length=20, blank=True, null=True)
    id = models.CharField(max_length=20, blank=True, null=True)
    phase = models.CharField(max_length=20, blank=True, null=True)
    state = models.CharField(max_length=50, blank=True, null=True)
    false_id = models.CharField(max_length=20, blank=True, null=True)
    grid_id = models.CharField(max_length=20, blank=True, null=True)
    no_of_indi = models.CharField(max_length=20, blank=True, null=True)
    habit = models.CharField(max_length=20, blank=True, null=True)
    medicinal_field = models.CharField(db_column='medicinal_', max_length=600, blank=True, null=True)  # Field renamed because it ended with '_'.
    medicinal = models.CharField(max_length=250, blank=True, null=True)
    economic = models.CharField(max_length=200, blank=True, null=True)
    endemic = models.CharField(max_length=200, blank=True, null=True)
    others = models.CharField(max_length=200, blank=True, null=True)
    species_name = models.CharField(max_length=200, blank=True, null=True)
    family = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'iirs_sampleplot_species'


class IirsUniCount(models.Model):
    grid1x = models.CharField(max_length=950, blank=True, null=True)
    join_count = models.CharField(max_length=90, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'iirs_uni_count'


class Kingdom(models.Model):
    kingdom_id = models.CharField(primary_key=True, max_length=250)
    kingdom_name = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'kingdom'


class LabAnimalImages(models.Model):
    ibin_speciesid = models.CharField(max_length=400, blank=True, null=True)
    img_path = models.CharField(max_length=600, blank=True, null=True)
    ibin_speciesname = models.CharField(max_length=350, blank=True, null=True)
    contributor = models.CharField(max_length=35, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'lab_animal_images'


class LabAnimalLabinfo(models.Model):
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    common = models.CharField(max_length=300, blank=True, null=True)
    gener = models.CharField(max_length=300, blank=True, null=True)
    lab = models.CharField(max_length=300, blank=True, null=True)
    contributor = models.CharField(max_length=300, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'lab_animal_labinfo'


class LizardCmnname(models.Model):
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    common_name = models.CharField(max_length=850, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'lizard_cmnname'


class LizardDistribution(models.Model):
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    state = models.CharField(max_length=250, blank=True, null=True)
    district = models.CharField(max_length=250, blank=True, null=True)
    zone = models.CharField(max_length=250, blank=True, null=True)
    lat = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    long = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'lizard_distribution'


class LizardHabit(models.Model):
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    habit = models.CharField(max_length=850, blank=True, null=True)
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'lizard_habit'


class LizardImages(models.Model):
    ibin_speciesid = models.CharField(max_length=400, blank=True, null=True)
    img_path = models.CharField(max_length=600, blank=True, null=True)
    ibin_speciesname = models.CharField(max_length=350, blank=True, null=True)
    contributor = models.CharField(max_length=35, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'lizard_images'


class LizardSize(models.Model):
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    size = models.CharField(max_length=850, blank=True, null=True)
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'lizard_size'


class LizardStatus(models.Model):
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    status = models.CharField(max_length=850, blank=True, null=True)
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'lizard_status'


class LizardTaxonomy(models.Model):
    scnm = models.CharField(primary_key=True, max_length=250)
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    kingdom_id = models.CharField(max_length=250, blank=True, null=True)
    phylum_id = models.CharField(max_length=250, blank=True, null=True)
    class_id = models.CharField(max_length=250, blank=True, null=True)
    order_id = models.CharField(max_length=250, blank=True, null=True)
    family_id = models.CharField(max_length=250, blank=True, null=True)
    genus_id = models.CharField(max_length=250, blank=True, null=True)
    species_id = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'lizard_taxonomy'


class Lizards(models.Model):
    ibin_speciesname = models.TextField(blank=True, null=True)
    diagnostic_features = models.TextField(db_column='Diagnostic Features', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    size = models.TextField(db_column='Size', blank=True, null=True)  # Field name made lowercase.
    head = models.TextField(db_column='Head', blank=True, null=True)  # Field name made lowercase.
    body = models.TextField(db_column='Body', blank=True, null=True)  # Field name made lowercase.
    limbs = models.TextField(db_column='Limbs', blank=True, null=True)  # Field name made lowercase.
    tail = models.TextField(db_column='Tail', blank=True, null=True)  # Field name made lowercase.
    remarks = models.TextField(db_column='Remarks', blank=True, null=True)  # Field name made lowercase.
    other_biological_details = models.TextField(db_column='Other Biological Details', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    distribution = models.TextField(db_column='Distribution', blank=True, null=True)  # Field name made lowercase.
    habitat = models.TextField(db_column='Habitat', blank=True, null=True)  # Field name made lowercase.
    miscellaneous_information = models.TextField(db_column='Miscellaneous Information', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    literature = models.TextField(db_column='Literature', blank=True, null=True)  # Field name made lowercase.
    ibin_speciesid = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'lizards'


class LizardsFinal(models.Model):
    diagnostic_features = models.TextField(db_column='Diagnostic Features', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    size = models.TextField(db_column='Size', blank=True, null=True)  # Field name made lowercase.
    head = models.TextField(db_column='Head', blank=True, null=True)  # Field name made lowercase.
    body = models.TextField(db_column='Body', blank=True, null=True)  # Field name made lowercase.
    limbs = models.TextField(db_column='Limbs', blank=True, null=True)  # Field name made lowercase.
    tail = models.TextField(db_column='Tail', blank=True, null=True)  # Field name made lowercase.
    remarks = models.TextField(db_column='Remarks', blank=True, null=True)  # Field name made lowercase.
    other_biological_details = models.TextField(db_column='Other Biological Details', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    distribution = models.TextField(db_column='Distribution', blank=True, null=True)  # Field name made lowercase.
    habitat = models.TextField(db_column='Habitat', blank=True, null=True)  # Field name made lowercase.
    miscellaneous_information = models.TextField(db_column='Miscellaneous Information', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    literature = models.TextField(db_column='Literature', blank=True, null=True)  # Field name made lowercase.
    ibin_speciesid = models.TextField(blank=True, null=True)
    status = models.TextField(blank=True, null=True)
    ibin_speciesname = models.TextField(blank=True, null=True)
    contributor = models.TextField(blank=True, null=True)
    common_name = models.TextField(db_column='common name', blank=True, null=True)  # Field renamed to remove unsuitable characters.
    state = models.TextField(blank=True, null=True)
    district = models.TextField(blank=True, null=True)
    zone = models.TextField(blank=True, null=True)
    lat = models.TextField(blank=True, null=True)
    long = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'lizards_final'


class Mamals(models.Model):
    ibin_speciesname = models.TextField(blank=True, null=True)
    common_names = models.TextField(db_column='Common Names', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    threats = models.TextField(db_column='Threats', blank=True, null=True)  # Field name made lowercase.
    trade = models.TextField(db_column='Trade', blank=True, null=True)  # Field name made lowercase.
    habit_habitat = models.TextField(db_column='HABIT & HABITAT', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    habitat_structure = models.TextField(db_column='Habitat Structure', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    distribution = models.TextField(db_column='Distribution', blank=True, null=True)  # Field name made lowercase.
    no_of_locations = models.TextField(db_column='No.of Locations', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    extent_of_occurrence = models.TextField(db_column='Extent of Occurrence', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    area_of_occurrence = models.TextField(db_column='Area of occurrence', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    altitude = models.TextField(db_column='Altitude', blank=True, null=True)  # Field name made lowercase.
    global_population = models.TextField(db_column='Global Population', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    captive_population = models.TextField(db_column='Captive Population', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    life_span = models.TextField(db_column='Life span', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    population_trend = models.TextField(db_column='Population Trend', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    conservation_status = models.TextField(db_column='Conservation status', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cites_appendix = models.TextField(db_column='CITES APPENDIX', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    diagnostic_features = models.TextField(db_column='DIAGNOSTIC FEATURES', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    measurements = models.TextField(db_column='Measurements', blank=True, null=True)  # Field name made lowercase.
    weight = models.TextField(db_column='Weight', blank=True, null=True)  # Field name made lowercase.
    miscellaneous_information = models.TextField(db_column='Miscellaneous Information', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    intraspecific_variations = models.TextField(db_column='Intraspecific variations', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    museum_records = models.TextField(db_column='Museum Records', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    sighting_records = models.TextField(db_column='Sighting Records', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    type_of_sighting = models.TextField(db_column='Type of Sighting', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    project_specific_records = models.TextField(db_column='Project Specific Records', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    taxon_related_studies_recommended = models.TextField(db_column='Taxon related studies recommended', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    remarks = models.TextField(db_column='Remarks', blank=True, null=True)  # Field name made lowercase.
    literature = models.TextField(db_column='Literature', blank=True, null=True)  # Field name made lowercase.
    endemism = models.TextField(db_column='Endemism', blank=True, null=True)  # Field name made lowercase.
    breeding_system = models.TextField(db_column='Breeding system', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ibin_speciesid = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mamals'


class MammalsCommonName(models.Model):
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    common_name = models.CharField(max_length=100, blank=True, null=True)
    ibin_speciesname = models.CharField(max_length=100, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mammals_common_name'


class MammalsDetails(models.Model):
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    diagnostic_characters = models.CharField(max_length=500, blank=True, null=True)
    measurments = models.CharField(max_length=500, blank=True, null=True)
    remarks = models.CharField(max_length=500, blank=True, null=True)
    ibin_speciesname = models.CharField(max_length=100, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mammals_details'


class MammalsDistribution(models.Model):
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    status = models.CharField(max_length=100, blank=True, null=True)
    ibin_speciesname = models.CharField(max_length=100, blank=True, null=True)
    t_group = models.CharField(max_length=100, blank=True, null=True)
    within_india = models.TextField(blank=True, null=True)
    outside_india = models.TextField(blank=True, null=True)
    iucn = models.TextField(blank=True, null=True)
    iwpa = models.CharField(max_length=100, blank=True, null=True)
    cites = models.CharField(max_length=100, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mammals_distribution'


class MammalsDistribution01(models.Model):
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    state = models.CharField(max_length=100, blank=True, null=True)
    ibin_speciesname = models.CharField(max_length=100, blank=True, null=True)
    district = models.CharField(max_length=100, blank=True, null=True)
    latitude = models.CharField(max_length=100, blank=True, null=True)
    longitude = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mammals_distribution_01'


class MammalsFinal(models.Model):
    ibin_speciesname = models.TextField(blank=True, null=True)
    common_names = models.TextField(db_column='Common Names', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    threats = models.TextField(db_column='Threats', blank=True, null=True)  # Field name made lowercase.
    trade = models.TextField(db_column='Trade', blank=True, null=True)  # Field name made lowercase.
    habit_habitat = models.TextField(db_column='HABIT & HABITAT', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    habitat_structure = models.TextField(db_column='Habitat Structure', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    distribution = models.TextField(db_column='Distribution', blank=True, null=True)  # Field name made lowercase.
    no_of_locations = models.TextField(db_column='No.of Locations', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    extent_of_occurrence = models.TextField(db_column='Extent of Occurrence', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    area_of_occurrence = models.TextField(db_column='Area of occurrence', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    altitude = models.TextField(db_column='Altitude', blank=True, null=True)  # Field name made lowercase.
    global_population = models.TextField(db_column='Global Population', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    captive_population = models.TextField(db_column='Captive Population', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    life_span = models.TextField(db_column='Life span', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    population_trend = models.TextField(db_column='Population Trend', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    conservation_status = models.TextField(db_column='Conservation status', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cites_appendix = models.TextField(db_column='CITES APPENDIX', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    diagnostic_features = models.TextField(db_column='DIAGNOSTIC FEATURES', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    measurements = models.TextField(db_column='Measurements', blank=True, null=True)  # Field name made lowercase.
    weight = models.TextField(db_column='Weight', blank=True, null=True)  # Field name made lowercase.
    miscellaneous_information = models.TextField(db_column='Miscellaneous Information', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    intraspecific_variations = models.TextField(db_column='Intraspecific variations', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    museum_records = models.TextField(db_column='Museum Records', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    sighting_records = models.TextField(db_column='Sighting Records', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    type_of_sighting = models.TextField(db_column='Type of Sighting', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    project_specific_records = models.TextField(db_column='Project Specific Records', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    taxon_related_studies_recommended = models.TextField(db_column='Taxon related studies recommended', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    remarks = models.TextField(db_column='Remarks', blank=True, null=True)  # Field name made lowercase.
    literature = models.TextField(db_column='Literature', blank=True, null=True)  # Field name made lowercase.
    endemism = models.TextField(db_column='Endemism', blank=True, null=True)  # Field name made lowercase.
    breeding_system = models.TextField(db_column='Breeding system', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ibin_speciesid = models.TextField(blank=True, null=True)
    contributor = models.TextField(blank=True, null=True)
    lat = models.TextField(blank=True, null=True)
    long = models.TextField(blank=True, null=True)
    area_of_occupancy = models.TextField(db_column='Area of occupancy', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    state = models.TextField(blank=True, null=True)
    district = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mammals_final'


class MammalsImages(models.Model):
    ibin_speciesid = models.CharField(max_length=400, blank=True, null=True)
    img_path = models.CharField(max_length=600, blank=True, null=True)
    ibin_speciesname = models.CharField(max_length=350, blank=True, null=True)
    contributor = models.CharField(max_length=350, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mammals_images'


class MammalsOccupancy(models.Model):
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    area_of_occupancy = models.CharField(max_length=100, blank=True, null=True)
    extent_of_occurence = models.CharField(max_length=100, blank=True, null=True)
    ibin_speciesname = models.CharField(max_length=100, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mammals_occupancy'


class MammalsThreats(models.Model):
    ibin_speciesid = models.CharField(max_length=100, blank=True, null=True)
    threats = models.CharField(max_length=100, blank=True, null=True)
    ibin_speciesname = models.CharField(max_length=100, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mammals_threats'


class MarineLifeImages(models.Model):
    ibin_speciesid = models.CharField(max_length=400, blank=True, null=True)
    img_path = models.CharField(max_length=600, blank=True, null=True)
    ibin_speciesname = models.CharField(max_length=350, blank=True, null=True)
    contributor = models.CharField(max_length=350, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'marine_life_images'


class MarinelifeCommonname(models.Model):
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    vernacular_name = models.CharField(max_length=450, blank=True, null=True)
    language = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'marinelife_commonname'


class MarinelifeHabitat(models.Model):
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    habitat = models.CharField(max_length=450, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'marinelife_habitat'


class MicrobesDeposited(models.Model):
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    deposited_culture = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'microbes_deposited'


class MicrobesFeatures(models.Model):
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    features = models.CharField(max_length=850, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'microbes_features'


class MicrobesImpdetails(models.Model):
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    important_view_point = models.CharField(max_length=850, blank=True, null=True)
    details = models.CharField(max_length=850, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'microbes_impdetails'


class MicrobesIsolation(models.Model):
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    isolation = models.CharField(max_length=450, blank=True, null=True)
    maintanance = models.CharField(max_length=450, blank=True, null=True)
    growth_temp = models.CharField(max_length=450, blank=True, null=True)
    growth_conditions = models.CharField(max_length=450, blank=True, null=True)
    incubation_time = models.CharField(max_length=450, blank=True, null=True)
    subculturing_period = models.CharField(max_length=450, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'microbes_isolation'


class MicrobesMarker(models.Model):
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    marker_type = models.CharField(max_length=850, blank=True, null=True)
    description = models.CharField(max_length=850, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'microbes_marker'


class MicrobesRef(models.Model):
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    reference = models.CharField(max_length=850, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'microbes_ref'


class MicrobesSynonyms(models.Model):
    synonyms = models.CharField(max_length=500, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=650, blank=True, null=True)
    source = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'microbes_synonyms'


class MicrobesUses(models.Model):
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    name = models.CharField(max_length=450, blank=True, null=True)
    institution = models.CharField(max_length=450, blank=True, null=True)
    city = models.CharField(max_length=450, blank=True, null=True)
    state = models.CharField(max_length=450, blank=True, null=True)
    habitat = models.CharField(max_length=450, blank=True, null=True)
    partsinplantsoranimals = models.CharField(max_length=450, blank=True, null=True)
    isolatedfromvillage = models.CharField(max_length=450, blank=True, null=True)
    isolatedfromtownorcity = models.CharField(max_length=450, blank=True, null=True)
    isolatedfromdistrict = models.CharField(max_length=450, blank=True, null=True)
    isolatedfromstate = models.CharField(max_length=450, blank=True, null=True)
    isolatedby = models.CharField(max_length=450, blank=True, null=True)
    isolateddate = models.CharField(max_length=450, blank=True, null=True)
    identifiedby = models.CharField(max_length=450, blank=True, null=True)
    identifieddate = models.CharField(max_length=450, blank=True, null=True)
    human = models.CharField(max_length=450, blank=True, null=True)
    animals = models.CharField(max_length=450, blank=True, null=True)
    plants = models.CharField(max_length=450, blank=True, null=True)
    other_microbes = models.CharField(max_length=450, blank=True, null=True)
    environment = models.CharField(max_length=450, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'microbes_uses'


class Modifiedsampleplotsroy(models.Model):
    grd_1x = models.CharField(max_length=250, blank=True, null=True)
    habitat = models.CharField(max_length=250, blank=True, null=True)
    individual_count = models.CharField(max_length=250, blank=True, null=True)
    join_id = models.CharField(max_length=250, blank=True, null=True)
    sp = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'modifiedsampleplotsroy'


class Mollusc(models.Model):
    ibin_speciesname = models.TextField(blank=True, null=True)
    habit_and_habitat = models.TextField(db_column='HABIT and HABITAT', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    common_names = models.TextField(db_column='Common Names', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    distribution = models.TextField(db_column='Distribution', blank=True, null=True)  # Field name made lowercase.
    morphological_features = models.TextField(db_column='MORPHOLOGICAL FEATURES', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    uses = models.TextField(db_column='Uses', blank=True, null=True)  # Field name made lowercase.
    miscellaneous_information = models.TextField(db_column='Miscellaneous Information', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    literature = models.TextField(db_column='Literature', blank=True, null=True)  # Field name made lowercase.
    ibin_speciesid = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mollusc'


class MolluscDistribution(models.Model):
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    country = models.CharField(max_length=250, blank=True, null=True)
    state = models.CharField(max_length=250, blank=True, null=True)
    district = models.CharField(max_length=250, blank=True, null=True)
    lat = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    long = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mollusc_distribution'


class MolluscFinal(models.Model):
    ibin_speciesname = models.TextField(blank=True, null=True)
    habit_and_habitat = models.TextField(db_column='HABIT and HABITAT', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    common_names = models.TextField(db_column='Common Names', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    distribution = models.TextField(db_column='Distribution', blank=True, null=True)  # Field name made lowercase.
    morphological_features = models.TextField(db_column='MORPHOLOGICAL FEATURES', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    uses = models.TextField(db_column='Uses', blank=True, null=True)  # Field name made lowercase.
    miscellaneous_information = models.TextField(db_column='Miscellaneous Information', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    literature = models.TextField(db_column='Literature', blank=True, null=True)  # Field name made lowercase.
    ibin_speciesid = models.TextField(blank=True, null=True)
    reference = models.TextField(blank=True, null=True)
    morphology = models.TextField(blank=True, null=True)
    contributor = models.TextField(blank=True, null=True)
    country = models.TextField(blank=True, null=True)
    state = models.TextField(blank=True, null=True)
    district = models.TextField(blank=True, null=True)
    lat = models.TextField(blank=True, null=True)
    long = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mollusc_final'


class MolluscImages(models.Model):
    ibin_speciesid = models.CharField(max_length=400, blank=True, null=True)
    img_path = models.CharField(max_length=600, blank=True, null=True)
    ibin_speciesname = models.CharField(max_length=350, blank=True, null=True)
    contributor = models.CharField(max_length=35, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mollusc_images'


class MolluscMorphology(models.Model):
    ibin_speciesid = models.CharField(max_length=400, blank=True, null=True)
    ibin_speciesname = models.CharField(max_length=350, blank=True, null=True)
    refernces = models.CharField(max_length=600, blank=True, null=True)
    morphology = models.CharField(max_length=600, blank=True, null=True)
    contributor = models.CharField(max_length=350, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mollusc_morphology'


class Mytab(models.Model):
    pid = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    sales = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    status = models.CharField(max_length=6, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mytab'


class Mytab2(models.Model):
    pid = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    sales = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    status = models.CharField(max_length=6, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mytab2'


class NewSynonyms(models.Model):
    spnm = models.CharField(max_length=250, blank=True, null=True)
    synonyms_scnm = models.CharField(max_length=250, blank=True, null=True)
    synonyms_spnm = models.CharField(max_length=250, blank=True, null=True)
    author = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'new_synonyms'


class Newjsontable(models.Model):
    contributed_by = models.TextField(db_column='contributed by', blank=True, null=True)  # Field renamed to remove unsuitable characters.
    filename = models.TextField(blank=True, null=True)
    common_names = models.TextField(db_column='Common Names', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    habit_habitat = models.TextField(db_column='HABIT & HABITAT', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    availability = models.TextField(blank=True, null=True)
    status = models.TextField(db_column='Status', blank=True, null=True)  # Field name made lowercase.
    diagnostic_features = models.TextField(db_column='DIAGNOSTIC FEATURES', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    reproductive_biology = models.TextField(db_column='REPRODUCTIVE BIOLOGY', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    breeding_system = models.TextField(db_column='Breeding system', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    phenology = models.TextField(db_column='Phenology', blank=True, null=True)  # Field name made lowercase.
    pollinators = models.TextField(db_column='Pollinators', blank=True, null=True)  # Field name made lowercase.
    floral_and_seed_biology = models.TextField(db_column='Floral and seed biology', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    major_pest_and_diseases = models.TextField(db_column='MAJOR PEST AND DISEASES', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    utilization = models.TextField(db_column='UTILIZATION', blank=True, null=True)  # Field name made lowercase.
    harvesting_practices = models.TextField(db_column='Harvesting Practices', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    produce_and_products = models.TextField(db_column='Produce and Products', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    chemistry = models.TextField(db_column='Chemistry', blank=True, null=True)  # Field name made lowercase.
    processing_techniques = models.TextField(db_column='Processing Techniques', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    other_uses = models.TextField(db_column='Other uses', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    export_potential = models.TextField(db_column='Export potential', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    miscellaneous_information = models.TextField(db_column='Miscellaneous Information', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    literature = models.TextField(db_column='Literature', blank=True, null=True)  # Field name made lowercase.
    seed_biology = models.TextField(db_column='Seed Biology', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    seed_germination = models.TextField(db_column='Seed Germination', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cultivation = models.TextField(db_column='Cultivation', blank=True, null=True)  # Field name made lowercase.
    major_pests_and_diseases = models.TextField(db_column='MAJOR PESTS AND DISEASES', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    parts_used = models.TextField(db_column='Parts used', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.

    class Meta:
        managed = False
        db_table = 'newjsontable'


class OtherGrids(models.Model):
    id = models.BigIntegerField(blank=True, null=True)
    category = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'other_grids'


class PhyllHerb(models.Model):
    spid = models.CharField(max_length=500, blank=True, null=True)
    species = models.CharField(max_length=500, blank=True, null=True)
    shape = models.CharField(max_length=500, blank=True, null=True)
    cataphyll = models.CharField(max_length=500, blank=True, null=True)
    fruit = models.CharField(max_length=500, blank=True, null=True)
    margin = models.CharField(max_length=500, blank=True, null=True)
    base = models.CharField(max_length=500, blank=True, null=True)
    lobes = models.CharField(max_length=500, blank=True, null=True)
    apex = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'phyll_herb'


class PhyllShrub(models.Model):
    spid = models.CharField(max_length=500, blank=True, null=True)
    species = models.CharField(max_length=500, blank=True, null=True)
    fruit = models.CharField(max_length=500, blank=True, null=True)
    shape = models.CharField(max_length=500, blank=True, null=True)
    spine = models.CharField(max_length=500, blank=True, null=True)
    flower = models.CharField(max_length=500, blank=True, null=True)
    fshape = models.CharField(max_length=500, blank=True, null=True)
    pattern = models.CharField(max_length=500, blank=True, null=True)
    leaf = models.CharField(max_length=500, blank=True, null=True)
    texture = models.CharField(max_length=500, blank=True, null=True)
    beneath = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'phyll_shrub'


class PhyllTree(models.Model):
    spid = models.CharField(max_length=500, blank=True, null=True)
    species = models.CharField(max_length=500, blank=True, null=True)
    shape = models.CharField(max_length=500, blank=True, null=True)
    numb = models.CharField(max_length=500, blank=True, null=True)
    fruit = models.CharField(max_length=500, blank=True, null=True)
    charac = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'phyll_tree'


class Phylum(models.Model):
    phylum_id = models.CharField(primary_key=True, max_length=250)
    phylum_name = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'phylum'


class PlantBioticZone(models.Model):
    ibin_speciesid = models.CharField(max_length=200, blank=True, null=True)
    bioticzone = models.CharField(max_length=250, blank=True, null=True)
    flora_name = models.CharField(max_length=200, blank=True, null=True)
    flora_volume = models.CharField(max_length=20, blank=True, null=True)
    author = models.CharField(max_length=100, blank=True, null=True)
    edition = models.CharField(max_length=150, blank=True, null=True)
    yop = models.CharField(max_length=10, blank=True, null=True)
    contributor = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'plant_biotic_zone'


class PlantBioticzone(models.Model):
    ibin_speciesid = models.TextField()
    bioticzone = models.TextField(blank=True, null=True)
    reference = models.TextField(blank=True, null=True)
    contributor = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'plant_bioticzone'


class PlantChromosomes(models.Model):
    img_path = models.CharField(max_length=250, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=200, blank=True, null=True)
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'plant_chromosomes'


class PlantCommonName(models.Model):
    ibin_speciesid = models.CharField(max_length=200, blank=True, null=True)
    language = models.CharField(max_length=250, blank=True, null=True)
    common_name = models.CharField(max_length=200, blank=True, null=True)
    flora_name = models.CharField(max_length=200, blank=True, null=True)
    flora_volume = models.CharField(max_length=100, blank=True, null=True)
    author = models.CharField(max_length=150, blank=True, null=True)
    edition = models.CharField(max_length=50, blank=True, null=True)
    yop = models.CharField(max_length=10, blank=True, null=True)
    contributor = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'plant_common_name'


class PlantConsStatus(models.Model):
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    iucn_status = models.CharField(max_length=100000, blank=True, null=True)
    assess_date = models.CharField(max_length=200, blank=True, null=True)
    source = models.CharField(max_length=200, blank=True, null=True)
    contributor = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'plant_cons_status'


class PlantGenus(models.Model):
    genus_id = models.CharField(primary_key=True, max_length=250)
    genus_name = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'plant_genus'


class PlantHabitat(models.Model):
    ibin_speciesid = models.CharField(max_length=200, blank=True, null=True)
    habitat = models.CharField(max_length=250000, blank=True, null=True)
    flora_name = models.CharField(max_length=200, blank=True, null=True)
    flora_volume = models.CharField(max_length=100, blank=True, null=True)
    author = models.CharField(max_length=150, blank=True, null=True)
    edition = models.CharField(max_length=50, blank=True, null=True)
    yop = models.CharField(max_length=10, blank=True, null=True)
    contributor = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'plant_habitat'


class PlantHabitatPheno(models.Model):
    ibin_speciesid = models.CharField(max_length=200, blank=True, null=True)
    habitat = models.CharField(max_length=250, blank=True, null=True)
    pheno = models.CharField(max_length=200, blank=True, null=True)
    flora_name = models.CharField(max_length=200, blank=True, null=True)
    flora_volume = models.CharField(max_length=100, blank=True, null=True)
    author = models.CharField(max_length=150, blank=True, null=True)
    edition = models.CharField(max_length=50, blank=True, null=True)
    yop = models.CharField(max_length=10, blank=True, null=True)
    contributor = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'plant_habitat_pheno'


class PlantIbinspecieslistTemp(models.Model):
    ibin_speciesname = models.CharField(max_length=10000, blank=True, null=True)
    ibin_scnm = models.CharField(max_length=10000, blank=True, null=True)
    author = models.CharField(max_length=10000, blank=True, null=True)
    path = models.CharField(max_length=100, blank=True, null=True)
    img_wt = models.CharField(max_length=10000, blank=True, null=True)
    contributor = models.CharField(max_length=10000, blank=True, null=True)
    category = models.CharField(max_length=10000, blank=True, null=True)
    genus = models.CharField(max_length=10000, blank=True, null=True)
    type = models.CharField(max_length=10000, blank=True, null=True)
    chromosme_path = models.CharField(max_length=10000, blank=True, null=True)
    chromosomes = models.CharField(max_length=10000, blank=True, null=True)
    species_id = models.CharField(max_length=10000, blank=True, null=True)
    recordid = models.ForeignKey('PlantPlantsmodel', models.DO_NOTHING, db_column='RecordID_id')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'plant_ibinspecieslist_temp'


class PlantImages(models.Model):
    ibin_speciesid = models.CharField(max_length=200, blank=True, null=True)
    img_ct = models.CharField(max_length=100, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)
    img_path = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'plant_images'


class PlantLiteratureIirs(models.Model):
    ibin_speciesid = models.CharField(max_length=450, blank=True, null=True)
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    author = models.CharField(max_length=250, blank=True, null=True)
    publication = models.CharField(max_length=250, blank=True, null=True)
    yop = models.CharField(max_length=50, blank=True, null=True)
    contributor = models.CharField(max_length=100, blank=True, null=True)
    compiler = models.CharField(max_length=350, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'plant_literature_iirs'


class PlantLiteratureUasb(models.Model):
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    author = models.CharField(max_length=250, blank=True, null=True)
    publication = models.CharField(max_length=250, blank=True, null=True)
    yop = models.CharField(max_length=50, blank=True, null=True)
    contributor = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'plant_literature_uasb'


class PlantPheno(models.Model):
    ibin_speciesid = models.CharField(max_length=200, blank=True, null=True)
    pheno = models.CharField(max_length=250, blank=True, null=True)
    flora_name = models.CharField(max_length=200, blank=True, null=True)
    flora_volume = models.CharField(max_length=100, blank=True, null=True)
    author = models.CharField(max_length=150, blank=True, null=True)
    edition = models.CharField(max_length=50, blank=True, null=True)
    yop = models.CharField(max_length=10, blank=True, null=True)
    contributor = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'plant_pheno'


class PlantPlantbioticzoneTemp(models.Model):
    bioticzone = models.CharField(max_length=250, blank=True, null=True)
    flora_name = models.CharField(max_length=200, blank=True, null=True)
    flora_volume = models.CharField(max_length=20, blank=True, null=True)
    author = models.CharField(max_length=100, blank=True, null=True)
    edition = models.CharField(max_length=150, blank=True, null=True)
    yop = models.CharField(max_length=10, blank=True, null=True)
    contributor = models.CharField(max_length=200, blank=True, null=True)
    recordid = models.ForeignKey('PlantPlantsmodel', models.DO_NOTHING, db_column='RecordID_id')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'plant_plantbioticzone_temp'


class PlantPlantcommonnameTemp(models.Model):
    language = models.CharField(max_length=250, blank=True, null=True)
    common_name = models.CharField(max_length=200, blank=True, null=True)
    flora_name = models.CharField(max_length=200, blank=True, null=True)
    flora_volume = models.CharField(max_length=100, blank=True, null=True)
    author = models.CharField(max_length=150, blank=True, null=True)
    edition = models.CharField(max_length=50, blank=True, null=True)
    yop = models.CharField(max_length=10, blank=True, null=True)
    contributor = models.CharField(max_length=200, blank=True, null=True)
    recordid = models.ForeignKey('PlantPlantsmodel', models.DO_NOTHING, db_column='RecordID_id')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'plant_plantcommonname_temp'


class PlantPlantconsstatusTemp(models.Model):
    iucn_status = models.CharField(max_length=100000, blank=True, null=True)
    assess_date = models.CharField(max_length=200, blank=True, null=True)
    source = models.CharField(max_length=200, blank=True, null=True)
    contributor = models.TextField(blank=True, null=True)
    recordid = models.ForeignKey('PlantPlantsmodel', models.DO_NOTHING, db_column='RecordID_id')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'plant_plantconsstatus_temp'


class PlantPlantgenusTemp(models.Model):
    genus_id = models.CharField(primary_key=True, max_length=250)
    genus_name = models.CharField(max_length=50)
    recordid = models.ForeignKey('PlantPlantsmodel', models.DO_NOTHING, db_column='RecordID_id')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'plant_plantgenus_temp'


class PlantPlanthabitatTemp(models.Model):
    habitat = models.CharField(max_length=250000, blank=True, null=True)
    flora_name = models.CharField(max_length=200, blank=True, null=True)
    flora_volume = models.CharField(max_length=100, blank=True, null=True)
    author = models.CharField(max_length=150, blank=True, null=True)
    edition = models.CharField(max_length=50, blank=True, null=True)
    yop = models.CharField(max_length=10, blank=True, null=True)
    contributor = models.CharField(max_length=200, blank=True, null=True)
    recordid = models.ForeignKey('PlantPlantsmodel', models.DO_NOTHING, db_column='RecordID_id')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'plant_planthabitat_temp'


class PlantPlantsdescTemp(models.Model):
    description = models.TextField(blank=True, null=True)
    contributor = models.TextField(blank=True, null=True)
    source = models.CharField(max_length=25000, blank=True, null=True)
    recordid = models.ForeignKey('PlantPlantsmodel', models.DO_NOTHING, db_column='RecordID_id')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'plant_plantsdesc_temp'


class PlantPlantsdiagnosticTemp(models.Model):
    ibin_speciesname = models.CharField(max_length=450, blank=True, null=True)
    diagnostic = models.CharField(max_length=45000, blank=True, null=True)
    recordid = models.ForeignKey('PlantPlantsmodel', models.DO_NOTHING, db_column='RecordID_id')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'plant_plantsdiagnostic_temp'


class PlantPlantsmodel(models.Model):
    validated = models.BooleanField()
    user_id = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'plant_plantsmodel'


class PlantPlantsynonymscolTemp(models.Model):
    synonyms = models.CharField(max_length=100000, blank=True, null=True)
    source = models.CharField(max_length=100000, blank=True, null=True)
    author = models.CharField(max_length=100000, blank=True, null=True)
    synonym_species = models.CharField(max_length=100000, blank=True, null=True)
    contributor = models.CharField(max_length=100000, blank=True, null=True)
    species_id = models.CharField(max_length=100000, blank=True, null=True)
    recordid = models.ForeignKey(PlantPlantsmodel, models.DO_NOTHING, db_column='RecordID_id')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'plant_plantsynonymscol_temp'


class PlantPlanttaxonomyTemp(models.Model):
    kingdom_id = models.CharField(max_length=250, blank=True, null=True)
    phylum_id = models.CharField(max_length=250, blank=True, null=True)
    class_id = models.CharField(max_length=250, blank=True, null=True)
    order_id = models.CharField(max_length=250, blank=True, null=True)
    family_id = models.CharField(max_length=250, blank=True, null=True)
    genus_id = models.CharField(max_length=250, blank=True, null=True)
    species_id = models.CharField(max_length=250, blank=True, null=True)
    recordid = models.ForeignKey(PlantPlantsmodel, models.DO_NOTHING, db_column='RecordID_id')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'plant_planttaxonomy_temp'


class PlantPlantusesTemp(models.Model):
    part = models.CharField(max_length=250, blank=True, null=True)
    uses = models.CharField(max_length=250, blank=True, null=True)
    flora_name = models.CharField(max_length=200, blank=True, null=True)
    flora_volume = models.CharField(max_length=100, blank=True, null=True)
    author = models.CharField(max_length=150, blank=True, null=True)
    edition = models.CharField(max_length=50, blank=True, null=True)
    yop = models.CharField(max_length=10, blank=True, null=True)
    contributor = models.CharField(max_length=200, blank=True, null=True)
    recordid = models.ForeignKey(PlantPlantsmodel, models.DO_NOTHING, db_column='RecordID_id')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'plant_plantuses_temp'


class PlantSynonymsCol(models.Model):
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    synonyms = models.CharField(max_length=250, blank=True, null=True)
    source = models.CharField(max_length=50, blank=True, null=True)
    author = models.CharField(max_length=200, blank=True, null=True)
    synonym_species = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'plant_synonyms_col'


class PlantSynonymsUasb(models.Model):
    ibin_speciesid = models.CharField(max_length=200, blank=True, null=True)
    synonyms = models.CharField(max_length=250, blank=True, null=True)
    flora_name = models.CharField(max_length=200, blank=True, null=True)
    flora_volume = models.CharField(max_length=100, blank=True, null=True)
    author = models.CharField(max_length=150, blank=True, null=True)
    edition = models.CharField(max_length=50, blank=True, null=True)
    yop = models.CharField(max_length=10, blank=True, null=True)
    contributor = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'plant_synonyms_uasb'


class PlantTaxonomy(models.Model):
    ibin_speciesid = models.CharField(max_length=250)
    kingdom_id = models.CharField(max_length=250, blank=True, null=True)
    phylum_id = models.CharField(max_length=250, blank=True, null=True)
    class_id = models.CharField(max_length=250, blank=True, null=True)
    order_id = models.CharField(max_length=250, blank=True, null=True)
    family_id = models.CharField(max_length=250, blank=True, null=True)
    genus_id = models.CharField(max_length=250, blank=True, null=True)
    species_id = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'plant_taxonomy'


class PlantUploadedCsv(models.Model):
    filename = models.TextField()
    filepath = models.TextField()
    status = models.CharField(max_length=256)
    date = models.DateField()
    user_id = models.ForeignKey(AuthUser, models.DO_NOTHING, db_column='User_Id_id')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'plant_uploaded_csv'


class PlantUploadedRecords(models.Model):
    status = models.CharField(max_length=256)
    date = models.DateField()
    recordid = models.ForeignKey(PlantPlantsmodel, models.DO_NOTHING, db_column='RecordID_id')  # Field name made lowercase.
    user_id = models.ForeignKey(AuthUser, models.DO_NOTHING, db_column='User_Id_id')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'plant_uploaded_records'


class PlantUses(models.Model):
    ibin_speciesid = models.CharField(max_length=200, blank=True, null=True)
    part = models.CharField(max_length=250, blank=True, null=True)
    uses = models.CharField(max_length=250, blank=True, null=True)
    flora_name = models.CharField(max_length=200, blank=True, null=True)
    flora_volume = models.CharField(max_length=100, blank=True, null=True)
    author = models.CharField(max_length=150, blank=True, null=True)
    edition = models.CharField(max_length=50, blank=True, null=True)
    yop = models.CharField(max_length=10, blank=True, null=True)
    contributor = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'plant_uses'


class PlantsDesc(models.Model):
    ibin_speciesid = models.TextField()
    description = models.TextField(blank=True, null=True)
    contributor = models.TextField(blank=True, null=True)
    source = models.CharField(max_length=25000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'plants_desc'


class PlantsDescIdrai(models.Model):
    ibin_speciesid = models.CharField(max_length=500, blank=True, null=True)
    ibin_speciesname = models.CharField(max_length=500, blank=True, null=True)
    description = models.CharField(max_length=5000, blank=True, null=True)
    source = models.CharField(max_length=500, blank=True, null=True)
    contributor = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'plants_desc_idrai'


class PlantsDiagnostic(models.Model):
    ibin_speciesname = models.CharField(max_length=450, blank=True, null=True)
    diagnostic = models.CharField(max_length=45000, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=450, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'plants_diagnostic'


class PlantsDistributionCimap(models.Model):
    ibin_speciesid = models.TextField()
    distribution = models.TextField(blank=True, null=True)
    contributor = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'plants_distribution_cimap'


class PlantsDistributionIhbt(models.Model):
    ibin_speciesid = models.TextField()
    distribution = models.TextField(blank=True, null=True)
    contributor = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'plants_distribution_ihbt'


class PlantsDistributionIirs(models.Model):
    ibin_speciesid = models.TextField(blank=True, null=True)
    ibin_speciesname = models.TextField(blank=True, null=True)
    distribution = models.TextField(blank=True, null=True)
    source = models.TextField(blank=True, null=True)
    contributor = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'plants_distribution_iirs'


class PlantsHabitat(models.Model):
    ibin_speciesname = models.CharField(max_length=450, blank=True, null=True)
    habit_habitat = models.CharField(max_length=45000, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=350, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'plants_habitat'


class PlantsLocationCimap(models.Model):
    ibin_speciesid = models.TextField(blank=True, null=True)
    minelevation = models.TextField(blank=True, null=True)
    maxelevation = models.TextField(blank=True, null=True)
    contributor = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'plants_location_cimap'


class PlantsMedicinalCimap(models.Model):
    ibin_speciesid = models.TextField()
    medicinal_uses_traditional = models.TextField(blank=True, null=True)
    medicinal_uses_other = models.TextField(blank=True, null=True)
    part_uses = models.TextField(blank=True, null=True)
    contributor = models.TextField(blank=True, null=True)
    ibin_speciesname = models.CharField(max_length=350, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'plants_medicinal_cimap'


class PlantsMedicinalIirs(models.Model):
    ibin_speciesid = models.TextField(blank=True, null=True)
    ibin_speciesname = models.TextField(blank=True, null=True)
    medicinal = models.TextField(blank=True, null=True)
    source = models.TextField(blank=True, null=True)
    contributor = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'plants_medicinal_iirs'


class PlantsPhenology(models.Model):
    ibin_speciesname = models.CharField(max_length=450, blank=True, null=True)
    phenology = models.CharField(max_length=45000, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=450, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'plants_phenology'


class PlantsPhenologyIirs(models.Model):
    ibin_speciesid = models.TextField(blank=True, null=True)
    ibin_speciesname = models.TextField(blank=True, null=True)
    phenology = models.TextField(blank=True, null=True)
    source = models.TextField(blank=True, null=True)
    contributor = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'plants_phenology_iirs'


class PrawnsCmnname(models.Model):
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    common_name = models.CharField(max_length=850, blank=True, null=True)
    language = models.CharField(max_length=850, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'prawns_cmnname'


class PrawnsColoration(models.Model):
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    coloration = models.CharField(max_length=850, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'prawns_coloration'


class PrawnsDistribution(models.Model):
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    habitat = models.CharField(max_length=250, blank=True, null=True)
    country = models.CharField(max_length=250, blank=True, null=True)
    state = models.CharField(max_length=250, blank=True, null=True)
    district = models.CharField(max_length=250, blank=True, null=True)
    lat = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    long = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'prawns_distribution'


class PrawnsFinal(models.Model):
    ibin_speciesname = models.TextField(blank=True, null=True)
    habit_and_habitat = models.TextField(db_column='HABIT and HABITAT', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    distribution = models.TextField(db_column='Distribution', blank=True, null=True)  # Field name made lowercase.
    morphological_features = models.TextField(db_column='MORPHOLOGICAL FEATURES', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    biology = models.TextField(db_column='BIOLOGY', blank=True, null=True)  # Field name made lowercase.
    common_names = models.TextField(db_column='Common Names', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    uses = models.TextField(db_column='Uses', blank=True, null=True)  # Field name made lowercase.
    miscellaneous_information = models.TextField(db_column='Miscellaneous Information', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    literature = models.TextField(db_column='Literature', blank=True, null=True)  # Field name made lowercase.
    ibin_speciesid = models.TextField(blank=True, null=True)
    coloration = models.TextField(blank=True, null=True)
    contributor = models.TextField(blank=True, null=True)
    language = models.TextField(blank=True, null=True)
    contributor_1 = models.TextField(db_column='contributor__1', blank=True, null=True)  # Field renamed because it contained more than one '_' in a row.
    maxsize = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'prawns_final'


class PrawnsImages(models.Model):
    ibin_speciesid = models.CharField(max_length=400, blank=True, null=True)
    img_path = models.CharField(max_length=600, blank=True, null=True)
    ibin_speciesname = models.CharField(max_length=350, blank=True, null=True)
    contributor = models.CharField(max_length=350, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'prawns_images'


class PrawnsMaxsize(models.Model):
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    maxsize = models.CharField(max_length=850, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'prawns_maxsize'


class ProtozoaGenus(models.Model):
    genus_id = models.CharField(primary_key=True, max_length=250)
    genus_name = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'protozoa_genus'


class ProtozoaImages(models.Model):
    ibin_speciesid = models.CharField(max_length=400, blank=True, null=True)
    img_path = models.CharField(max_length=600, blank=True, null=True)
    ibin_speciesname = models.CharField(max_length=350, blank=True, null=True)
    contributor = models.CharField(max_length=350, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'protozoa_images'


class ProtozoaTaxonomy(models.Model):
    ibin_speciesid = models.CharField(primary_key=True, max_length=250)
    kingdom_id = models.CharField(max_length=50, blank=True, null=True)
    phylum_id = models.CharField(max_length=50, blank=True, null=True)
    class_id = models.CharField(max_length=100, blank=True, null=True)
    order_id = models.CharField(max_length=100, blank=True, null=True)
    family_id = models.CharField(max_length=100, blank=True, null=True)
    genus_id = models.CharField(max_length=150, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'protozoa_taxonomy'


class Ratings(models.Model):
    rating = models.IntegerField(blank=True, null=True)
    user_id = models.CharField(max_length=250)
    feedback = models.TextField(blank=True, null=True)
    species_id = models.CharField(max_length=350)
    name = models.TextField(blank=True, null=True)
    affiliation = models.TextField(blank=True, null=True)
    contactdetails = models.TextField(blank=True, null=True)
    specialization = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ratings'


class Rattans(models.Model):
    spid = models.CharField(max_length=500, blank=True, null=True)
    species = models.CharField(max_length=500, blank=True, null=True)
    habit = models.CharField(max_length=500, blank=True, null=True)
    vein = models.CharField(max_length=500, blank=True, null=True)
    shape = models.CharField(max_length=500, blank=True, null=True)
    apex = models.CharField(max_length=500, blank=True, null=True)
    knee = models.CharField(max_length=500, blank=True, null=True)
    spines = models.CharField(max_length=500, blank=True, null=True)
    leaflets = models.CharField(max_length=500, blank=True, null=True)
    leaf = models.CharField(max_length=500, blank=True, null=True)
    surface = models.CharField(max_length=500, blank=True, null=True)
    ocrea = models.CharField(max_length=500, blank=True, null=True)
    shoot = models.CharField(max_length=500, blank=True, null=True)
    fruit = models.CharField(max_length=500, blank=True, null=True)
    petiole = models.CharField(max_length=500, blank=True, null=True)
    sheath = models.CharField(max_length=500, blank=True, null=True)
    invol = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'rattans'


class RefAllium(models.Model):
    authors = models.CharField(max_length=250, blank=True, null=True)
    year = models.CharField(max_length=250, blank=True, null=True)
    paper_title = models.CharField(max_length=250, blank=True, null=True)
    journal = models.CharField(max_length=250, blank=True, null=True)
    volume = models.CharField(max_length=250, blank=True, null=True)
    page_doi_isbn = models.CharField(max_length=250, blank=True, null=True)
    s_no = models.CharField(max_length=10, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ref_allium'


class SdmSpecieslist(models.Model):
    ibin_speciesid = models.CharField(max_length=350, blank=True, null=True)
    ibin_speciesname = models.CharField(max_length=350, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'sdm_specieslist'


class ShlokaTranslation(models.Model):
    book = models.TextField(blank=True, null=True)
    subject = models.TextField(blank=True, null=True)
    keywords = models.TextField(blank=True, null=True)
    verse_no = models.TextField(blank=True, null=True)
    details = models.TextField(blank=True, null=True)
    verse = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'shloka_translation'


class Snakes(models.Model):
    ibin_speciesname = models.TextField(blank=True, null=True)
    common_names = models.TextField(db_column='Common Names', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    other_biological_details = models.TextField(db_column='Other Biological Details', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    habit_habitat = models.TextField(db_column='HABIT & HABITAT', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    distribution = models.TextField(db_column='Distribution', blank=True, null=True)  # Field name made lowercase.
    altitude = models.TextField(db_column='Altitude', blank=True, null=True)  # Field name made lowercase.
    status = models.TextField(blank=True, null=True)
    conservation_status = models.TextField(db_column='Conservation status', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    size = models.TextField(db_column='Size', blank=True, null=True)  # Field name made lowercase.
    diagnostic_features = models.TextField(db_column='DIAGNOSTIC FEATURES', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    length = models.TextField(db_column='Length', blank=True, null=True)  # Field name made lowercase.
    body = models.TextField(db_column='Body', blank=True, null=True)  # Field name made lowercase.
    head = models.TextField(db_column='Head', blank=True, null=True)  # Field name made lowercase.
    tail = models.TextField(db_column='Tail', blank=True, null=True)  # Field name made lowercase.
    eyes = models.TextField(db_column='Eyes', blank=True, null=True)  # Field name made lowercase.
    scales = models.TextField(db_column='Scales', blank=True, null=True)  # Field name made lowercase.
    colouration = models.TextField(db_column='Colouration', blank=True, null=True)  # Field name made lowercase.
    breeding_biology = models.TextField(db_column='Breeding Biology', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    literature = models.TextField(db_column='Literature', blank=True, null=True)  # Field name made lowercase.
    remarks = models.TextField(db_column='Remarks', blank=True, null=True)  # Field name made lowercase.
    miscellaneous_information = models.TextField(db_column='Miscellaneous Information', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ibin_speciesid = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'snakes'


class SnakesCommonName(models.Model):
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    common_name = models.CharField(max_length=850, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'snakes_common_name'


class SnakesFinal(models.Model):
    ibin_speciesname = models.TextField(blank=True, null=True)
    common_names = models.TextField(db_column='Common Names', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    other_biological_details = models.TextField(db_column='Other Biological Details', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    habit_habitat = models.TextField(db_column='HABIT & HABITAT', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    distribution = models.TextField(db_column='Distribution', blank=True, null=True)  # Field name made lowercase.
    altitude = models.TextField(db_column='Altitude', blank=True, null=True)  # Field name made lowercase.
    conservation_status = models.TextField(db_column='Conservation status', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    size = models.TextField(db_column='Size', blank=True, null=True)  # Field name made lowercase.
    diagnostic_features = models.TextField(db_column='DIAGNOSTIC FEATURES', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    length = models.TextField(db_column='Length', blank=True, null=True)  # Field name made lowercase.
    body = models.TextField(db_column='Body', blank=True, null=True)  # Field name made lowercase.
    head = models.TextField(db_column='Head', blank=True, null=True)  # Field name made lowercase.
    tail = models.TextField(db_column='Tail', blank=True, null=True)  # Field name made lowercase.
    eyes = models.TextField(db_column='Eyes', blank=True, null=True)  # Field name made lowercase.
    scales = models.TextField(db_column='Scales', blank=True, null=True)  # Field name made lowercase.
    colouration = models.TextField(db_column='Colouration', blank=True, null=True)  # Field name made lowercase.
    breeding_biology = models.TextField(db_column='Breeding Biology', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    literature = models.TextField(db_column='Literature', blank=True, null=True)  # Field name made lowercase.
    remarks = models.TextField(db_column='Remarks', blank=True, null=True)  # Field name made lowercase.
    miscellaneous_information = models.TextField(db_column='Miscellaneous Information', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ibin_speciesid = models.TextField(blank=True, null=True)
    foresttype = models.TextField(blank=True, null=True)
    contributor = models.TextField(blank=True, null=True)
    threats = models.TextField(blank=True, null=True)
    status = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'snakes_final'


class SnakesForesttype(models.Model):
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    foresttype = models.CharField(max_length=850, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'snakes_foresttype'


class SnakesHabit(models.Model):
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    habit = models.CharField(max_length=850, blank=True, null=True)
    habitat = models.CharField(max_length=850, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'snakes_habit'


class SnakesImages(models.Model):
    ibin_speciesid = models.CharField(max_length=400, blank=True, null=True)
    img_path = models.CharField(max_length=600, blank=True, null=True)
    ibin_speciesname = models.CharField(max_length=350, blank=True, null=True)
    contributor = models.CharField(max_length=350, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'snakes_images'


class SnakesStatus(models.Model):
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    status = models.CharField(max_length=850, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'snakes_status'


class SnakesThreats(models.Model):
    ibin_speciesname = models.CharField(max_length=250, blank=True, null=True)
    threats = models.CharField(max_length=850, blank=True, null=True)
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'snakes_threats'


class SpeciesBats(models.Model):
    genus = models.CharField(max_length=400, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'species_bats'


class SpeciesBirds(models.Model):
    genus = models.CharField(primary_key=True, max_length=400)

    class Meta:
        managed = False
        db_table = 'species_birds'


class SpeciesButterflies(models.Model):
    genus = models.CharField(primary_key=True, max_length=400)

    class Meta:
        managed = False
        db_table = 'species_butterflies'


class SpeciesCol(models.Model):
    genus = models.CharField(max_length=400, blank=True, null=True)
    species = models.CharField(max_length=250, blank=True, null=True)
    old_spnm = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'species_col'


class SpeciesCroppest(models.Model):
    genus = models.CharField(max_length=400, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'species_croppest'


class SpeciesLizard(models.Model):
    genus = models.CharField(primary_key=True, max_length=400)

    class Meta:
        managed = False
        db_table = 'species_lizard'


class SpeciesMarlife(models.Model):
    ibin_speciesid = models.CharField(max_length=400, blank=True, null=True)
    ibin_id = models.CharField(max_length=400, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'species_marlife'


class SpeciesMicrobes(models.Model):
    ibin_speciesid = models.CharField(max_length=400, blank=True, null=True)
    ibin_id = models.CharField(max_length=400, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'species_microbes'


class SpeciesPrawns(models.Model):
    genus = models.CharField(primary_key=True, max_length=400)

    class Meta:
        managed = False
        db_table = 'species_prawns'


class SpeciesSnakes(models.Model):
    genus = models.CharField(primary_key=True, max_length=400)

    class Meta:
        managed = False
        db_table = 'species_snakes'


class Speciestaxon(models.Model):
    species_id = models.CharField(primary_key=True, max_length=250)
    species_name = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'speciestaxon'


class SubfamilyHexa(models.Model):
    subfamily_id = models.CharField(primary_key=True, max_length=250)
    subfamily_name = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'subfamily_hexa'


class SupfamilyHexa(models.Model):
    supfamily_id = models.CharField(primary_key=True, max_length=250)
    supfamily_name = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'supfamily_hexa'


class Synonyms(models.Model):
    synonyms = models.CharField(max_length=500, blank=True, null=True)
    spnm = models.CharField(max_length=250, blank=True, null=True)
    ibin_spid = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'synonyms'


class SynonymsCol(models.Model):
    synonyms = models.CharField(max_length=500, blank=True, null=True)
    col_spnm = models.CharField(max_length=250, blank=True, null=True)
    ibin_spnm = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'synonyms_col'


class TempIbinSpecieslist(models.Model):
    ibin_speciesid = models.CharField(primary_key=True, max_length=250)
    ibin_speciesname = models.CharField(max_length=250)
    ibin_scnm = models.CharField(max_length=250)
    author = models.CharField(max_length=250, blank=True, null=True)
    path = models.CharField(max_length=250, blank=True, null=True)
    img_wt = models.IntegerField(blank=True, null=True)
    contributor = models.CharField(max_length=200, blank=True, null=True)
    category = models.CharField(max_length=50, blank=True, null=True)
    genus = models.CharField(max_length=250, blank=True, null=True)
    type = models.CharField(max_length=100, blank=True, null=True)
    chromosme_path = models.CharField(max_length=450, blank=True, null=True)
    chromosomes = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'temp_ibin_specieslist'


class TempMamals(models.Model):
    ibin_speciesid = models.CharField(unique=True, max_length=-1, blank=True, null=True)
    diagnostic_characters = models.CharField(max_length=-1, blank=True, null=True)
    measurements = models.CharField(max_length=-1, blank=True, null=True)
    remarks = models.CharField(max_length=-1, blank=True, null=True)
    ibin_speciesname = models.CharField(max_length=-1, blank=True, null=True)
    contributor = models.CharField(max_length=-1, blank=True, null=True)
    common_name = models.CharField(max_length=-1, blank=True, null=True)
    state = models.CharField(max_length=1, blank=True, null=True)
    district = models.CharField(max_length=-1, blank=True, null=True)
    latitude = models.CharField(max_length=-1, blank=True, null=True)
    longitude = models.CharField(max_length=-1, blank=True, null=True)
    measurments = models.CharField(max_length=-1, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'temp_mamals'


class TempPlantSynonymsCol(models.Model):
    ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
    synonyms = models.CharField(max_length=250, blank=True, null=True)
    source = models.CharField(max_length=50, blank=True, null=True)
    author = models.CharField(max_length=200, blank=True, null=True)
    synonym_species = models.CharField(max_length=250, blank=True, null=True)
    contributor = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'temp_plant_synonyms_col'


class TempPlantsDesc(models.Model):
    ibin_speciesid = models.TextField(blank=True, null=True)
    ibin_speciesname = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    contributor = models.TextField(blank=True, null=True)
    source = models.CharField(max_length=25000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'temp_plants_desc'


class TortoiseImages(models.Model):
    ibin_speciesid = models.CharField(max_length=400, blank=True, null=True)
    img_path = models.CharField(max_length=600, blank=True, null=True)
    ibin_speciesname = models.CharField(max_length=350, blank=True, null=True)
    contributor = models.CharField(max_length=35, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tortoise_images'


class Turtles(models.Model):
    ibin_speciesname = models.TextField(blank=True, null=True)
    common_names = models.TextField(db_column='Common Names', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    habit_habitat = models.TextField(db_column='HABIT & HABITAT', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    economic_importance = models.TextField(db_column='Economic importance', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    threat = models.TextField(db_column='Threat', blank=True, null=True)  # Field name made lowercase.
    distribution = models.TextField(db_column='Distribution', blank=True, null=True)  # Field name made lowercase.
    extent_of_occurrence = models.TextField(db_column='Extent of Occurrence', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    area_of_occurrence = models.TextField(db_column='Area of occurrence', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    altitude = models.TextField(db_column='Altitude', blank=True, null=True)  # Field name made lowercase.
    population = models.TextField(db_column='Population', blank=True, null=True)  # Field name made lowercase.
    conservation_status = models.TextField(db_column='Conservation status', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    size = models.TextField(db_column='Size', blank=True, null=True)  # Field name made lowercase.
    body = models.TextField(db_column='Body', blank=True, null=True)  # Field name made lowercase.
    flippers_limbs = models.TextField(db_column='Flippers/Limbs', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    tail = models.TextField(db_column='Tail', blank=True, null=True)  # Field name made lowercase.
    carapace = models.TextField(db_column='Carapace', blank=True, null=True)  # Field name made lowercase.
    colouration = models.TextField(db_column='Colouration', blank=True, null=True)  # Field name made lowercase.
    colour = models.TextField(blank=True, null=True)
    nesting = models.TextField(db_column='Nesting', blank=True, null=True)  # Field name made lowercase.
    breeding_system = models.TextField(db_column='Breeding system', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    threat_1 = models.TextField(db_column='Threat__1', blank=True, null=True)  # Field name made lowercase. Field renamed because it contained more than one '_' in a row.
    economic_importance_0 = models.TextField(db_column='Economic Importance', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because of name conflict.
    literature = models.TextField(db_column='Literature', blank=True, null=True)  # Field name made lowercase.
    other_biological_details = models.TextField(db_column='Other Biological Details', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    miscellaneous_information = models.TextField(db_column='Miscellaneous Information', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ibin_speciesid = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'turtles'


class UasEgLinks(models.Model):
    grdid = models.DecimalField(max_digits=250, decimal_places=0, blank=True, null=True)
    uni_sp = models.CharField(max_length=1050, blank=True, null=True)
    link = models.CharField(max_length=50000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'uas_eg_links'


class UasWgLinks(models.Model):
    grdid = models.DecimalField(max_digits=250, decimal_places=0, blank=True, null=True)
    uni_sp = models.CharField(max_length=1050, blank=True, null=True)
    link = models.CharField(max_length=50000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'uas_wg_links'


class Users(models.Model):
    user_name = models.CharField(max_length=250, blank=True, null=True)
    password = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'users'


class VegfracBr(models.Model):
    low = models.CharField(max_length=250, blank=True, null=True)
    medium = models.CharField(max_length=250, blank=True, null=True)
    high = models.CharField(max_length=250, blank=True, null=True)
    very_high = models.CharField(max_length=250, blank=True, null=True)
    non_forest = models.CharField(max_length=250, blank=True, null=True)
    total = models.CharField(max_length=250, blank=True, null=True)
    grd_id_1x = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'vegfrac_br'


class VegfracDi(models.Model):
    low = models.CharField(max_length=250, blank=True, null=True)
    medium = models.CharField(max_length=250, blank=True, null=True)
    high = models.CharField(max_length=250, blank=True, null=True)
    very_high = models.CharField(max_length=250, blank=True, null=True)
    non_forest = models.CharField(max_length=250, blank=True, null=True)
    total = models.CharField(max_length=250, blank=True, null=True)
    grd_id_1x = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'vegfrac_di'


class VegfracFrag(models.Model):
    low = models.CharField(max_length=250, blank=True, null=True)
    medium = models.CharField(max_length=250, blank=True, null=True)
    high = models.CharField(max_length=250, blank=True, null=True)
    non_forest = models.CharField(max_length=250, blank=True, null=True)
    total = models.CharField(max_length=250, blank=True, null=True)
    grd_id_1x = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'vegfrac_frag'


class VisitorCount(models.Model):
    counter_page = models.TextField(blank=True, null=True)
    counter_field = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'visitor_count'

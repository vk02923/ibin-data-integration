from django.db import models

from django.contrib.auth.models import User

# widget=forms.TextInput(attrs={'class' : 'myfieldclass'})


class CustomUser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile')
    organisation_name = models.CharField(verbose_name="Organisation name",max_length=100, blank=False)
    piname = models.CharField(verbose_name="PI NAME",max_length=100, blank=False)
    telephone = models.CharField(verbose_name="Telephone",null=False, blank=False,max_length=10)
    qualification=models.TextField(verbose_name="Qualification", blank=False,max_length=100)
    expertise_in_which_family=models.TextField(verbose_name="Expertise in which field",blank=False,max_length=100)
    project_name=models.TextField(verbose_name="Project Name",blank=False,max_length=100)
    image = models.ImageField(upload_to='images/',null=True,blank=True)


    def __str__(self):
        return self.user.username


# Create your models here.

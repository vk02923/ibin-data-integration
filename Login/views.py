from django.shortcuts import render
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib import messages
from.forms import *
from django.http import HttpResponse
from django.contrib.auth import authenticate,login,logout
from django.shortcuts import redirect
from django.http import JsonResponse

def Login(request):
    if request.user.is_authenticated:
        return redirect("plantsystemhome")
    else:
        if request.method == 'POST':
            form = authForm(request=request, data=request.POST)
            if form.is_valid():
                username = form.cleaned_data.get('username')
                password = form.cleaned_data.get('password')
                user = authenticate(username=username, password=password)
                if user is not None:
                    login(request, user)
                    print("yes")
                    messages.info(request, f"You are now logged in as {username}")
                    return redirect('plantsystemhome')
                else:
                    print("no")
                    # error="Username and Password combination is incorrect"
                    return render(request = request,
                        template_name = "index.html",
                        context={"form":form})

            else:
                print("no")
                error="Captcha is incorrect"
                return render(request = request,
                        template_name = "index.html",
                        context={"form":form,"error":error})
        form = authForm()
        return render(request = request,
                        template_name = "Login/index.html",
                        context={"form":form})



def logout_request(request):
    logout(request)
    return redirect("Login")





    # return  redirect('plantsystemhome')

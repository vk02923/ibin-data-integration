from django.forms import ModelForm
from django.contrib.auth.forms import AuthenticationForm
from .models import *
from captcha.fields import CaptchaField


class authForm(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super(authForm, self).__init__(*args, **kwargs)

    captcha = CaptchaField()
        
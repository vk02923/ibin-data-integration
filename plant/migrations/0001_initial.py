# Generated by Django 3.1.5 on 2021-06-22 06:45

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Plantsmodel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('validated', models.BooleanField(default=False)),
                ('user_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='PlantUses_temp',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('part', models.CharField(blank=True, max_length=250, null=True)),
                ('uses', models.CharField(blank=True, max_length=250, null=True)),
                ('flora_name', models.CharField(blank=True, max_length=200, null=True)),
                ('flora_volume', models.CharField(blank=True, max_length=100, null=True)),
                ('author', models.CharField(blank=True, max_length=150, null=True)),
                ('edition', models.CharField(blank=True, max_length=50, null=True)),
                ('yop', models.CharField(blank=True, max_length=10, null=True)),
                ('contributor', models.CharField(blank=True, max_length=200, null=True)),
                ('RecordID', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='plant.plantsmodel')),
            ],
        ),
        migrations.CreateModel(
            name='PlantTaxonomy_temp',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('kingdom_id', models.CharField(blank=True, max_length=250, null=True)),
                ('phylum_id', models.CharField(blank=True, max_length=250, null=True)),
                ('class_id', models.CharField(blank=True, max_length=250, null=True)),
                ('order_id', models.CharField(blank=True, max_length=250, null=True)),
                ('family_id', models.CharField(blank=True, max_length=250, null=True)),
                ('genus_id', models.CharField(blank=True, max_length=250, null=True)),
                ('species_id', models.CharField(blank=True, max_length=250, null=True)),
                ('RecordID', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='plant.plantsmodel')),
            ],
        ),
        migrations.CreateModel(
            name='PlantSynonymsCol_temp',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('synonyms', models.CharField(blank=True, max_length=100000, null=True)),
                ('source', models.CharField(blank=True, max_length=100000, null=True)),
                ('author', models.CharField(blank=True, max_length=100000, null=True)),
                ('synonym_species', models.CharField(blank=True, max_length=100000, null=True)),
                ('contributor', models.CharField(blank=True, max_length=100000, null=True)),
                ('species_id', models.CharField(blank=True, max_length=100000, null=True)),
                ('RecordID', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='plant.plantsmodel')),
            ],
        ),
        migrations.CreateModel(
            name='PlantsDiagnostic_temp',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ibin_speciesname', models.CharField(blank=True, max_length=450, null=True)),
                ('diagnostic', models.CharField(blank=True, max_length=45000, null=True)),
                ('RecordID', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='plant.plantsmodel')),
            ],
        ),
        migrations.CreateModel(
            name='PlantsDesc_temp',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', models.TextField(blank=True, null=True)),
                ('contributor', models.TextField(blank=True, null=True)),
                ('source', models.CharField(blank=True, max_length=25000, null=True)),
                ('RecordID', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='plant.plantsmodel')),
            ],
        ),
        migrations.CreateModel(
            name='PlantHabitat_temp',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('habitat', models.CharField(blank=True, max_length=250000, null=True)),
                ('flora_name', models.CharField(blank=True, max_length=200, null=True)),
                ('flora_volume', models.CharField(blank=True, max_length=100, null=True)),
                ('author', models.CharField(blank=True, max_length=150, null=True)),
                ('edition', models.CharField(blank=True, max_length=50, null=True)),
                ('yop', models.CharField(blank=True, max_length=10, null=True)),
                ('contributor', models.CharField(blank=True, max_length=200, null=True)),
                ('RecordID', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='plant.plantsmodel')),
            ],
        ),
        migrations.CreateModel(
            name='PlantGenus_temp',
            fields=[
                ('genus_id', models.CharField(max_length=250, primary_key=True, serialize=False)),
                ('genus_name', models.CharField(max_length=50)),
                ('RecordID', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='plant.plantsmodel')),
            ],
        ),
        migrations.CreateModel(
            name='PlantConsStatus_temp',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('iucn_status', models.CharField(blank=True, max_length=100000, null=True)),
                ('assess_date', models.CharField(blank=True, max_length=200, null=True)),
                ('source', models.CharField(blank=True, max_length=200, null=True)),
                ('contributor', models.TextField(blank=True, null=True)),
                ('RecordID', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='plant.plantsmodel')),
            ],
        ),
        migrations.CreateModel(
            name='PlantCommonName_temp',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('language', models.CharField(blank=True, max_length=250, null=True)),
                ('common_name', models.CharField(blank=True, max_length=200, null=True)),
                ('flora_name', models.CharField(blank=True, max_length=200, null=True)),
                ('flora_volume', models.CharField(blank=True, max_length=100, null=True)),
                ('author', models.CharField(blank=True, max_length=150, null=True)),
                ('edition', models.CharField(blank=True, max_length=50, null=True)),
                ('yop', models.CharField(blank=True, max_length=10, null=True)),
                ('contributor', models.CharField(blank=True, max_length=200, null=True)),
                ('RecordID', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='plant.plantsmodel')),
            ],
        ),
        migrations.CreateModel(
            name='PlantBioticZone_temp',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('bioticzone', models.CharField(blank=True, max_length=250, null=True)),
                ('flora_name', models.CharField(blank=True, max_length=200, null=True)),
                ('flora_volume', models.CharField(blank=True, max_length=20, null=True)),
                ('author', models.CharField(blank=True, max_length=100, null=True)),
                ('edition', models.CharField(blank=True, max_length=150, null=True)),
                ('yop', models.CharField(blank=True, max_length=10, null=True)),
                ('contributor', models.CharField(blank=True, max_length=200, null=True)),
                ('RecordID', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='plant.plantsmodel')),
            ],
        ),
        migrations.CreateModel(
            name='IbinSpecieslist_temp',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ibin_speciesname', models.CharField(blank=True, max_length=10000, null=True)),
                ('ibin_scnm', models.CharField(blank=True, max_length=10000, null=True)),
                ('author', models.CharField(blank=True, max_length=10000, null=True)),
                ('path', models.ImageField(blank=True, null=True, upload_to='images/')),
                ('img_wt', models.CharField(blank=True, max_length=10000, null=True)),
                ('contributor', models.CharField(blank=True, max_length=10000, null=True)),
                ('category', models.CharField(blank=True, max_length=10000, null=True)),
                ('genus', models.CharField(blank=True, max_length=10000, null=True)),
                ('type', models.CharField(blank=True, max_length=10000, null=True)),
                ('chromosme_path', models.CharField(blank=True, max_length=10000, null=True)),
                ('chromosomes', models.CharField(blank=True, max_length=10000, null=True)),
                ('species_id', models.CharField(blank=True, max_length=10000, null=True)),
                ('RecordID', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='plant.plantsmodel')),
            ],
        ),
    ]

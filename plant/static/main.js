
//jQuery time
$( document ).ready(function() {
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches


    
$('.captcha').click(function () {
    $.getJSON("/captcha/refresh/", function (result) {
        $('.captcha').attr('src', result['image_url']);
        $('#id_captcha_0').val(result['key'])
    });
});



// Show the first tab and hide the rest
$('#tabs-nav li:first-child').addClass('active');
$('.tab-content').hide();
$('.tab-content:first').show();

// Click function
$('#tabs-nav li').click(function(){
  $('#tabs-nav li').removeClass('active');
  $(this).addClass('active');
  $('.tab-content').hide();

  $(this).addClass('active');
  
  
  // $(".list-group-item ").css("color", "white");
  
  
  var activeTab = $(this).find('a').attr('href');
  $(activeTab).fadeIn();
  return false;
});


});




// click on table 


// $('td').click(function(){
//   var row_index = $(this).parent().index();
//   var col_index = $(this).index();
//   var currentRow=$(this).closest("tr"); 
       
//   // var col1=currentRow.text(); // get current row 1st TD value
//   // var k= $(".dataframe").find("tr:first td").length;
//   //   alert(row_index)
//   //   // const link=window.reverse('edit_form', { pk: row_index });
//   //   document.location.href=window.reverse('edit_form', { pk: row_index });

//   alert(currentRow.text().toArray())
 
// });







// csv

$(document).ready(function(){

  // code to read selected table row cell data (values).
  $(".dataframe").on('click','.btnSelect',function(){
       // get the current row
       var currentRow=$(this).closest("tr"); 
       
       var col1=currentRow.text(); // get current row 1st TD value
      
       
       alert(col1);
  });
});



// model

// Open modal in AJAX callback



// edit table

(function($, window, document, undefined) {
  var pluginName = "editable",
    defaults = {
      keyboard: true,
      dblclick: true,
      button: true,
      buttonSelector: ".edit",
      maintainWidth: true,
      dropdowns: {},
      edit: function() {},
      save: function() {},
      cancel: function() {}
    };

  function editable(element, options) {
    this.element = element;
    this.options = $.extend({}, defaults, options);

    this._defaults = defaults;
    this._name = pluginName;

    this.init();
  }

  editable.prototype = {
    init: function() {
      this.editing = false;

      if (this.options.dblclick) {
        $(this.element)
          .css('cursor', 'pointer')
          .bind('dblclick', this.toggle.bind(this));
      }

      if (this.options.button) {
        $(this.options.buttonSelector, this.element)
          .bind('click', this.toggle.bind(this));
      }
    },

    toggle: function(e) {
      e.preventDefault();

      this.editing = !this.editing;

      if (this.editing) {
        this.edit();
      } else {
        this.save();
      }
    },

    edit: function() {
      var instance = this,
        values = {};

      $('td[data-field]', this.element).each(function() {
        var input,
          field = $(this).data('field'),
          value = $(this).text(),
          width = $(this).width();

        values[field] = value;

        $(this).empty();

        if (instance.options.maintainWidth) {
          $(this).width(width);
        }

        if (field in instance.options.dropdowns) {
          input = $('<select></select>');

          for (var i = 0; i < instance.options.dropdowns[field].length; i++) {
            $('<option></option>')
              .text(instance.options.dropdowns[field][i])
              .appendTo(input);
          };

          input.val(value)
            .data('old-value', value)
            .dblclick(instance._captureEvent);
        } else {
          input = $('<input type="text" />')
            .val(value)
            .data('old-value', value)
            .dblclick(instance._captureEvent);
        }

        input.appendTo(this);

        if (instance.options.keyboard) {
          input.keydown(instance._captureKey.bind(instance));
        }
      });

      this.options.edit.bind(this.element)(values);
    },

    save: function() {
      var instance = this,
        values = {};

      $('td[data-field]', this.element).each(function() {
        var value = $(':input', this).val();

        values[$(this).data('field')] = value;

        $(this).empty()
          .text(value);
      });

      this.options.save.bind(this.element)(values);
    },

    cancel: function() {
      var instance = this,
        values = {};

      $('td[data-field]', this.element).each(function() {
        var value = $(':input', this).data('old-value');

        values[$(this).data('field')] = value;

        $(this).empty()
          .text(value);
      });

      this.options.cancel.bind(this.element)(values);
    },

    _captureEvent: function(e) {
      e.stopPropagation();
    },

    _captureKey: function(e) {
      if (e.which === 13) {
        this.editing = false;
        this.save();
      } else if (e.which === 27) {
        this.editing = false;
        this.cancel();
      }
    }
  };

  $.fn[pluginName] = function(options) {
    return this.each(function() {
      if (!$.data(this, "plugin_" + pluginName)) {
        $.data(this, "plugin_" + pluginName,
          new editable(this, options));
      }
    });
  };

})(jQuery, window, document);

editTable();

//custome editable starts
function editTable(){
  
  $(function() {
  var pickers = {};

  $('table tr').editable({
    dropdowns: {
      sex: ['Male', 'Female']
    },
    edit: function(values) {
      $(".edit i", this)
        .removeClass('fa-pencil')
        .addClass('fa-save')
        .attr('title', 'Save');

      pickers[this] = new Pikaday({
        field: $("td[data-field=birthday] input", this)[0],
        format: 'MMM D, YYYY'
      });
    },
    save: function(values) {
      $(".edit i", this)
        .removeClass('fa-save')
        .addClass('fa-pencil')
        .attr('title', 'Edit');

      if (this in pickers) {
        pickers[this].destroy();
        delete pickers[this];
      }
    },
    cancel: function(values) {
      $(".edit i", this)
        .removeClass('fa-save')
        .addClass('fa-pencil')
        .attr('title', 'Edit');

      if (this in pickers) {
        pickers[this].destroy();
        delete pickers[this];
      }
    }
  });
});
  
}

$(".add-row").click(function(){
  $("#editableTable").find("tbody tr:first").before("<tr><td data-field='name'></td><td data-field='name'></td><td data-field='name'></td><td data-field='name'></td><td><a class='button button-small edit' title='Edit'><i class='fa fa-pencil'></i></a> <a class='button button-small' title='Delete'><i class='fa fa-trash'></i></a></td></tr>");   
  editTable();  
  setTimeout(function(){   
    $("#editableTable").find("tbody tr:first td:last a[title='Edit']").click(); 
  }, 200); 
  
  setTimeout(function(){ 
    $("#editableTable").find("tbody tr:first td:first input[type='text']").focus();
      }, 300); 
  
   $("#editableTable").find("a[title='Delete']").unbind('click').click(function(e){
        $(this).closest("tr").remove();
    });
   
});

function myFunction() {
    
}

$("#editableTable").find("a[title='Delete']").click(function(e){  
  var x;
    if (confirm("Are you sure you want to delete entire row?") == true) {
        $(this).closest("tr").remove();
    } else {
        
    }     
});

// $('#editableTable').DataTable( {
//   "paging":   false
// } );


// POST TABLE DATA

// $( "#save_csv" ).click(function() {
//   alert( "Handler for .click() called." );

// var proParams=[]; // <-- declare as array
// var param = null;
// var rows = $("#editableTable").dataTable().fnGetNodes();
// for (var i = 0; i < rows.length; i++) {
//     var aData = $("#editableTable").dataTable().fnGetData(i);
//     param = new Object(); // <-- creates a new object
//     param.SpeciesID = aData[0];
//     param.GlobalUniqueIdentfier = aData[1];
//     param.Abstract = aData[2];
//     param.RecordBasis = aData[3];
//     param.Genus = aData[4];
//     param.Specie = aData[5];
//     param.Race = aData[6];
//     param.Strains = aData[7];
//     param.CommonName = aData[8];
//     param.Language = aData[9];
//     param.Kingdom = aData[10];
//     param.Phylum = aData[11];
//     param.Class = aData[12];
//     param.Order = aData[13];
//     param.Family = aData[14];
   
    
//     proParams.push(param); // <-- push a new element into array
// }
// var js=JSON.stringify(proParams)


// $.ajax({
//   type: "POST",
//   url: window.reverse('save_form'),
//   data: js,
//   dataType: "json",
//   success: function(result) {

//     //Write your code here
//       },
// });

// // window.location.href=window.reverse('save_form',{ pk: proParams });

// });


$(".edit_save").click(function() {
  alert()
  $(".button span").html($(".button span").html() == 'followed' ? 'follow' : 'followed');
});

$('#editableTable').dataTable( {
  "paging":   false,
  "columnDefs": [
    { "width": "100%", "targets": 0 }
  ]
} );



if (window.matchMedia("(max-width: 767px)").matches)  
{ 
    
    // The viewport is less than 768 pixels wide 
    $(".mobile_tabs").css({"display": "none"});

    $(".mobile_tabs_screen").css({"display": "block"});
    
   
} else{
  $(".mobile_tabs_screen").css({"display": "none"});
  

 
  
}



$(document).ready(function () {
  $("a,:input").click(function () {
      refresh = false;
  });

});


$(document).ready(function(){
  $(".dsk").click(function() {
      $("html, body").animate({ 
          scrollTop: 0 
      }, "slow");
      return false;
  });
});


// var curr;
// var prev=null;

//   function reply_click(clicked_id)
//   {
    // alert("."+clicked_id)
    // $("."+clicked_id).removeAttr('color');
    // $("."+clicked_id).css("background-color", "white");
  //   curr=clicked_id;
  //   prev=clicked_id;

  //   $('#'+curr).addClass('text-light');
  //   $('#'+curr).addClass('font-weight-bold');

  

    

    
   
  // }

  // function change(prev,curr){
  //   $('#'+prev).addClass('text-light');

  //   $('#'+curr).addClass('text-light');
  //   $('#'+curr).addClass('font-weight-bold');
    

  // }


  

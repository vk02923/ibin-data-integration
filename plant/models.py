from django.db import models
from django.contrib.auth.models import User
import string
from datetime import date,datetime






# class PlantBioticZone(models.Model):
#     ibin_speciesid = models.CharField(max_length=200, blank=True, null=True)
#     bioticzone = models.CharField(max_length=250, blank=True, null=True)
#     flora_name = models.CharField(max_length=200, blank=True, null=True)
#     flora_volume = models.CharField(max_length=20, blank=True, null=True)
#     author = models.CharField(max_length=100, blank=True, null=True)
#     edition = models.CharField(max_length=150, blank=True, null=True)
#     yop = models.CharField(max_length=10, blank=True, null=True)
#     contributor = models.CharField(max_length=200, blank=True, null=True)

#     class Meta:
#         managed = False
#         db_table = 'plant_biotic_zone'



# class PlantCommonName(models.Model):
#     ibin_speciesid = models.CharField(max_length=200, blank=True, null=True)
#     language = models.CharField(max_length=250, blank=True, null=True)
#     common_name = models.CharField(max_length=200, blank=True, null=True)
#     flora_name = models.CharField(max_length=200, blank=True, null=True)
#     flora_volume = models.CharField(max_length=100, blank=True, null=True)
#     author = models.CharField(max_length=150, blank=True, null=True)
#     edition = models.CharField(max_length=50, blank=True, null=True)
#     yop = models.CharField(max_length=10, blank=True, null=True)
#     contributor = models.CharField(max_length=200, blank=True, null=True)

#     class Meta:
#         managed = False
#         db_table = 'plant_common_name'



# class PlantConsStatus(models.Model):
#     ibin_speciesid = models.CharField(max_length=250, blank=True, null=True)
#     iucn_status = models.CharField(max_length=100000, blank=True, null=True)
#     assess_date = models.CharField(max_length=200, blank=True, null=True)
#     source = models.CharField(max_length=200, blank=True, null=True)
#     contributor = models.TextField(blank=True, null=True)

#     class Meta:
#         managed = False
#         db_table = 'plant_cons_status'



# class PlantGenus(models.Model):
#     genus_id = models.CharField(primary_key=True, max_length=250)
#     genus_name = models.CharField(max_length=50)

#     class Meta:
#         managed = False
#         db_table = 'plant_genus'



# class PlantSynonymsCol(models.Model):
#     ibin_speciesid = models.CharField(max_length=100000, blank=True, null=True)
#     synonyms = models.CharField(max_length=100000, blank=True, null=True)
#     source = models.CharField(max_length=100000, blank=True, null=True)
#     author = models.CharField(max_length=100000, blank=True, null=True)
#     synonym_species = models.CharField(max_length=100000, blank=True, null=True)
#     contributor = models.CharField(max_length=100000, blank=True, null=True)
#     species_id = models.CharField(max_length=100000, blank=True, null=True)

#     class Meta:
#         managed = False
#         db_table = 'plant_synonyms_col'




# class PlantHabitat(models.Model):
#     ibin_speciesid = models.CharField(max_length=200, blank=True, null=True)
#     habitat = models.CharField(max_length=250000, blank=True, null=True)
#     flora_name = models.CharField(max_length=200, blank=True, null=True)
#     flora_volume = models.CharField(max_length=100, blank=True, null=True)
#     author = models.CharField(max_length=150, blank=True, null=True)
#     edition = models.CharField(max_length=50, blank=True, null=True)
#     yop = models.CharField(max_length=10, blank=True, null=True)
#     contributor = models.CharField(max_length=200, blank=True, null=True)

#     class Meta:
#         managed = False
#         db_table = 'plant_habitat'



# class PlantTaxonomy(models.Model):
#     ibin_speciesid = models.CharField(max_length=250)
#     kingdom_id = models.CharField(max_length=250, blank=True, null=True)
#     phylum_id = models.CharField(max_length=250, blank=True, null=True)
#     class_id = models.CharField(max_length=250, blank=True, null=True)
#     order_id = models.CharField(max_length=250, blank=True, null=True)
#     family_id = models.CharField(max_length=250, blank=True, null=True)
#     genus_id = models.CharField(max_length=250, blank=True, null=True)
#     species_id = models.CharField(max_length=250, blank=True, null=True)

#     class Meta:
#         managed = False
#         db_table = 'plant_taxonomy'



# class PlantUses(models.Model):
#     ibin_speciesid = models.CharField(max_length=200, blank=True, null=True)
#     part = models.CharField(max_length=250, blank=True, null=True)
#     uses = models.CharField(max_length=250, blank=True, null=True)
#     flora_name = models.CharField(max_length=200, blank=True, null=True)
#     flora_volume = models.CharField(max_length=100, blank=True, null=True)
#     author = models.CharField(max_length=150, blank=True, null=True)
#     edition = models.CharField(max_length=50, blank=True, null=True)
#     yop = models.CharField(max_length=10, blank=True, null=True)
#     contributor = models.CharField(max_length=200, blank=True, null=True)

#     class Meta:
#         managed = False
#         db_table = 'plant_uses'



# class PlantsDesc(models.Model):
#     ibin_speciesid = models.TextField()
#     description = models.TextField(blank=True, null=True)
#     contributor = models.TextField(blank=True, null=True)
#     source = models.CharField(max_length=25000, blank=True, null=True)

#     class Meta:
#         managed = False
#         db_table = 'plants_desc'




# class PlantsDiagnostic(models.Model):
#     ibin_speciesname = models.CharField(max_length=450, blank=True, null=True)
#     diagnostic = models.CharField(max_length=45000, blank=True, null=True)
#     ibin_speciesid = models.CharField(max_length=450, blank=True, null=True)

#     class Meta:
#         managed = False
#         db_table = 'plants_diagnostic'





















# ----------------------







class Plantsmodel(models.Model):
    user_id=models.ForeignKey(User,on_delete=models.CASCADE)
    validated=models.BooleanField(default=False)

    def __str__(self):
        return (self.user_id.username + "_" + str(self.id))



class IbinSpecieslist_temp(models.Model):
    RecordID=models.ForeignKey(Plantsmodel,on_delete=models.CASCADE)
    ibin_speciesname = models.CharField(max_length=10000, blank=True, null=True)
    ibin_scnm = models.CharField(max_length=10000, blank=True, null=True)
    author = models.CharField(max_length=10000, blank=True, null=True)
    path = models.ImageField(upload_to='images/',null=True,blank=True)
    img_wt = models.CharField(max_length=10000, blank=True, null=True)
    contributor = models.CharField(max_length=10000, blank=True, null=True)
    category = models.CharField(max_length=10000, blank=True, null=True)
    genus = models.CharField(max_length=10000, blank=True, null=True)
    type = models.CharField(max_length=10000, blank=True, null=True)
    chromosme_path = models.CharField(max_length=10000, blank=True, null=True)
    chromosomes = models.CharField(max_length=10000, blank=True, null=True)
    species_id = models.CharField(max_length=10000, blank=True, null=True)

    def __str__(self):
        return  str(self.RecordID)


class PlantBioticZone_temp(models.Model):
    RecordID=models.ForeignKey(Plantsmodel,on_delete=models.CASCADE)
    bioticzone = models.CharField(max_length=250, blank=True, null=True)
    flora_name = models.CharField(max_length=200, blank=True, null=True)
    flora_volume = models.CharField(max_length=20, blank=True, null=True)
    author = models.CharField(max_length=100, blank=True, null=True)
    edition = models.CharField(max_length=150, blank=True, null=True)
    yop = models.CharField(max_length=10, blank=True, null=True)
    contributor = models.CharField(max_length=200, blank=True, null=True)

    def __str__(self):
        return  str(self.RecordID)



class PlantCommonName_temp(models.Model):
    RecordID=models.ForeignKey(Plantsmodel,on_delete=models.CASCADE)
    language = models.CharField(max_length=250, blank=True, null=True)
    common_name = models.CharField(max_length=200, blank=True, null=True)
    flora_name = models.CharField(max_length=200, blank=True, null=True)
    flora_volume = models.CharField(max_length=100, blank=True, null=True)
    author = models.CharField(max_length=150, blank=True, null=True)
    edition = models.CharField(max_length=50, blank=True, null=True)
    yop = models.CharField(max_length=10, blank=True, null=True)
    contributor = models.CharField(max_length=200, blank=True, null=True)

    def __str__(self):
        return  str(self.RecordID)



class PlantConsStatus_temp(models.Model):
    RecordID=models.ForeignKey(Plantsmodel,on_delete=models.CASCADE)
    iucn_status = models.CharField(max_length=100000, blank=True, null=True)
    assess_date = models.CharField(max_length=200, blank=True, null=True)
    source = models.CharField(max_length=200, blank=True, null=True)
    contributor = models.TextField(blank=True, null=True)

    def __str__(self):
        return  str(self.RecordID)


class PlantGenus_temp(models.Model):
    RecordID=models.ForeignKey(Plantsmodel,on_delete=models.CASCADE)
    genus_id = models.CharField(primary_key=True, max_length=250)
    genus_name = models.CharField(max_length=50)

    def __str__(self):
        return  str(self.RecordID)



class PlantSynonymsCol_temp(models.Model):
    RecordID=models.ForeignKey(Plantsmodel,on_delete=models.CASCADE)
    synonyms = models.CharField(max_length=100000, blank=True, null=True)
    source = models.CharField(max_length=100000, blank=True, null=True)
    author = models.CharField(max_length=100000, blank=True, null=True)
    synonym_species = models.CharField(max_length=100000, blank=True, null=True)
    contributor = models.CharField(max_length=100000, blank=True, null=True)
    species_id = models.CharField(max_length=100000, blank=True, null=True)

    def __str__(self):
        return  str(self.RecordID)




class PlantHabitat_temp(models.Model):
    RecordID=models.ForeignKey(Plantsmodel,on_delete=models.CASCADE)
    habitat = models.CharField(max_length=250000, blank=True, null=True)
    flora_name = models.CharField(max_length=200, blank=True, null=True)
    flora_volume = models.CharField(max_length=100, blank=True, null=True)
    author = models.CharField(max_length=150, blank=True, null=True)
    edition = models.CharField(max_length=50, blank=True, null=True)
    yop = models.CharField(max_length=10, blank=True, null=True)
    contributor = models.CharField(max_length=200, blank=True, null=True)

    def __str__(self):
        return  str(self.RecordID)



class PlantTaxonomy_temp(models.Model):
    RecordID=models.ForeignKey(Plantsmodel,on_delete=models.CASCADE)
    kingdom_id = models.CharField(max_length=250, blank=True, null=True)
    phylum_id = models.CharField(max_length=250, blank=True, null=True)
    class_id = models.CharField(max_length=250, blank=True, null=True)
    order_id = models.CharField(max_length=250, blank=True, null=True)
    family_id = models.CharField(max_length=250, blank=True, null=True)
    genus_id = models.CharField(max_length=250, blank=True, null=True)
    species_id = models.CharField(max_length=250, blank=True, null=True)

    def __str__(self):
        return  str(self.RecordID)



class PlantUses_temp(models.Model):
    RecordID=models.ForeignKey(Plantsmodel,on_delete=models.CASCADE)
    part = models.CharField(max_length=250, blank=True, null=True)
    uses = models.CharField(max_length=250, blank=True, null=True)
    flora_name = models.CharField(max_length=200, blank=True, null=True)
    flora_volume = models.CharField(max_length=100, blank=True, null=True)
    author = models.CharField(max_length=150, blank=True, null=True)
    edition = models.CharField(max_length=50, blank=True, null=True)
    yop = models.CharField(max_length=10, blank=True, null=True)
    contributor = models.CharField(max_length=200, blank=True, null=True)

    def __str__(self):
        return  str(self.RecordID)



class PlantsDesc_temp(models.Model):
    RecordID=models.ForeignKey(Plantsmodel,on_delete=models.CASCADE)
    description = models.TextField(blank=True, null=True)
    contributor = models.TextField(blank=True, null=True)
    source = models.CharField(max_length=25000, blank=True, null=True)

    def __str__(self):
        return  str(self.RecordID)




class PlantsDiagnostic_temp(models.Model):
    ibin_speciesname = models.CharField(max_length=450, blank=True, null=True)
    diagnostic = models.CharField(max_length=45000, blank=True, null=True)
    RecordID=models.ForeignKey(Plantsmodel,on_delete=models.CASCADE)

    def __str__(self):
        return  str(self.RecordID)







class uploaded_csv(models.Model):
    User_Id=models.ForeignKey(User,on_delete=models.CASCADE)
    filename=models.TextField(blank=False,null=False)
    filepath=models.TextField(blank=False,null=False)
    status=models.CharField(max_length=256, choices=[('1', 'denied'), ('2', 'processing'),('3', 'processed'),('4', 'success'),('5', 'upload successful')])
    date=models.DateField(default=date.today)
    def __str__(self):
        return  str(self.filename)


class uploaded_records(models.Model):
    User_Id=models.ForeignKey(User,on_delete=models.CASCADE)
    RecordID=models.ForeignKey(Plantsmodel,on_delete=models.CASCADE)
    status=models.CharField(max_length=256, choices=[('1', 'denied'), ('2', 'processing'),('3', 'processed'),('4', 'success'),('5', 'upload successful')])
    date=models.DateField(default=date.today)
    def __str__(self):
        return  str(self.RecordID)  









class PlantGenus(models.Model):
    genus_id = models.CharField(primary_key=True, max_length=250)
    genus_name = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'plant_genus'







class IbinSpecieslist(models.Model):
    ibin_speciesid = models.CharField(max_length=800, blank=True, null=True)
    ibin_speciesname = models.CharField(max_length=800, blank=True, null=True)
    ibin_scnm = models.CharField(max_length=800, blank=True, null=True)
    author = models.CharField(max_length=800, blank=True, null=True)
    path = models.CharField(max_length=800, blank=True, null=True)
    img_wt = models.CharField(max_length=800, blank=True, null=True)
    contributor = models.CharField(max_length=800, blank=True, null=True)
    category = models.CharField(max_length=800, blank=True, null=True)
    genus = models.CharField(max_length=800, blank=True, null=True)
    type = models.CharField(max_length=800, blank=True, null=True)
    chromosme_path = models.CharField(max_length=800, blank=True, null=True)
    chromosomes = models.CharField(max_length=800, blank=True, null=True)
    # species_id = models.CharField(max_length=800, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ibin_specieslist'
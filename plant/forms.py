from django.forms import ModelForm
from .models import *
from django import forms


class IbinSpecieslist_temp_form(ModelForm):
      class Meta:
        model = IbinSpecieslist_temp
        fields = "__all__"
        exclude= ('RecordID','img_wt','category','type','chromosme_path','chromosomes','species_id')
      def __init__(self, *args, **kwargs):
        super(IbinSpecieslist_temp_form, self).__init__(*args, **kwargs)
      
        self.fields['ibin_speciesname'].label = "Species Name"
        self.fields['ibin_scnm'].label = "Species Name with Author"
        self.fields['path'].label = "Thumbnail"
        self.fields['ibin_speciesname'].widget=forms.TextInput(attrs={ 'id':'tags' })
         



class PlantBioticZone_temp_form(ModelForm):
     class Meta:
        model = PlantBioticZone_temp
        fields = "__all__"
        exclude= ('RecordID',)
        

class PlantCommonName_temp_form(ModelForm):
     class Meta:
        model = PlantCommonName_temp
        fields = "__all__"
        exclude= ('RecordID',)
     def __init__(self, *args, **kwargs):
        super(PlantCommonName_temp_form, self).__init__(*args, **kwargs)
      
        self.fields['yop'].label = "Year of Publication"
      



class PlantConsStatus_temp_form(ModelForm):
     class Meta:
        model = PlantConsStatus_temp
        fields = "__all__"
        exclude= ('RecordID',)



class PlantSynonymsCol_temp_form(ModelForm):
     class Meta:
        model = PlantSynonymsCol_temp
        fields = "__all__"
        exclude= ('RecordID','species_id')


class PlantHabitat_temp_form(ModelForm):
      class Meta:
        model = PlantHabitat_temp
        fields = "__all__"
        exclude= ('RecordID',)
      def __init__(self, *args, **kwargs):
        super(PlantHabitat_temp_form, self).__init__(*args, **kwargs)
      
        self.fields['yop'].label = "Year of Publication"


class PlantTaxonomy_temp_form(ModelForm):
     class Meta:
        model = PlantTaxonomy_temp
        fields = "__all__"
        exclude= ('RecordID',)



class PlantUses_temp_form(ModelForm):
      class Meta:
        model = PlantUses_temp
        fields = "__all__"
        exclude= ('RecordID',)
      def __init__(self, *args, **kwargs):
        super(PlantUses_temp_form, self).__init__(*args, **kwargs)
      
        self.fields['yop'].label = "Year of Publication"  




class PlantsDesc_temp_form(ModelForm):
     class Meta:
        model = PlantsDesc_temp
        fields = "__all__"
        exclude= ('RecordID',)





class PlantsDiagnostic_temp_form(ModelForm):
      class Meta:
        model = PlantsDiagnostic_temp
        fields = "__all__"
        exclude= ('RecordID',)
      def __init__(self, *args, **kwargs):
        super(PlantsDiagnostic_temp_form, self).__init__(*args, **kwargs)
      
        self.fields['ibin_speciesname'].label = "Species Name"  
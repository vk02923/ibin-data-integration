from django.shortcuts import render
from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from .forms import *
import pandas as pd
from django.contrib import messages
import csv
import chardet 
from django.shortcuts import redirect
import json
from django.views.decorators.csrf import csrf_exempt
import simplejson as jsonn
from django.http import JsonResponse
from django.db import connection
from django.http import HttpResponse
from django.core.files.storage import FileSystemStorage
import psycopg2

# Create your views here.


@login_required
def input_field(request):


    
    if 'term' in request.GET:
        conn = psycopg2.connect(
        database="ibin_new", user='iirs', password='IBIN', host='127.0.0.1', port= '5432'
        )
        cursor = conn.cursor()


        s="select * from ibin_specieslist where ibin_speciesname like '"+request.GET.get('term')+"%' "

        cursor.execute(s)


        mobile_records = cursor.fetchall()

        print(mobile_records)


        # print("inside")
        # print(request.GET.get('term'))
        
        # qs=IbinSpecieslist.objects.get(genus__contains=request.GET.get('term'))
        
        exp=list()
        for prod in mobile_records:
            exp.append(prod[1])
        print(exp)
        return JsonResponse(exp,safe=False)



    

    if request.method == 'POST':
            print("form submitted")
        
            splisform=IbinSpecieslist_temp_form(request.POST)
            bioticform=PlantBioticZone_temp_form(request.POST)
            commonname=PlantCommonName_temp_form(request.POST)
            consstatus=PlantConsStatus_temp_form(request.POST)
            synonyms=PlantSynonymsCol_temp_form(request.POST)
            habitat=PlantHabitat_temp_form(request.POST)
            taxanomy=PlantTaxonomy_temp_form(request.POST)
            usesform=PlantUses_temp_form(request.POST)
            desc=PlantsDesc_temp_form(request.POST)
            diagnostic=PlantsDiagnostic_temp_form(request.POST)
           
            # herberiumfiles=request.FILES.getlist('Herberium')
            # LineDiagramfiles=request.FILES.getlist('LineDiagram')
            # others=request.FILES.getlist('others')
            # photographs=request.FILES.getlist('photographs')

            # print(herberiumfiles)



            if  splisform.is_valid() and bioticform.is_valid() and commonname.is_valid() and consstatus.is_valid() and synonyms.is_valid() and habitat.is_valid() and taxanomy.is_valid() and usesform.is_valid and desc.is_valid() and diagnostic.is_valid()  :

                print("validated")
                datamodel = Plantsmodel(user_id=request.user,validated=False)
                datamodel.save()
                # belement=BaseElement(RecordID=datamodel,SpeciesID=Baseform.cleaned_data.get('SpeciesID'),GlobalUniqueIdentfier=Baseform.cleaned_data.get('GlobalUniqueIdentfier'),Abstract=Baseform.cleaned_data.get('Abstract'),RecordBasis=Baseform.cleaned_data.get('RecordBasis'))

                obj=splisform.save(commit=False)
                obj.RecordID=datamodel
                obj.save()

                obj=bioticform.save(commit=False)
                obj.RecordID=datamodel
                obj.save()

                obj=commonname.save(commit=False)
                obj.RecordID=datamodel
                obj.save()

                obj=consstatus.save(commit=False)
                obj.RecordID=datamodel
                obj.save()

                obj=synonyms.save(commit=False)
                obj.RecordID=datamodel
                obj.save()

                obj=habitat.save(commit=False)
                obj.RecordID=datamodel
                obj.save()

                obj=taxanomy.save(commit=False)
                obj.RecordID=datamodel
                obj.save()


                obj=usesform.save(commit=False)
                obj.RecordID=datamodel
                obj.save()

                obj=desc.save(commit=False)
                obj.RecordID=datamodel
                obj.save()


                obj=diagnostic.save(commit=False)
                obj.RecordID=datamodel
                obj.save()

                


                # obj=Multimedia(RecordID=datamodel,Source=fullmultimediaform.cleaned_data.get("Source"),Collector=fullmultimediaform.cleaned_data.get("Collector"))
                # obj.save()


                # for f in herberiumfiles:
                #     Images.objects.create(RecordID=datamodel,Type="Herberium",image=f)
                # for f in LineDiagramfiles:
                #     Images.objects.create(RecordID=datamodel,Type="LineDiagram",image=f)
                # for f in others:
                #     Images.objects.create(RecordID=datamodel,Type="others",image=f)
                # for f in photographs:
                #     Images.objects.create(RecordID=datamodel,Type="photographs",image=f)
                

                # obj=datasetForm.save(commit=False)
                # obj.RecordID=datamodel
                # obj.save()


                obj=uploaded_records(User_Id=request.user,RecordID=datamodel,status='5')
                obj.save()



                
                return redirect("successsubmit")
            
            else:
                error="error submiting form please try again"
                
                context1={
                        "splisform":splisform,
                        "bioticform":bioticform,
                        "commonname":commonname,
                        "consstatus":consstatus,
                        "synonyms":synonyms,
                        "habitat":habitat,
                        "taxanomy":taxanomy,
                        "usesform":usesform,
                        "desc":desc,
                        "diagnostic":diagnostic,
                        
                        
                        
                        # "multimediaform":fullmultimediaform,
                    
                        'error':error,

                        }

                return render(request,template_name = "plant.html",context=context1)





    # Baseform = BaseElementForm()
    splisform=IbinSpecieslist_temp_form(request.POST)
    bioticform=PlantBioticZone_temp_form(request.POST)
    commonname=PlantCommonName_temp_form(request.POST)
    consstatus=PlantConsStatus_temp_form(request.POST)
    synonyms=PlantSynonymsCol_temp_form(request.POST)
    habitat=PlantHabitat_temp_form(request.POST)
    taxanomy=PlantTaxonomy_temp_form(request.POST)
    usesform=PlantUses_temp_form(request.POST)
    desc=PlantsDesc_temp_form(request.POST)
    diagnostic=PlantsDiagnostic_temp_form(request.POST)

    context1={
                        "splisform":splisform,
                        "bioticform":bioticform,
                        "commonname":commonname,
                        "consstatus":consstatus,
                        "synonyms":synonyms,
                        "habitat":habitat,
                        "taxanomy":taxanomy,
                        "usesform":usesform,
                        "desc":desc,
                        "diagnostic":diagnostic,
                        # "multimediaform":fullmultimediaform,

                        }

        # return render(request = request,
        #                 template_name = "index.html",
        #                 context={"form":form})
    
    return render(request = request,
                        template_name = "plant.html",context=context1)




@login_required
def Home(request):
    return render(request,template_name="plantform.html")


@login_required
def successsubmit(request):
    return render(request,template_name="Successfulsubmit.html")





@login_required
def uploadcsv(request):
    if request.method == "POST":
        print("yes file found")
        my_uploaded_file = request.FILES['my_uploaded_file']
        if not my_uploaded_file.name.endswith('.csv'):
            return render(request,template_name="uploadcsv.html",context={"error":"THIS IS NOT A CSV FILE"})
        else:
            # data = pd.read_csv (my_uploaded_file,encoding="ISO-8859-1",warn_bad_lines=False, error_bad_lines=False)
            today = date.today()
            folder='file_uploads/'+str(today)
            fs = FileSystemStorage(location=folder) #defaults to   MEDIA_ROOT  
            filen = fs.save(my_uploaded_file.name,my_uploaded_file)
            file_url = fs.url(filen)
            print(file_url)
            obj=uploaded_csv(User_Id=request.user,filename=filen,filepath=folder+file_url,status='5')
            obj.save()
            
    return render(request,template_name="uploadcsv.html")




@csrf_exempt
def save_form(request):
    
    if request.method == 'POST':
       
        
        request_data=json.loads(request.body)
        
        for k in request_data:
            res=k.keys()
            break
        
        

        cursor = connection.cursor()

        s="Create table test_table  (id serial PRIMARY KEY,"
        

        for i in res:
            s=s+i+" VARCHAR  ,"
          

        s=s[:len(s)-1]+" );"

       
        print(s)


        cursor.execute(s)

    

        for k in request_data:
            a=[]
            s='INSERT INTO test_table ('
            for i in res:
                s=s+'' +i+','
            s=s[:len(s)-1]+') VALUES ('
            for i in res:
                a.append(k[i])
                s=s+' %s ,'
            s=s[:len(s)-1]+" )"

            print(s)

            cursor.execute(s,a)

        

        todo_obj = query_to_dicts('''SELECT * FROM test_table''')

        with open('E:/protagonist.csv', 'w', newline='') as file:
            writer = csv.writer(file)
            writer.writerow(res)
            for obj in todo_obj:
                tes=[]
                for i in res:
                    tes.append(obj[i])
                
                writer.writerow(tes)
        

        cursor.execute(''' drop table test_table ''')

    return render(request,template_name="save_form.html")





def query_to_dicts(query_string, *query_args):

    #log.debug(str(dir(connection)))
    cursor = connection.cursor()
    #log.debug(str(dir(cursor)))
    cursor.execute(query_string, query_args)
    #log.debug(cursor.rowcount)log
    col_names = [desc[0] for desc in cursor.description]
    #log.debug(str(col_names))
    while True:
        row = cursor.fetchone()
        if row is None:
            break
        
        row_dict = dict(zip(col_names, row))
        yield row_dict
    
    return




def fileuploades(request):
    cu=uploaded_csv.objects.filter(User_Id=request.user)

    context={
        "data":cu,
        
    }

    return render(request,"fileuploades.html",context)






# def autocomp(request):
#     if 'term' in request.GET:
#         print("inside")
       
#         qs=IbinSpecieslist.objects.filter(genus=request.GET.get('term'))
#         exp=list()
#         for prod in qs:
#             exp.append(prod.genus)
#         return JsonResponse(exp,safe=False)